import 'dart:math';

import 'package:cleanis_store/common_widget/title_form.dart';
import 'package:cleanis_store/config/colors.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import 'package:url_launcher/url_launcher.dart';

import 'widget/orderstatus_widget.dart';

class TransactionDetail extends StatefulWidget {
  final String transactionId, userId;
  const TransactionDetail({Key key, this.transactionId, this.userId})
      : super(key: key);

  @override
  _TransactionDetailState createState() => _TransactionDetailState();
}

class _TransactionDetailState extends State<TransactionDetail> {
  final nominal = NumberFormat("#,##0", "en_US");
  final uid = FirebaseAuth.instance.currentUser.uid;
  final db = FirebaseFirestore.instance;
  final _formKey = GlobalKey<FormState>();

  Future getTransactionDetail() async {
    final transaction =
        await db.collection('transactions').doc(widget.transactionId).get();
    return transaction;
  }

  Future updateUnit({String unit, String note}) {
    try {
      return db
          .collection('transactions')
          .doc(widget.transactionId)
          .update({'totalOrder': unit, 'note': note});
    } catch (e) {
      return e.code;
    }
  }

  Future denialTransaction() {
    try {
      return db
          .collection('transactions')
          .doc(widget.transactionId)
          .update({'rejected': true});
    } catch (e) {
      return e.code;
    }
  }

  Future setNotification({String title, String description}) {
    try {
      return db
          .collection('usersProfile')
          .doc(widget.userId)
          .collection('notifications')
          .doc()
          .set({
        'created_at': DateTime.now(),
        'description': description,
        'isArchive': false,
        'isRead': false,
        'title': title
      });
    } catch (e) {
      return e.code;
    }
  }

  codConfirmed({
    String latStore,
    String lngStore,
    String latUser,
    String lngUser,
  }) {
    // Return 'COD - 1024 M Away / Self Service'
    var distance = calculateDistance(
      double.parse(latStore),
      double.parse(lngStore),
      double.parse(latUser),
      double.parse(lngUser),
    );

    return 'Delivery - ${nominal.format(distance)} M Away';
  }

  double calculateDistance(lat1, lon1, lat2, lon2) {
    var p = 0.017453292519943295;
    var c = cos;
    var a = 0.5 -
        c((lat2 - lat1) * p) / 2 +
        c(lat1 * p) * c(lat2 * p) * (1 - c((lon2 - lon1) * p)) / 2;
    return 1000 * 12742 * asin(sqrt(a));
  }

  confirmTransaction(
      BuildContext context, String status, String transactionID) {
    var currentDate = DateTime.now();
    var date = DateFormat("d MMMM yyyy", "id_ID").format(currentDate);
    var time = DateFormat("hh.mm", "id_ID").format(currentDate);
    Widget confirmButton = TextButton(
      child: const Text("Konfirmasi"),
      style: OutlinedButton.styleFrom(
        primary: Colors.white,
        backgroundColor: primaryColor,
      ),
      onPressed: () {
        db.collection('transactions').doc(widget.transactionId).update({
          'confirmed': true,
          'confirmStatusDate': date,
          'confirmStatusTime': time,
        });
        setNotification(
            title: 'Pesanan telah dikonfirmasi',
            description:
                'Pesanan anda dengan ID: $transactionID, telah dikonfirmasi');
        int count = 0;
        Navigator.popUntil(context, (route) {
          return count++ == 2;
        });
      },
    );
    Widget pickedButton = TextButton(
      child: const Text("Selesai Mengambil"),
      style: OutlinedButton.styleFrom(
        primary: Colors.white,
        backgroundColor: primaryColor,
      ),
      onPressed: () {
        db.collection('transactions').doc(widget.transactionId).update({
          'picked': true,
          'pickStatusDate': date,
          'pickStatusTime': time,
        });
        // Store to notifications users;
        setNotification(
            title: 'Pesanan telah diambil',
            description:
                'Pesanan anda dengan ID: $transactionID, telah diambil');
        int count = 0;
        Navigator.popUntil(context, (route) {
          return count++ == 2;
        });
      },
    );
    Widget processButton = TextButton(
      child: const Text("Process"),
      style: OutlinedButton.styleFrom(
        primary: Colors.white,
        backgroundColor: primaryColor,
      ),
      onPressed: () {
        db.collection('transactions').doc(widget.transactionId).update({
          'process': true,
          'processStatusDate': date,
          'processStatusTime': time
        });
        setNotification(
            title: 'Pesanan anda di proses',
            description:
                'Pesanan anda dengan ID: $transactionID, sedang diproses/dikerjakan');
        int count = 0;
        Navigator.popUntil(context, (route) {
          return count++ == 2;
        });
      },
    );
    Widget shippedButton = TextButton(
      child: const Text("Pengiriman Selesai"),
      style: OutlinedButton.styleFrom(
        primary: Colors.white,
        backgroundColor: primaryColor,
      ),
      onPressed: () {
        db.collection('transactions').doc(widget.transactionId).update({
          'shipped': true,
          'shippStatusDate': date,
          'shippStatusTime': time
        });
        setNotification(
            title: 'Pesanan telah dikemas',
            description:
                'Pesanan anda dengan ID: $transactionID, telah dikemas/dipacking, ');
        int count = 0;
        Navigator.popUntil(context, (route) {
          return count++ == 2;
        });
      },
    );
    Widget deliveryButton = TextButton(
      child: const Text("Konfirmasi Delivery"),
      style: OutlinedButton.styleFrom(
        primary: Colors.white,
        backgroundColor: primaryColor,
      ),
      onPressed: () {
        db.collection('transactions').doc(widget.transactionId).update({
          'delivery': true,
          'deliverStatusDate': date,
          'deliverStatusTime': time
        });
        setNotification(
            title: 'Pesanan telah diterima',
            description:
                'Pesanan anda dengan ID: $transactionID, telah diterima, terima kasih telah bertransaksi dengan kami');
        int count = 0;
        Navigator.popUntil(context, (route) {
          return count++ == 2;
        });
      },
    );
    Widget cancelButton = OutlinedButton(
      child: const Text("Batal"),
      onPressed: () => Navigator.pop(context),
    );
    Widget noActionButton = OutlinedButton(
      child: const Text("Tutup"),
      onPressed: () => Navigator.pop(context),
    );
    AlertDialog alert = AlertDialog(
      title: const Text("Konfirmasi pesanan",
          style: TextStyle(
            fontSize: 18,
            fontWeight: FontWeight.bold,
          )),
      content: const Text("Konfirmasi pesanan ini?"),
      actions: [
        cancelButton,
        (status == 'unconfirmed')
            ? confirmButton
            : (status == 'confirm')
                ? pickedButton
                : (status == 'picked')
                    ? processButton
                    : (status == 'process')
                        ? shippedButton
                        : (status == 'shipped')
                            ? deliveryButton
                            : noActionButton
      ],
    );
    showDialog(
      context: context,
      builder: (BuildContext context) {
        return alert;
      },
    );
  }

  Future getStore() async {
    final store =
        await FirebaseFirestore.instance.collection('stores').doc(uid).get();
    return store;
  }

  statusUpdate(confirmed, picked, process, shipped, delivery) {
    if (confirmed && picked && process && shipped && delivery) {
      return 'delivery';
    } else if (confirmed && picked && process && shipped) {
      return 'shipped';
    } else if (confirmed && picked && process) {
      return 'process';
    } else if (confirmed && picked) {
      return 'picked';
    } else if (confirmed) {
      return 'confirm';
    } else {
      return 'unconfirmed';
    }
  }

  statusTextButton(confirmed, picked, process, shipped, delivery) {
    if (confirmed && picked && process && shipped && delivery) {
      return 'Transaksi Selesai';
    } else if (confirmed && picked && process && shipped) {
      return 'Diterima Pelanggan';
    } else if (confirmed && picked && process) {
      return 'Pengiriman selesai';
    } else if (confirmed && picked) {
      return 'Pengerjaan Selesai';
    } else if (confirmed) {
      return 'Selesai Menjemput';
    } else {
      return 'Konfirmasi Pesanan';
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text('Transaction Detail'),
        backgroundColor: primaryColor,
      ),
      body: SingleChildScrollView(
        child: FutureBuilder(
          builder: (context, snapshot) {
            if (snapshot.connectionState == ConnectionState.done) {
              final transaction = snapshot.data[0];
              final store = snapshot.data[1];
              final costDelivery = nominal.format(
                  (double.parse(transaction['distance']) * 0.001) *
                      double.parse(transaction['costDelivery']));
              final orderPrice = nominal.format(
                  (double.parse(transaction['totalOrder']) *
                      double.parse(transaction['price'])));
              final totalPrice = nominal.format(
                  (double.parse(transaction['distance']) * 0.001) *
                          double.parse(transaction['costDelivery']) +
                      (double.parse(transaction['totalOrder']) *
                          double.parse(transaction['price'])));

              bool confirmed = transaction['confirmed'];
              bool picked = transaction['picked'];
              bool process = transaction['process'];
              bool shipped = transaction['shipped'];
              bool delivery = transaction['delivery'];

              final _unitController = TextEditingController()
                ..text = transaction['totalOrder'];
              final _noteController = TextEditingController()
                ..text = transaction['note'];
              return Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Container(
                    margin: const EdgeInsets.all(10),
                    decoration: BoxDecoration(
                      color: Colors.white,
                      borderRadius:
                          const BorderRadius.all(Radius.circular(8.0)),
                      border: Border.all(width: 1, color: Colors.grey[400]),
                    ),
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.end,
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: <Widget>[
                        Container(
                          padding: const EdgeInsets.fromLTRB(15, 15, 15, 5),
                          child: Column(
                            children: <Widget>[
                              Row(
                                mainAxisAlignment:
                                    MainAxisAlignment.spaceBetween,
                                children: [
                                  Column(
                                    mainAxisAlignment: MainAxisAlignment.end,
                                    crossAxisAlignment:
                                        CrossAxisAlignment.start,
                                    children: [
                                      Text(
                                        '#' + transaction['transactionId'],
                                        style: const TextStyle(
                                            fontWeight: FontWeight.w700),
                                      ),
                                      Text(
                                        transaction['orderServiceName'],
                                        style: const TextStyle(
                                          fontSize: 22,
                                          fontWeight: FontWeight.w700,
                                        ),
                                      ),
                                      Text(transaction['dateOrder']),
                                      Text(
                                        codConfirmed(
                                            latStore: store['lat'],
                                            lngStore: store['lng'],
                                            latUser: transaction['lat'],
                                            lngUser: transaction['lng']),
                                        style: const TextStyle(
                                          fontSize: 14,
                                          fontWeight: FontWeight.w600,
                                        ),
                                      ),
                                    ],
                                  ),
                                  ElevatedButton(
                                    onPressed: () {
                                      var lat = transaction['lat'];
                                      var lng = transaction['lng'];
                                      String url =
                                          'https://www.google.com/maps/search/?api=1&query=$lat,$lng';
                                      launch(url);
                                    },
                                    child: Column(
                                      mainAxisAlignment:
                                          MainAxisAlignment.center,
                                      children: const [
                                        Icon(
                                          Icons.near_me_outlined,
                                          color: primaryColor,
                                          size: 28,
                                        ),
                                        Text(
                                          'See on Map',
                                          style: TextStyle(
                                            color: primaryColor,
                                            fontWeight: FontWeight.w600,
                                            fontSize: 12,
                                          ),
                                        )
                                      ],
                                    ),
                                    style: ElevatedButton.styleFrom(
                                      fixedSize: const Size(55, 55),
                                      tapTargetSize:
                                          MaterialTapTargetSize.shrinkWrap,
                                      padding: EdgeInsets.zero,
                                      elevation: 0,
                                      primary: Colors.white,
                                      side: const BorderSide(
                                        width: 1.5,
                                        color: primaryColor,
                                      ),
                                    ),
                                  ),
                                ],
                              ),
                              const SizedBox(height: 10),
                              ListTransactionDetail(
                                title: 'Alamat:',
                                value: transaction['address'],
                              ),
                              ListTransactionDetail(
                                title: 'Jemput:',
                                value: (transaction['pickupDate'] != null)
                                    ? '${transaction['pickupDate']}, ${transaction['pickupTime']}'
                                    : 'Kosong',
                              ),
                              ListTransactionDetail(
                                title: 'Antar:',
                                value: (transaction['deliveryDate'] != null)
                                    ? '${transaction['deliveryDate']}, ${transaction['deliveryTime']}'
                                    : 'Kosong',
                              ),
                              ListTransactionDetail(
                                title: 'Pembayaran:',
                                value: transaction['paymentMethod'],
                              ),
                              ListTransactionDetail(
                                title: 'Layanan:',
                                value:
                                    '${transaction['orderServiceName']} - ${transaction['price']}/${transaction['orderType']}',
                              ),
                              ListTransactionDetail(
                                title: 'Ongkos Kirim:',
                                value:
                                    '${nominal.format(double.parse(transaction['costDelivery']))} X ${double.parse(transaction['distance']).toStringAsFixed(0)} M = $costDelivery',
                              ),
                              ListTransactionDetail(
                                title: 'Jumlah Pesanan:',
                                value:
                                    '${transaction['totalOrder']} ${transaction['orderType']} X ${transaction['price']}/${transaction['orderType']} = $orderPrice',
                              ),
                              Row(
                                children: [
                                  const Text(
                                    'Total',
                                    style: TextStyle(
                                      fontSize: 16,
                                      color: Colors.black54,
                                    ),
                                  ),
                                  const Spacer(),
                                  Text(
                                    'Rp. $totalPrice',
                                    style: const TextStyle(
                                      fontSize: 18,
                                      fontWeight: FontWeight.bold,
                                      color: Colors.black87,
                                    ),
                                  )
                                ],
                              )
                            ],
                          ),
                        ),
                        Container(
                          decoration: BoxDecoration(
                              border: Border(
                                  top: BorderSide(
                                      width: 1, color: Colors.grey[200]))),
                          child: ExpansionTile(
                            title: const Text('Order Status'),
                            children: [
                              Container(
                                padding:
                                    const EdgeInsets.fromLTRB(12, 0, 15, 10),
                                child: Column(
                                  children: [
                                    // Confirmed
                                    OrderStatus(
                                      isActive: transaction['confirmed'],
                                      title: 'Confirmed',
                                      subtile: (transaction['confirmed'])
                                          ? 'Your order has been confirmed'
                                          : 'Your order has not been confirmed',
                                      time: transaction['confirmStatusDate'] ??
                                          '',
                                      date: transaction['confirmStatusTime'] ??
                                          '',
                                      image: 'confirmed.png',
                                    ),
                                    OrderStatus(
                                      isActive: transaction['picked'],
                                      title: 'Picked',
                                      subtile: (transaction['picked'])
                                          ? 'Your order has been picked'
                                          : 'Your order has not been picked',
                                      time: transaction['pickStatusDate'] ?? '',
                                      date: transaction['pickStatusTime'] ?? '',
                                      image: 'picked.png',
                                    ),
                                    OrderStatus(
                                      isActive: transaction['process'],
                                      title: 'In process',
                                      subtile: (transaction['process'])
                                          ? 'Your order now is on process'
                                          : 'Your order has not been process',
                                      time: transaction['processStatusDate'] ??
                                          '',
                                      date: transaction['processStatusTime'] ??
                                          '',
                                      image: 'process.png',
                                    ),
                                    OrderStatus(
                                      isActive: transaction['shipped'],
                                      title: 'Shipped',
                                      subtile: (transaction['shipped'])
                                          ? 'Your order is out for shipped'
                                          : 'Your order has not been shipeed',
                                      time:
                                          transaction['shippStatusDate'] ?? '',
                                      date:
                                          transaction['shippStatusTime'] ?? '',
                                      image: 'shipped.png',
                                    ),
                                    OrderStatus(
                                      isActive: transaction['delivery'],
                                      title: 'Delivered',
                                      subtile: (transaction['delivery'])
                                          ? 'Successfully delivered on you'
                                          : 'Your order has not been delivery',
                                      time: transaction['deliverStatusDate'] ??
                                          '',
                                      date: transaction['deliverStatusTime'] ??
                                          '',
                                      image: 'delivered.png',
                                    ),
                                  ],
                                ),
                              ),
                            ],
                          ),
                        ),
                      ],
                    ),
                  ),
                  (!delivery)
                      ? Container(
                          margin: const EdgeInsets.fromLTRB(10, 0, 10, 10),
                          decoration: BoxDecoration(
                            color: Colors.white,
                            borderRadius:
                                const BorderRadius.all(Radius.circular(8.0)),
                            border:
                                Border.all(width: 1, color: Colors.grey[400]),
                          ),
                          child: Form(
                            key: _formKey,
                            child: Column(
                              children: [
                                Container(
                                  padding: const EdgeInsets.symmetric(
                                      horizontal: 10),
                                  child: Column(
                                    crossAxisAlignment:
                                        CrossAxisAlignment.start,
                                    children: [
                                      const TitleForm(title: "Update Pesanan"),
                                      Container(
                                        color: Colors.white,
                                        child: TextFormField(
                                          controller: _unitController,
                                          validator: (val) => val.isEmpty
                                              ? 'Field is required'
                                              : null,
                                          keyboardType: TextInputType.number,
                                          decoration: InputDecoration(
                                            labelText: transaction['orderType'],
                                            border: const OutlineInputBorder(),
                                            contentPadding:
                                                const EdgeInsets.all(10),
                                            isDense: true,
                                            hintText: 'Update KG/Pcs',
                                          ),
                                        ),
                                      ),
                                      const SizedBox(height: 5),
                                      Container(
                                        color: Colors.white,
                                        child: TextFormField(
                                          controller: _noteController,
                                          validator: (val) => val.isEmpty
                                              ? 'Field is required'
                                              : null,
                                          keyboardType: TextInputType.number,
                                          decoration: const InputDecoration(
                                            border: OutlineInputBorder(),
                                            isDense: true,
                                            labelText: 'Catatan',
                                            hintText: 'Catatan',
                                          ),
                                        ),
                                      ),
                                      Row(
                                        mainAxisAlignment:
                                            MainAxisAlignment.end,
                                        children: [
                                          ElevatedButton(
                                            onPressed: () {
                                              if (_formKey.currentState
                                                  .validate()) {
                                                updateUnit(
                                                    unit: _unitController.text,
                                                    note: _noteController.text);
                                                setState(() {});
                                              }
                                            },
                                            child: const Text(
                                              'UPDATE',
                                              style: TextStyle(
                                                fontSize: 14,
                                                color: primaryColor,
                                                fontWeight: FontWeight.w500,
                                              ),
                                            ),
                                            style: ElevatedButton.styleFrom(
                                              elevation: 0,
                                              padding: EdgeInsets.zero,
                                              primary: Colors.white,
                                              side: const BorderSide(
                                                width: 1.0,
                                                color: primaryColor,
                                              ),
                                            ),
                                          ),
                                        ],
                                      )
                                    ],
                                  ),
                                ),
                              ],
                            ),
                          ),
                        )
                      : const SizedBox(),
                  (!delivery)
                      ? Padding(
                          padding: const EdgeInsets.fromLTRB(10, 0, 10, 20),
                          child: Row(
                            children: [
                              (!confirmed)
                                  ? Expanded(
                                      child: ElevatedButton(
                                        onPressed: () {
                                          denialTransaction();
                                          int count = 0;
                                          Navigator.popUntil(context, (route) {
                                            return count++ == 1;
                                          });
                                        },
                                        child: const Text('TOLAK',
                                            style:
                                                TextStyle(color: primaryColor)),
                                        style: ElevatedButton.styleFrom(
                                          elevation: 0,
                                          padding: const EdgeInsets.symmetric(
                                              vertical: 15),
                                          primary: Colors.white,
                                          side: const BorderSide(
                                            width: 2.0,
                                            color: primaryColor,
                                          ),
                                          textStyle: const TextStyle(
                                            fontSize: 18,
                                            fontWeight: FontWeight.bold,
                                          ),
                                        ),
                                      ),
                                    )
                                  : const SizedBox(),
                              (!confirmed)
                                  ? const SizedBox(width: 5)
                                  : const SizedBox(),
                              Expanded(
                                child: ElevatedButton(
                                  onPressed: () {
                                    confirmTransaction(
                                        context,
                                        statusUpdate(confirmed, picked, process,
                                            shipped, delivery),
                                        transaction['transactionId']);
                                    setState(() {});
                                  },
                                  child: Text(statusTextButton(confirmed,
                                      picked, process, shipped, delivery)),
                                  style: ElevatedButton.styleFrom(
                                    elevation: 0,
                                    padding: const EdgeInsets.symmetric(
                                        vertical: 15),
                                    primary: primaryColor,
                                    textStyle: const TextStyle(
                                      fontSize: 18,
                                      fontWeight: FontWeight.bold,
                                    ),
                                  ),
                                ),
                              ),
                            ],
                          ),
                        )
                      : const SizedBox(),
                ],
              );
            }
            return const Center(child: CircularProgressIndicator());
          },
          future: Future.wait([getTransactionDetail(), getStore()]),
          // future: getTransactionDetail(),
        ),
      ),
    );
  }
}

class ListTransactionDetail extends StatelessWidget {
  final String title, value;
  const ListTransactionDetail({
    Key key,
    this.title,
    this.value,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Row(
      children: [
        Text(
          title,
          style: const TextStyle(
            fontSize: 16,
            color: Colors.black54,
          ),
        ),
        const Spacer(),
        Text(
          value,
          style: const TextStyle(
            fontSize: 16,
            color: Colors.black87,
          ),
        )
      ],
    );
  }
}
