import 'dart:math';

import 'package:cleanis_store/common_widget/emptypage_widget.dart';
import 'package:cleanis_store/config/colors.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import 'package:url_launcher/url_launcher.dart';
import 'widget/transactionscard_widget.dart';

double storeLat, storeLng;

class TransactionsScreen extends StatefulWidget {
  const TransactionsScreen({Key key}) : super(key: key);

  @override
  _TransactionsScreenState createState() => _TransactionsScreenState();
}

class _TransactionsScreenState extends State<TransactionsScreen>
    with SingleTickerProviderStateMixin {
  final nominal = NumberFormat("#,##0", "en_US");
  TabController _tabController;
  ScrollController _scrollViewController;
  int _selectedIndex = 0;
  final uid = FirebaseAuth.instance.currentUser.uid;
  final db = FirebaseFirestore.instance;

  CollectionReference collectionReference =
      FirebaseFirestore.instance.collection('stores');

  void getStore() {
    collectionReference.doc(uid).get().then((value) {
      var fields = value;
      setState(() {
        storeLat = double.parse(fields.get('lat'));
        storeLng = double.parse(fields.get('lng'));
      });
    });
  }

  Future<QuerySnapshot> getTransactions(status) {
    // Composite index order by newest date
    switch (status) {
      case 0:
        return db
            .collection('transactions')
            .where('storeId', isEqualTo: uid)
            .where('confirmed', isEqualTo: false)
            .where('rejected', isEqualTo: false)
            .get();
        break;
      case 1:
        return db
            .collection('transactions')
            .where('storeId', isEqualTo: uid)
            .where('confirmed', isEqualTo: true)
            .where('picked', isEqualTo: false)
            .where('rejected', isEqualTo: false)
            .get();
        break;
      case 2:
        return db
            .collection('transactions')
            .where('storeId', isEqualTo: uid)
            .where('confirmed', isEqualTo: true)
            .where('picked', isEqualTo: true)
            .where('process', isEqualTo: false)
            .where('rejected', isEqualTo: false)
            .get();
        break;
      case 3:
        return db
            .collection('transactions')
            .where('storeId', isEqualTo: uid)
            .where('confirmed', isEqualTo: true)
            .where('picked', isEqualTo: true)
            .where('process', isEqualTo: true)
            .where('shipped', isEqualTo: false)
            .where('rejected', isEqualTo: false)
            .get();
        break;
      case 4:
        return db
            .collection('transactions')
            .where('storeId', isEqualTo: uid)
            .where('confirmed', isEqualTo: true)
            .where('picked', isEqualTo: true)
            .where('process', isEqualTo: true)
            .where('shipped', isEqualTo: true)
            .where('rejected', isEqualTo: false)
            .get();
        break;

      default:
        return db
            .collection('transactions')
            .where('storeId', isEqualTo: uid)
            .where('confirmed', isEqualTo: false)
            .where('rejected', isEqualTo: false)
            .get();
    }
  }

  getRoute() async {
    var headUrl = 'https://www.google.com/maps/dir/$storeLat,$storeLng/';
    var storeDistance = [];
    final data = await db
        .collection('transactions')
        .where('storeId', isEqualTo: uid)
        .where('confirmed', isEqualTo: true)
        .where('picked', isEqualTo: false)
        .where('rejected', isEqualTo: false)
        .get();

    for (var element in data.docs) {
      String idTransactions = element.id;
      String idUser = element.get('userId');
      String serviceName = element.get('orderServiceName');
      double latUser = double.parse(element.get('lat'));
      double lngUser = double.parse(element.get('lng'));
      double distance = calculateDistance(storeLat, storeLng, latUser, lngUser);

      storeDistance.add({
        "idTransactions": idTransactions,
        "idUser": idUser,
        "serviceName": serviceName,
        "latUser": latUser,
        "lngUser": lngUser,
        "distance": distance
      });
      for (var element in storeDistance) {
        element;
      }
    }
    storeDistance.sort((a, b) => a["distance"].compareTo(b["distance"]));

    String geoposition = "";

    for (var element in storeDistance) {
      geoposition += element['latUser'].toString() +
          ',' +
          element['lngUser'].toString() +
          '/';
    }
    String directionUrl = headUrl + geoposition;
    return directionUrl;
  }

  double calculateDistance(lat1, lon1, lat2, lon2) {
    var p = 0.017453292519943295;
    var c = cos;
    var a = 0.5 -
        c((lat2 - lat1) * p) / 2 +
        c(lat1 * p) * c(lat2 * p) * (1 - c((lon2 - lon1) * p)) / 2;
    return 1000 * 12742 * asin(sqrt(a));
  }

  getStatusName(confirmed, picked, process, shipped, delivery) {
    if (confirmed && picked && process && shipped && delivery) {
      return 'Delivery';
    } else if (confirmed && picked && process && shipped) {
      return 'Shipped';
    } else if (confirmed && picked && process) {
      return 'On Process';
    } else if (confirmed && picked) {
      return 'Has Been Picked Up';
    } else if (confirmed) {
      return 'On Picked';
    } else {
      return 'Menunggu konfirmasi';
    }
  }

  @override
  void initState() {
    super.initState();
    getStore();
    _tabController = TabController(vsync: this, length: 5);
    _scrollViewController = ScrollController(initialScrollOffset: 0.0);
    _tabController.addListener(() {
      setState(() {
        _selectedIndex = _tabController.index;
      });
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: background,
      body: NestedScrollView(
        controller: _scrollViewController,
        headerSliverBuilder: (BuildContext context, bool boxIsScrolled) {
          return <Widget>[
            SliverAppBar(
              backgroundColor: primaryColor,
              title: const Text('Transactions'),
              pinned: true,
              floating: true,
              forceElevated: boxIsScrolled,
              actions: <Widget>[
                (_selectedIndex == 1)
                    ? IconButton(
                        padding: const EdgeInsets.only(right: 20),
                        icon: const Icon(
                          Icons.directions_outlined,
                          color: Colors.white,
                        ),
                        onPressed: () async {
                          var url = await getRoute();
                          launch(url);
                        },
                      )
                    : const SizedBox(),
              ],
              bottom: TabBar(
                isScrollable: true,
                onTap: (index) {
                  _selectedIndex = index;
                },
                indicatorColor: Colors.white,
                tabs: <Widget>[
                  Tab(
                    child: Row(
                      children: [
                        const Icon(Icons.pending_actions_outlined),
                        const SizedBox(width: 10),
                        Text(_selectedIndex == 0 ? 'Need Confirmed' : '')
                      ],
                    ),
                  ),
                  Tab(
                    child: Row(
                      children: [
                        const Icon(Icons.delivery_dining_rounded),
                        const SizedBox(width: 10),
                        Text(_selectedIndex == 1 ? 'On Pickup' : '')
                      ],
                    ),
                  ),
                  Tab(
                    child: Row(
                      children: [
                        const Icon(Icons.local_laundry_service_rounded),
                        const SizedBox(width: 10),
                        Text(_selectedIndex == 2 ? 'In Process' : '')
                      ],
                    ),
                  ),
                  Tab(
                    child: Row(
                      children: [
                        const Icon(Icons.local_shipping),
                        const SizedBox(width: 10),
                        Text(_selectedIndex == 3 ? 'On Delivery' : '')
                      ],
                    ),
                  ),
                  Tab(
                    child: Row(
                      children: [
                        const Icon(Icons.inventory_outlined),
                        const SizedBox(width: 10),
                        Text(_selectedIndex == 4 ? 'Delivered' : '')
                      ],
                    ),
                  ),
                ],
                controller: _tabController,
              ),
            )
          ];
        },
        body: TabBarView(
          children: [
            // Need Confirmed
            FutureBuilder(
              future: getTransactions(_selectedIndex),
              builder: (context, snapshot) {
                if (snapshot.connectionState == ConnectionState.done) {
                  final transaction = snapshot.data.docs;
                  if (transaction.length == 0) {
                    return const EmptyPage();
                  } else {
                    return MediaQuery.removePadding(
                      context: context,
                      removeTop: true,
                      child: ListView.builder(
                        shrinkWrap: true,
                        itemCount: snapshot.data.docs.length,
                        itemBuilder: (BuildContext context, int index) {
                          return TransactionsCard(
                            userId: transaction[index]['userId'],
                            docId: transaction[index].id,
                            storeId: transaction[index]['storeId'],
                            transactionId: transaction[index]['transactionId'],
                            store: transaction[index]['orderServiceName'],
                            dateOrder: transaction[index]['dateOrder'],
                            status: getStatusName(
                              transaction[index]['confirmed'],
                              transaction[index]['picked'],
                              transaction[index]['process'],
                              transaction[index]['shipped'],
                              transaction[index]['delivery'],
                            ),
                            confirmed: transaction[index]['confirmed'],
                            picked: transaction[index]['picked'],
                            process: transaction[index]['process'],
                            shipped: transaction[index]['shipped'],
                            delivery: transaction[index]['delivery'],
                          );
                        },
                      ),
                    );
                  }
                }
                return const Center(child: CircularProgressIndicator());
              },
            ),
            FutureBuilder(
              future: getTransactions(_selectedIndex),
              builder: (context, snapshot) {
                if (snapshot.connectionState == ConnectionState.done) {
                  final transaction = snapshot.data.docs;
                  if (transaction.length == 0) {
                    return const EmptyPage();
                  } else {
                    return MediaQuery.removePadding(
                      context: context,
                      removeTop: true,
                      child: ListView.builder(
                        shrinkWrap: true,
                        itemCount: snapshot.data.docs.length,
                        itemBuilder: (BuildContext context, int index) {
                          return TransactionsCard(
                            userId: transaction[index]['userId'],
                            docId: transaction[index].id,
                            storeId: transaction[index]['storeId'],
                            transactionId: transaction[index]['transactionId'],
                            store: transaction[index]['orderServiceName'],
                            dateOrder: transaction[index]['dateOrder'],
                            status: getStatusName(
                              transaction[index]['confirmed'],
                              transaction[index]['picked'],
                              transaction[index]['process'],
                              transaction[index]['shipped'],
                              transaction[index]['delivery'],
                            ),
                            confirmed: transaction[index]['confirmed'],
                            picked: transaction[index]['picked'],
                            process: transaction[index]['process'],
                            shipped: transaction[index]['shipped'],
                            delivery: transaction[index]['delivery'],
                          );
                        },
                      ),
                    );
                  }
                }
                return const Center(child: CircularProgressIndicator());
              },
            ),
            FutureBuilder(
              future: getTransactions(_selectedIndex),
              builder: (context, snapshot) {
                if (snapshot.connectionState == ConnectionState.done) {
                  final transaction = snapshot.data.docs;
                  if (transaction.length == 0) {
                    return const EmptyPage();
                  } else {
                    return MediaQuery.removePadding(
                      context: context,
                      removeTop: true,
                      child: ListView.builder(
                        shrinkWrap: true,
                        itemCount: snapshot.data.docs.length,
                        itemBuilder: (BuildContext context, int index) {
                          return TransactionsCard(
                            userId: transaction[index]['userId'],
                            docId: transaction[index].id,
                            storeId: transaction[index]['storeId'],
                            transactionId: transaction[index]['transactionId'],
                            store: transaction[index]['orderServiceName'],
                            dateOrder: transaction[index]['dateOrder'],
                            status: getStatusName(
                              transaction[index]['confirmed'],
                              transaction[index]['picked'],
                              transaction[index]['process'],
                              transaction[index]['shipped'],
                              transaction[index]['delivery'],
                            ),
                            confirmed: transaction[index]['confirmed'],
                            picked: transaction[index]['picked'],
                            process: transaction[index]['process'],
                            shipped: transaction[index]['shipped'],
                            delivery: transaction[index]['delivery'],
                          );
                        },
                      ),
                    );
                  }
                }
                return const Center(child: CircularProgressIndicator());
              },
            ),
            FutureBuilder(
              future: getTransactions(_selectedIndex),
              builder: (context, snapshot) {
                if (snapshot.connectionState == ConnectionState.done) {
                  final transaction = snapshot.data.docs;
                  if (transaction.length == 0) {
                    return const EmptyPage();
                  } else {
                    return MediaQuery.removePadding(
                      context: context,
                      removeTop: true,
                      child: ListView.builder(
                        shrinkWrap: true,
                        itemCount: snapshot.data.docs.length,
                        itemBuilder: (BuildContext context, int index) {
                          return TransactionsCard(
                            userId: transaction[index]['userId'],
                            docId: transaction[index].id,
                            storeId: transaction[index]['storeId'],
                            transactionId: transaction[index]['transactionId'],
                            store: transaction[index]['orderServiceName'],
                            dateOrder: transaction[index]['dateOrder'],
                            status: getStatusName(
                              transaction[index]['confirmed'],
                              transaction[index]['picked'],
                              transaction[index]['process'],
                              transaction[index]['shipped'],
                              transaction[index]['delivery'],
                            ),
                            confirmed: transaction[index]['confirmed'],
                            picked: transaction[index]['picked'],
                            process: transaction[index]['process'],
                            shipped: transaction[index]['shipped'],
                            delivery: transaction[index]['delivery'],
                          );
                        },
                      ),
                    );
                  }
                }
                return const Center(child: CircularProgressIndicator());
              },
            ),
            FutureBuilder(
              future: getTransactions(_selectedIndex),
              builder: (context, snapshot) {
                if (snapshot.connectionState == ConnectionState.done) {
                  final transaction = snapshot.data.docs;
                  if (transaction.length == 0) {
                    return const EmptyPage();
                  } else {
                    return MediaQuery.removePadding(
                      context: context,
                      removeTop: true,
                      child: ListView.builder(
                        shrinkWrap: true,
                        itemCount: snapshot.data.docs.length,
                        itemBuilder: (BuildContext context, int index) {
                          return TransactionsCard(
                            userId: transaction[index]['userId'],
                            docId: transaction[index].id,
                            storeId: transaction[index]['storeId'],
                            transactionId: transaction[index]['transactionId'],
                            store: transaction[index]['orderServiceName'],
                            dateOrder: transaction[index]['dateOrder'],
                            status: getStatusName(
                              transaction[index]['confirmed'],
                              transaction[index]['picked'],
                              transaction[index]['process'],
                              transaction[index]['shipped'],
                              transaction[index]['delivery'],
                            ),
                            confirmed: transaction[index]['confirmed'],
                            picked: transaction[index]['picked'],
                            process: transaction[index]['process'],
                            shipped: transaction[index]['shipped'],
                            delivery: transaction[index]['delivery'],
                          );
                        },
                      ),
                    );
                  }
                }
                return const Center(child: CircularProgressIndicator());
              },
            ),
          ],
          controller: _tabController,
        ),
      ),
    );
  }
}
