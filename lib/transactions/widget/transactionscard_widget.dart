import 'package:cleanis_store/config/colors.dart';
import 'package:cleanis_store/transactions/transaction_detail.dart';
import 'package:flutter/material.dart';

class TransactionsCard extends StatelessWidget {
  final String docId, transactionId, storeId, userId;
  final String store, dateOrder, price, status;
  final bool confirmed, picked, process, shipped, delivery;
  const TransactionsCard({
    Key key,
    this.store,
    this.dateOrder,
    this.price,
    this.confirmed,
    this.picked,
    this.process,
    this.shipped,
    this.delivery,
    this.status,
    this.transactionId,
    this.storeId,
    this.docId,
    this.userId,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      width: double.infinity,
      padding: const EdgeInsets.fromLTRB(10, 10, 10, 0),
      child: Ink(
        color: Colors.white,
        child: InkWell(
          splashColor: primaryLightenColor,
          onTap: () {
            Navigator.push(
              context,
              MaterialPageRoute(
                builder: (context) => TransactionDetail(
                  transactionId: docId,
                  userId: userId,
                ),
              ),
            );
          },
          child: Container(
            decoration: BoxDecoration(
              borderRadius: const BorderRadius.all(
                Radius.circular(8.0),
              ),
              border: Border.all(
                width: 1,
                color: Colors.grey[400],
              ),
            ),
            child: Column(
              children: <Widget>[
                Container(
                  padding: const EdgeInsets.fromLTRB(10, 10, 10, 5),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: <Widget>[
                      Text(
                        store,
                        style: const TextStyle(
                          fontSize: 20,
                          fontWeight: FontWeight.w700,
                        ),
                      ),
                      Text(
                        '#$transactionId',
                        style: const TextStyle(
                          fontSize: 15,
                          fontWeight: FontWeight.w600,
                        ),
                      ),
                    ],
                  ),
                ),
                Container(
                  margin: const EdgeInsets.only(bottom: 5),
                  padding: const EdgeInsets.symmetric(horizontal: 10),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: <Widget>[
                      Text(
                        dateOrder,
                        style: const TextStyle(
                          fontSize: 14,
                          fontWeight: FontWeight.w400,
                        ),
                      ),
                      Text(
                        status,
                        style: const TextStyle(
                          fontSize: 14,
                          color: Colors.blue,
                          fontWeight: FontWeight.w500,
                        ),
                      ),
                    ],
                  ),
                ),
                Container(
                  padding:
                      const EdgeInsets.symmetric(horizontal: 10, vertical: 12),
                  decoration: BoxDecoration(
                    border: Border(
                      top: BorderSide(
                        width: 0.8,
                        color: Colors.grey[400],
                      ),
                    ),
                  ),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: <Widget>[
                      Container(
                        padding: const EdgeInsets.symmetric(horizontal: 4),
                        child: Column(
                          children: <Widget>[
                            ImageStatus(
                              image: 'confirmed',
                              isActive: confirmed,
                            ),
                            const SizedBox(height: 5),
                            const Text(
                              'Confirmed',
                              style: TextStyle(
                                fontSize: 12,
                                fontWeight: FontWeight.w500,
                              ),
                            )
                          ],
                        ),
                      ),
                      Container(
                        padding: const EdgeInsets.symmetric(horizontal: 4),
                        child: Column(
                          children: <Widget>[
                            ImageStatus(
                              image: 'picked',
                              isActive: picked,
                            ),
                            const SizedBox(height: 5),
                            const Text(
                              'Picked Up',
                              style: TextStyle(
                                fontSize: 12,
                                fontWeight: FontWeight.w500,
                              ),
                            )
                          ],
                        ),
                      ),
                      Container(
                        padding: const EdgeInsets.symmetric(horizontal: 4),
                        child: Column(
                          children: <Widget>[
                            ImageStatus(
                              image: 'process',
                              isActive: process,
                            ),
                            const SizedBox(height: 5),
                            const Text(
                              'In Process',
                              style: TextStyle(
                                fontSize: 12,
                                fontWeight: FontWeight.w500,
                              ),
                            )
                          ],
                        ),
                      ),
                      Container(
                        padding: const EdgeInsets.symmetric(horizontal: 4),
                        child: Column(
                          children: <Widget>[
                            ImageStatus(
                              image: 'shipped',
                              isActive: shipped,
                            ),
                            const SizedBox(height: 5),
                            const Text(
                              'Shipped',
                              style: TextStyle(
                                fontSize: 12,
                                fontWeight: FontWeight.w500,
                              ),
                            )
                          ],
                        ),
                      ),
                      Container(
                        padding: const EdgeInsets.symmetric(horizontal: 4),
                        child: Column(
                          children: <Widget>[
                            ImageStatus(
                              image: 'delivered',
                              isActive: delivery,
                            ),
                            const SizedBox(height: 5),
                            const Text(
                              'Delivered',
                              style: TextStyle(
                                fontSize: 12,
                                fontWeight: FontWeight.w500,
                              ),
                            )
                          ],
                        ),
                      ),
                    ],
                  ),
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }
}

class ImageStatus extends StatelessWidget {
  final String image;
  final bool isActive;
  const ImageStatus({
    Key key,
    this.image,
    this.isActive,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return (isActive)
        ? Image.asset(
            'assets/images/status/$image.png',
            height: 40,
          )
        : ColorFiltered(
            colorFilter: const ColorFilter.matrix(<double>[
              0.2126,
              0.7152,
              0.0722,
              0,
              0,
              0.2126,
              0.7152,
              0.0722,
              0,
              0,
              0.2126,
              0.7152,
              0.0722,
              0,
              0,
              0,
              0,
              0,
              1,
              0,
            ]),
            child: Image.asset(
              'assets/images/status/$image.png',
              height: 40,
            ),
          );
  }
}
