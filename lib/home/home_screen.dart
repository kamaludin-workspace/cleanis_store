import 'package:cleanis_store/config/colors.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';

import 'widget/counter_transaction.dart';
import '../common_widget/emptypage_widget.dart';
import 'widget/latest_transactions.dart';
import 'widget/page_title.dart';

String process;

class HomeScreen extends StatefulWidget {
  const HomeScreen({Key key}) : super(key: key);

  @override
  _HomeScreenState createState() => _HomeScreenState();
}

class _HomeScreenState extends State<HomeScreen> {
  final uid = FirebaseAuth.instance.currentUser.uid;
  final db = FirebaseFirestore.instance;

  void getProcessTransactions() {
    db
        .collection('transactions')
        .where('storeId', isEqualTo: uid)
        .where('confirmed', isEqualTo: true)
        .where('delivery', isEqualTo: false)
        .where('rejected', isEqualTo: false)
        .get()
        .then((value) {
      var size = value.size;
      setState(() {
        process = size.toString();
      });
    });
  }

  @override
  void initState() {
    getProcessTransactions();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text('Home'),
        backgroundColor: primaryColor,
        // actions: <Widget>[
        //   IconButton(
        //     padding: const EdgeInsets.only(right: 20),
        //     icon: const Icon(
        //       Icons.post_add_outlined,
        //       color: Colors.white,
        //     ),
        //     onPressed: () {
        //       TODO: Cara menambah transaksi manual oleh toko
        //       1. Toko membuat/isi form service
        //       2. Toko menyerahkan/menyediakan qrcode/kode unik, yang bisa discan user
        //       3. User scan atau masukkan kode unik untuk klaim transaksi
        //     },
        //   ),
        // ],
      ),
      body: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          const PageTitle(title: 'Dashboard'),
          StreamBuilder<QuerySnapshot>(
              stream: db
                  .collection('transactions')
                  .where('storeId', isEqualTo: uid)
                  .where('confirmed', isEqualTo: false)
                  .where('rejected', isEqualTo: false)
                  .snapshots(),
              builder: (context, snapshot) {
                if (!snapshot.hasData) {
                  return const Center(
                    child: CircularProgressIndicator(),
                  );
                } else {
                  return CounterTransaction(
                    incoming: snapshot.data.docs.length.toString() ?? '0',
                    process: process ?? '0',
                  );
                }
              }),
          const PageTitle(title: 'Latest Transactions'),
          Container(
            padding: const EdgeInsets.symmetric(horizontal: 15),
            child: StreamBuilder<QuerySnapshot>(
              stream: db
                  .collection('transactions')
                  .where('storeId', isEqualTo: uid)
                  .where('confirmed', isEqualTo: false)
                  .where('rejected', isEqualTo: false)
                  .orderBy('created_at', descending: true)
                  .snapshots(),
              builder: (context, snapshot) {
                if (!snapshot.hasData) {
                  return const Center(
                    child: CircularProgressIndicator(),
                  );
                } else {
                  return (snapshot.data.docs.isNotEmpty)
                      ? ListView(
                          physics: const NeverScrollableScrollPhysics(),
                          shrinkWrap: true,
                          children: snapshot.data.docs.map((doc) {
                            return LatesTransactions(
                              id: doc.id,
                              name: (doc.data() as dynamic)['orderServiceName'],
                              date: (doc.data() as dynamic)['dateOrder'],
                            );
                          }).toList())
                      : const EmptyPage();
                }
              },
            ),
          )
        ],
      ),
    );
  }
}
