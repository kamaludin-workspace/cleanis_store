import 'package:flutter/material.dart';

class OrderStatus extends StatelessWidget {
  final String title, subtile, time, date, image;
  final bool isActive;
  const OrderStatus({
    Key key,
    this.title,
    this.subtile,
    this.time,
    this.date,
    this.image,
    this.isActive,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Row(
      children: [
        (isActive)
            ? Image.asset(
                'assets/images/basket/$image',
                height: 30,
              )
            : ColorFiltered(
                colorFilter: const ColorFilter.matrix(<double>[
                  0.2126,
                  0.7152,
                  0.0722,
                  0,
                  0,
                  0.2126,
                  0.7152,
                  0.0722,
                  0,
                  0,
                  0.2126,
                  0.7152,
                  0.0722,
                  0,
                  0,
                  0,
                  0,
                  0,
                  1,
                  0,
                ]),
                child: Image.asset(
                  'assets/images/basket/$image',
                  height: 30,
                ),
              ),
        Container(
          padding: const EdgeInsets.symmetric(horizontal: 10, vertical: 15),
          child: RichText(
            text: TextSpan(
              children: [
                TextSpan(
                  text: "$title \n",
                  style: const TextStyle(
                    color: Colors.black,
                    fontSize: 14,
                    fontWeight: FontWeight.w400,
                  ),
                ),
                TextSpan(
                  text: subtile,
                  style: const TextStyle(
                    fontSize: 10,
                    fontWeight: FontWeight.w300,
                    color: Colors.black,
                  ),
                ),
              ],
            ),
          ),
        ),
        const Spacer(),
        RichText(
          textAlign: TextAlign.right,
          text: TextSpan(
            children: [
              TextSpan(
                text: "$time \n",
                style: const TextStyle(
                  fontSize: 13,
                  fontWeight: FontWeight.w300,
                  color: Colors.black,
                ),
              ),
              TextSpan(
                text: date,
                style: const TextStyle(
                  color: Colors.black,
                  fontSize: 11,
                  fontWeight: FontWeight.w400,
                ),
              ),
            ],
          ),
        ),
      ],
    );
  }
}
