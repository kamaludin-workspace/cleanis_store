import 'dart:math';

import 'package:cleanis_store/common_widget/title_form.dart';
import 'package:cleanis_store/config/colors.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import 'package:url_launcher/url_launcher.dart';

import '../common_widget/emptypage_widget.dart';
import 'widget/serviceinfo_widget.dart';

class TransactionDetail extends StatefulWidget {
  final String transactionId;
  const TransactionDetail({Key key, this.transactionId}) : super(key: key);

  @override
  _TransactionDetailState createState() => _TransactionDetailState();
}

class _TransactionDetailState extends State<TransactionDetail> {
  final nominal = NumberFormat("#,##0", "en_US");
  final db = FirebaseFirestore.instance;
  final uid = FirebaseAuth.instance.currentUser.uid;
  final _formKey = GlobalKey<FormState>();

  Future getStore() async {
    final store =
        await FirebaseFirestore.instance.collection('stores').doc(uid).get();
    return store;
  }

  Future getTransactionDetail() async {
    final transaction =
        await db.collection('transactions').doc(widget.transactionId).get();
    return transaction;
  }

  Future updateUnit({String unit, String note}) {
    try {
      return db
          .collection('transactions')
          .doc(widget.transactionId)
          .update({'totalOrder': unit, 'note': note});
    } catch (e) {
      return e.code;
    }
  }

  codConfirmed({
    String latStore,
    String lngStore,
    String latUser,
    String lngUser,
  }) {
    // Return 'COD - 1024 M Away / Self Service'
    var distance = calculateDistance(
      double.parse(latStore),
      double.parse(lngStore),
      double.parse(latUser),
      double.parse(lngUser),
    );

    return 'Delivery - ${nominal.format(distance)} M Away';
  }

  double calculateDistance(lat1, lon1, lat2, lon2) {
    var p = 0.017453292519943295;
    var c = cos;
    var a = 0.5 -
        c((lat2 - lat1) * p) / 2 +
        c(lat1 * p) * c(lat2 * p) * (1 - c((lon2 - lon1) * p)) / 2;
    return 1000 * 12742 * asin(sqrt(a));
  }

  confirmTransaction(BuildContext context) {
    var currentDate = DateTime.now();
    var date = DateFormat("d MMMM yyyy", "id_ID").format(currentDate);
    var time = DateFormat("hh.mm", "id_ID").format(currentDate);
    Widget confirmButton = TextButton(
      child: const Text(
        "Konfirmasi",
        style: TextStyle(color: Colors.white),
      ),
      style: OutlinedButton.styleFrom(
        primary: Colors.white,
        backgroundColor: primaryColor,
      ),
      onPressed: () {
        db.collection('transactions').doc(widget.transactionId).update({
          'confirmed': true,
          'confirmStatusDate': date,
          'confirmStatusTime': time
        });
        Navigator.pop(context);
      },
    );
    Widget cancelButton = OutlinedButton(
      child: const Text("Batal"),
      onPressed: () => Navigator.pop(context),
    );
    AlertDialog alert = AlertDialog(
      title: const Text("Konfirmasi pesanan",
          style: TextStyle(
            fontSize: 18,
            fontWeight: FontWeight.bold,
          )),
      content: const Text("Konfirmasi pesanan ini?"),
      actions: [
        cancelButton,
        confirmButton,
      ],
    );
    showDialog(
      context: context,
      builder: (BuildContext context) {
        return alert;
      },
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text('Transactions Detail'),
        backgroundColor: primaryColor,
      ),
      body: SingleChildScrollView(
        child: FutureBuilder(
          builder: (context, snapshot) {
            if (snapshot.connectionState == ConnectionState.done) {
              try {
                final serviceDetail = snapshot.data[0];
                final store = snapshot.data[1];
                final _unitController = TextEditingController()
                  ..text = serviceDetail['totalOrder'];
                final _noteController = TextEditingController()
                  ..text = serviceDetail['note'];
                return Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Container(
                      margin: const EdgeInsets.fromLTRB(10, 10, 10, 0),
                      decoration: BoxDecoration(
                        color: Colors.white,
                        borderRadius:
                            const BorderRadius.all(Radius.circular(8.0)),
                        border: Border.all(width: 1, color: Colors.grey[400]),
                      ),
                      child: Column(
                        mainAxisAlignment: MainAxisAlignment.end,
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: <Widget>[
                          Container(
                            padding: const EdgeInsets.symmetric(
                                horizontal: 15, vertical: 10),
                            child: Row(
                              children: [
                                Expanded(
                                  child: Column(
                                    mainAxisAlignment: MainAxisAlignment.end,
                                    crossAxisAlignment:
                                        CrossAxisAlignment.start,
                                    children: <Widget>[
                                      Text(
                                        serviceDetail['orderServiceName'],
                                        style: const TextStyle(
                                          fontSize: 22,
                                          fontWeight: FontWeight.w700,
                                        ),
                                      ),
                                      Text(serviceDetail['dateOrder']),
                                      (serviceDetail['pickupDate'] != '-')
                                          ? Text(
                                              codConfirmed(
                                                  latStore: store['lat'],
                                                  lngStore: store['lng'],
                                                  latUser: serviceDetail['lat'],
                                                  lngUser:
                                                      serviceDetail['lng']),
                                              style: const TextStyle(
                                                fontSize: 14,
                                                fontWeight: FontWeight.w600,
                                              ),
                                            )
                                          : const SizedBox()
                                    ],
                                  ),
                                ),
                                (serviceDetail['pickupDate'] != '-')
                                    ? ElevatedButton(
                                        onPressed: () {
                                          var lat = serviceDetail['lat'];
                                          var lng = serviceDetail['lng'];
                                          String url =
                                              'https://www.google.com/maps/search/?api=1&query=$lat,$lng';
                                          launch(url);
                                        },
                                        child: Column(
                                          mainAxisAlignment:
                                              MainAxisAlignment.center,
                                          children: const [
                                            Icon(
                                              Icons.near_me_outlined,
                                              color: primaryColor,
                                              size: 28,
                                            ),
                                            Text(
                                              'See on Map',
                                              style: TextStyle(
                                                color: primaryColor,
                                                fontWeight: FontWeight.w600,
                                                fontSize: 12,
                                              ),
                                            )
                                          ],
                                        ),
                                        style: ElevatedButton.styleFrom(
                                          fixedSize: const Size(55, 55),
                                          tapTargetSize:
                                              MaterialTapTargetSize.shrinkWrap,
                                          padding: EdgeInsets.zero,
                                          elevation: 0,
                                          primary: Colors.white,
                                          side: const BorderSide(
                                            width: 1.5,
                                            color: primaryColor,
                                          ),
                                        ),
                                      )
                                    : const SizedBox(),
                              ],
                            ),
                          ),
                          const Divider(),
                          Container(
                            padding: const EdgeInsets.symmetric(horizontal: 15),
                            child: Row(
                              mainAxisAlignment: MainAxisAlignment.spaceBetween,
                              children: <Widget>[
                                const Text(
                                  'Order Info',
                                  style: TextStyle(
                                    fontSize: 18,
                                  ),
                                ),
                                Text(
                                  '#' + serviceDetail['transactionId'],
                                  style: const TextStyle(
                                      fontWeight: FontWeight.w700),
                                )
                              ],
                            ),
                          ),
                          const Divider(),
                          ServiceInfo(
                            title: 'Waktu Jemput',
                            subtitle: (serviceDetail['pickupDate'] != '-')
                                ? '${serviceDetail['pickupDate']}, ${serviceDetail['pickupTime']}'
                                : 'Kosong',
                          ),
                          ServiceInfo(
                            title: 'Waktu Antar',
                            subtitle: (serviceDetail['deliveryDate'] != '-')
                                ? '${serviceDetail['deliveryDate']}, ${serviceDetail['deliveryTime']}'
                                : 'Kosong',
                          ),
                          (serviceDetail['pickupDate'] != '-')
                              ? ServiceInfo(
                                  title:
                                      'Alamat - ${serviceDetail['addressName']}',
                                  subtitle: serviceDetail['address'],
                                )
                              : const SizedBox(),
                          ServiceInfo(
                            title: 'Metode Pembayaran',
                            subtitle: serviceDetail['paymentMethod'],
                          ),
                          ServiceInfo(
                              title:
                                  'Rp. ${nominal.format(int.parse(serviceDetail['price']))} X ${serviceDetail['totalOrder']} ${serviceDetail['orderType']}',
                              subtitle:
                                  'Rp. ${nominal.format(int.parse(serviceDetail['price']) * double.parse(serviceDetail['totalOrder']))}'),
                          ServiceInfo(
                              title:
                                  'Rp. ${nominal.format(double.parse(serviceDetail['costDelivery']))}/Km X ${serviceDetail['distance']}',
                              subtitle:
                                  'Rp. ${((double.parse(serviceDetail['distance']) * 0.001) * double.parse(serviceDetail['costDelivery'])).toStringAsFixed(0)}'),
                          const Divider(),
                          ServiceInfo(
                            title: 'Total',
                            subtitle:
                                'Rp. ${nominal.format((double.parse(serviceDetail['totalOrder']) * int.parse(serviceDetail['price'])) + (double.parse(serviceDetail['distance']) * 0.001) * int.parse(serviceDetail['costDelivery']))}',
                          ),
                          Container(
                            margin: const EdgeInsets.only(top: 5),
                            child: ExpansionTile(
                              title: const Text('Customer'),
                              children: [
                                CustomerCard(
                                  name: 'Nama Pemesan',
                                  thumbnail:
                                      'https://ik.imagekit.io/n0t5masg5jg/stores/photoUsers/download__1__ihBz_84PoMP.jfif?updatedAt=1639036921125',
                                  addres: serviceDetail['address'],
                                  phoneNumber: '09821231',
                                ),
                              ],
                            ),
                          )
                        ],
                      ),
                    ),
                    Container(
                      margin: const EdgeInsets.all(10),
                      decoration: BoxDecoration(
                        color: Colors.white,
                        borderRadius:
                            const BorderRadius.all(Radius.circular(8.0)),
                        border: Border.all(width: 1, color: Colors.grey[400]),
                      ),
                      child: Form(
                        key: _formKey,
                        child: Column(
                          children: [
                            Container(
                              padding:
                                  const EdgeInsets.symmetric(horizontal: 10),
                              child: Column(
                                crossAxisAlignment: CrossAxisAlignment.start,
                                children: [
                                  const TitleForm(title: "Update Pesanan"),
                                  Container(
                                    color: Colors.white,
                                    child: TextFormField(
                                      controller: _unitController,
                                      validator: (val) => val.isEmpty
                                          ? 'Field is required'
                                          : null,
                                      keyboardType: TextInputType.number,
                                      decoration: InputDecoration(
                                        labelText: serviceDetail['orderType'],
                                        border: const OutlineInputBorder(),
                                        contentPadding:
                                            const EdgeInsets.all(10),
                                        isDense: true,
                                        hintText: 'Update KG/Pcs',
                                      ),
                                    ),
                                  ),
                                  const SizedBox(height: 5),
                                  Container(
                                    color: Colors.white,
                                    child: TextFormField(
                                      controller: _noteController,
                                      validator: (val) => val.isEmpty
                                          ? 'Field is required'
                                          : null,
                                      keyboardType: TextInputType.number,
                                      decoration: const InputDecoration(
                                        border: OutlineInputBorder(),
                                        isDense: true,
                                        labelText: 'Catatan',
                                        hintText: 'Catatan',
                                      ),
                                    ),
                                  ),
                                  Row(
                                    mainAxisAlignment: MainAxisAlignment.end,
                                    children: [
                                      ElevatedButton(
                                        onPressed: () {
                                          if (_formKey.currentState
                                              .validate()) {
                                            updateUnit(
                                                unit: _unitController.text,
                                                note: _noteController.text);
                                            setState(() {});
                                          }
                                        },
                                        child: const Text(
                                          'UPDATE',
                                          style: TextStyle(
                                            fontSize: 18,
                                            color: Colors.white,
                                            fontWeight: FontWeight.bold,
                                          ),
                                        ),
                                        style: ElevatedButton.styleFrom(
                                            elevation: 0,
                                            primary: primaryColor,
                                            side: const BorderSide(
                                              width: 1.0,
                                              color: primaryColor,
                                            )),
                                      ),
                                    ],
                                  )
                                ],
                              ),
                            ),
                          ],
                        ),
                      ),
                    ),
                    (serviceDetail['confirmed'] == false)
                        ? Padding(
                            padding: const EdgeInsets.fromLTRB(10, 0, 10, 20),
                            child: Row(
                              children: [
                                Expanded(
                                  child: ElevatedButton(
                                    onPressed: () {},
                                    child: const Text(
                                      'Tolak',
                                      style: TextStyle(
                                        fontSize: 18,
                                        color: primaryColor,
                                        fontWeight: FontWeight.bold,
                                      ),
                                    ),
                                    style: ElevatedButton.styleFrom(
                                        padding: const EdgeInsets.symmetric(
                                            vertical: 15),
                                        elevation: 0,
                                        primary: Colors.white,
                                        side: const BorderSide(
                                          width: 1.0,
                                          color: primaryColor,
                                        )),
                                  ),
                                ),
                                const SizedBox(width: 10),
                                Expanded(
                                  child: ElevatedButton(
                                    onPressed: () {
                                      confirmTransaction(context);
                                      setState(() {});
                                    },
                                    child: const Text('Konfirmasi'),
                                    style: ElevatedButton.styleFrom(
                                      padding: const EdgeInsets.symmetric(
                                          vertical: 15),
                                      elevation: 0,
                                      primary: primaryColor,
                                      textStyle: const TextStyle(
                                        fontSize: 18,
                                        fontWeight: FontWeight.bold,
                                      ),
                                    ),
                                  ),
                                ),
                              ],
                            ),
                          )
                        : const SizedBox(),
                  ],
                );
              } catch (e) {
                return const EmptyPage();
              }
            }
            return const Center(child: CircularProgressIndicator());
          },
          future: Future.wait([getTransactionDetail(), getStore()]),
        ),
      ),
    );
  }
}

class CustomerCard extends StatelessWidget {
  final String name, addres, thumbnail, phoneNumber;
  const CustomerCard({
    Key key,
    this.name,
    this.addres,
    this.thumbnail,
    this.phoneNumber,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: const EdgeInsets.fromLTRB(15, 0, 15, 10),
      width: double.infinity,
      child: Row(
        crossAxisAlignment: CrossAxisAlignment.center,
        children: [
          CircleAvatar(
            radius: 26,
            backgroundColor: Colors.grey[300],
            child: CircleAvatar(
              radius: 25,
              backgroundColor: Colors.white,
              backgroundImage: NetworkImage(thumbnail),
            ),
          ),
          const SizedBox(width: 10),
          Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Text(
                name,
                style:
                    const TextStyle(fontSize: 20, fontWeight: FontWeight.bold),
              ),
              Text(
                addres,
                style:
                    const TextStyle(fontSize: 16, fontWeight: FontWeight.w400),
              ),
            ],
          ),
          const Spacer(),
          ElevatedButton(
            onPressed: () {
              if (phoneNumber != null) {
                launch('tel:' + ((phoneNumber == null) ? '0' : phoneNumber));
              }
            },
            child: const Icon(
              Icons.phone_in_talk_outlined,
              color: primaryColor,
              size: 28,
            ),
            style: ElevatedButton.styleFrom(
              fixedSize: const Size(0, 50),
              tapTargetSize: MaterialTapTargetSize.shrinkWrap,
              padding: EdgeInsets.zero,
              elevation: 0,
              primary: Colors.white,
              side: const BorderSide(
                width: 1.5,
                color: primaryColor,
              ),
            ),
          )
        ],
      ),
    );
  }
}
