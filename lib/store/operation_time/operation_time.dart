import 'package:cleanis_store/common_widget/title_form.dart';
import 'package:cleanis_store/config/colors.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';

class OperationTime extends StatefulWidget {
  const OperationTime({Key key}) : super(key: key);

  @override
  _OperationTimeState createState() => _OperationTimeState();
}

class _OperationTimeState extends State<OperationTime> {
  final uid = FirebaseAuth.instance.currentUser.uid;
  final db = FirebaseFirestore.instance;

  Future loadOperationTime() async {
    final service = await db
        .collection('stores')
        .doc(uid)
        .collection('operationTime')
        .get();
    return service;
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: primaryColor,
        title: const Text('Operation Time'),
      ),
      body: FutureBuilder(
        builder: (context, snapshot) {
          if (snapshot.connectionState == ConnectionState.done) {
            final times = snapshot.data;
            return SingleChildScrollView(
              child: Column(
                children: [
                  MediaQuery.removePadding(
                    context: context,
                    removeTop: true,
                    child: ListView.builder(
                      physics: const NeverScrollableScrollPhysics(),
                      shrinkWrap: true,
                      itemCount: times.docs.length,
                      itemBuilder: (BuildContext context, int index) {
                        return ExpansionTile(
                          title: Text(
                            times.docs[index]['dayID'],
                            style: const TextStyle(
                              fontSize: 20,
                              fontWeight: FontWeight.w500,
                            ),
                          ),
                          children: [
                            Container(
                              padding: const EdgeInsets.symmetric(
                                horizontal: 15,
                              ),
                              width: double.infinity,
                              child: Row(
                                crossAxisAlignment: CrossAxisAlignment.start,
                                children: [
                                  Column(
                                    crossAxisAlignment:
                                        CrossAxisAlignment.start,
                                    children: [
                                      const Text(
                                        'Open At:',
                                        style: TextStyle(
                                            fontSize: 16,
                                            fontWeight: FontWeight.w500),
                                      ),
                                      Text(
                                        times.docs[index]['openHours'],
                                        style: const TextStyle(
                                            fontSize: 28,
                                            fontWeight: FontWeight.w600),
                                      )
                                    ],
                                  ),
                                  const SizedBox(width: 15),
                                  Column(
                                    crossAxisAlignment:
                                        CrossAxisAlignment.start,
                                    children: [
                                      const Text(
                                        'Close At:',
                                        style: TextStyle(
                                            fontSize: 16,
                                            fontWeight: FontWeight.w500),
                                      ),
                                      Text(
                                        times.docs[index]['closeHours'],
                                        style: const TextStyle(
                                            fontSize: 28,
                                            fontWeight: FontWeight.w600),
                                      )
                                    ],
                                  ),
                                  const Spacer(),
                                  TextButton.icon(
                                    icon: const Icon(
                                        Icons.event_available_outlined),
                                    label: const Text(
                                      'EDIT',
                                      style: TextStyle(
                                        fontSize: 16,
                                        fontWeight: FontWeight.bold,
                                      ),
                                    ),
                                    onPressed: () {
                                      showModalBottomSheet(
                                          context: context,
                                          backgroundColor: Colors.transparent,
                                          isDismissible: true,
                                          isScrollControlled: true,
                                          builder: (context) => EditHours(
                                                docID: times.docs[index].id,
                                                open: times.docs[index]
                                                    ['openHours'],
                                                close: times.docs[index]
                                                    ['closeHours'],
                                              )).then((_) => {setState(() {})});
                                    },
                                  )
                                ],
                              ),
                            ),
                          ],
                        );
                      },
                    ),
                  ),
                  const SizedBox(height: 20),
                ],
              ),
            );
          }
          return const Center(
            child: CircularProgressIndicator(),
          );
        },
        future: loadOperationTime(),
      ),
    );
  }
}

class EditHours extends StatefulWidget {
  final String docID, open, close;
  const EditHours({Key key, this.docID, this.open, this.close})
      : super(key: key);

  @override
  State<EditHours> createState() => _EditHoursState();
}

class _EditHoursState extends State<EditHours> {
  final uid = FirebaseAuth.instance.currentUser.uid;
  final db = FirebaseFirestore.instance;
  bool error = false;
  TimeOfDay _time = TimeOfDay.now();

  Future loadHours() async {
    final hours = await db
        .collection('stores')
        .doc(uid)
        .collection('operationTime')
        .doc(widget.docID)
        .get();
    return hours;
  }

  Future updateHours({
    String docId,
    String open,
    String close,
  }) {
    try {
      return db
          .collection('stores')
          .doc(uid)
          .collection('operationTime')
          .doc(widget.docID)
          .update({'closeHours': close, 'openHours': open});
    } catch (e) {
      return e.code;
    }
  }

  @override
  Widget build(BuildContext context) {
    final _openHoursController = TextEditingController()..text = widget.open;
    final _closeHoursController = TextEditingController()..text = widget.close;

    Future<void> _openTime(BuildContext context) async {
      final TimeOfDay _pickedTime = await showTimePicker(
        context: context,
        initialTime: _time,
        builder: (BuildContext context, Widget child) {
          return MediaQuery(
            data: MediaQuery.of(context).copyWith(alwaysUse24HourFormat: true),
            child: child,
          );
        },
      );

      if (_pickedTime != null) {
        _time = _pickedTime;
        var hour =
            (_time.hour < 9) ? ('0' + _time.hour.toString()) : _time.hour;
        var minute =
            (_time.minute < 9) ? ('0' + _time.minute.toString()) : _time.minute;
        _openHoursController.text = '$hour.$minute';
      }
    }

    Future<void> _closeTime(BuildContext context) async {
      final TimeOfDay _pickedTime = await showTimePicker(
        context: context,
        initialTime: _time,
        builder: (BuildContext context, Widget child) {
          return MediaQuery(
            data: MediaQuery.of(context).copyWith(alwaysUse24HourFormat: true),
            child: child,
          );
        },
      );

      if (_pickedTime != null) {
        _time = _pickedTime;
        var hour =
            (_time.hour < 9) ? ('0' + _time.hour.toString()) : _time.hour;
        var minute =
            (_time.minute < 9) ? ('0' + _time.minute.toString()) : _time.minute;
        _closeHoursController.text = '$hour.$minute';
      }
    }

    return SingleChildScrollView(
      child: Padding(
        padding:
            EdgeInsets.only(bottom: MediaQuery.of(context).viewInsets.bottom),
        child: Container(
          padding: const EdgeInsets.symmetric(vertical: 30, horizontal: 20),
          decoration: const BoxDecoration(
            color: Colors.white,
            borderRadius: BorderRadius.only(
              topLeft: Radius.circular(15.0),
              topRight: Radius.circular(15.0),
            ),
          ),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              (error == true)
                  ? const Text('Kolom tidak boleh kosong',
                      style: TextStyle(
                        color: Colors.red,
                        fontSize: 16,
                      ))
                  : const SizedBox(),
              Row(
                children: [
                  Expanded(
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        const TitleForm(title: "Jam Buka"),
                        Container(
                          color: Colors.white,
                          child: TextFormField(
                            controller: _openHoursController,
                            readOnly: true,
                            onTap: () async {
                              _openTime(context);
                            },
                            validator: (val) =>
                                val.isEmpty ? 'Tidak boleh kosong' : null,
                            decoration: const InputDecoration(
                              isDense: true,
                              contentPadding: EdgeInsets.only(left: 10),
                              border: OutlineInputBorder(),
                              suffixIcon: Icon(
                                Icons.schedule,
                                size: 18,
                              ),
                              labelText: 'Jam',
                            ),
                          ),
                        ),
                      ],
                    ),
                  ),
                  const SizedBox(width: 10),
                  Expanded(
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        const TitleForm(title: "Jam Tutup"),
                        Container(
                          color: Colors.white,
                          child: TextFormField(
                            controller: _closeHoursController,
                            readOnly: true,
                            onTap: () async {
                              _closeTime(context);
                            },
                            validator: (val) =>
                                val.isEmpty ? 'Tidak boleh kosong' : null,
                            decoration: const InputDecoration(
                              isDense: true,
                              contentPadding: EdgeInsets.only(left: 10),
                              border: OutlineInputBorder(),
                              suffixIcon: Icon(
                                Icons.schedule,
                                size: 18,
                              ),
                              labelText: 'Jam',
                            ),
                          ),
                        ),
                      ],
                    ),
                  ),
                ],
              ),
              ElevatedButton(
                child: const Text(
                  'Simpan',
                  style: TextStyle(
                      fontSize: 18,
                      fontWeight: FontWeight.bold,
                      color: Colors.white),
                ),
                onPressed: () {
                  updateHours(
                      open: _openHoursController.text,
                      close: _closeHoursController.text);
                  Navigator.of(context).pop();
                  setState(() {});
                },
                style: ButtonStyle(
                  backgroundColor:
                      MaterialStateProperty.all<Color>(primaryColor),
                ),
              )
            ],
          ),
        ),
      ),
    );
  }
}
