import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:cleanis_store/config/colors.dart';
import 'package:geolocator/geolocator.dart';
import 'package:universe/universe.dart';

import 'widget/button_submit_widget.dart';

class StoreForm extends StatefulWidget {
  const StoreForm({Key key}) : super(key: key);

  @override
  _StoreFormState createState() => _StoreFormState();
}

class _StoreFormState extends State<StoreForm> {
  var appBar = AppBar();
  bool loading = false;

  final _mapKey = UniqueKey();
  List<Marker> markers = [];

  Position position;

  getLocation() async {
    bool serviceEnabled;
    LocationPermission permission;

    serviceEnabled = await Geolocator.isLocationServiceEnabled();
    if (!serviceEnabled) {
      // Location services are not enabled don't continue
      // accessing the position and request users of the
      // App to enable the location services.
      return Future.error('Location services are disabled.');
    }

    permission = await Geolocator.checkPermission();
    if (permission == LocationPermission.denied) {
      permission = await Geolocator.requestPermission();
      if (permission == LocationPermission.denied) {
        // Permissions are denied, next time you could try
        // requesting permissions again (this is also where
        // Android's shouldShowRequestPermissionRationale
        // returned true. According to Android guidelines
        // your App should show an explanatory UI now.
        return Future.error('Location permissions are denied');
      }
    }

    if (permission == LocationPermission.deniedForever) {
      // Permissions are denied forever, handle appropriately.
      return Future.error(
          'Location permissions are permanently denied, we cannot request permissions.');
    }

    position = await Geolocator.getCurrentPosition(
        desiredAccuracy: LocationAccuracy.best);
    return position;
  }

  void addressInfo() {
    WidgetsBinding.instance.addPostFrameCallback((_) async {
      await showDialog<String>(
        context: context,
        builder: (BuildContext context) => AlertDialog(
          title: const Text(
            "Menambah toko baru?",
            style: TextStyle(fontSize: 22, fontWeight: FontWeight.bold),
          ),
          content: const Text(
            "Ketik atau tap pada peta lokasi yang anda inginkan, dan isi form yang ditampilkan.\nJangan lupa, pastikan lokasi sudah benar dan presisi.",
            style: TextStyle(fontSize: 16, fontWeight: FontWeight.bold),
          ),
          actions: <Widget>[
            ElevatedButton(
              child: const Text("OK"),
              onPressed: () {
                Navigator.of(context).pop();
              },
            ),
          ],
        ),
      );
    });
  }

  @override
  void initState() {
    super.initState();
    getLocation();
    addressInfo();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      resizeToAvoidBottomInset: false,
      appBar: AppBar(
        backgroundColor: primaryColor,
        title: const Text('Create Store'),
        elevation: 0,
        actions: <Widget>[
          IconButton(
            padding: const EdgeInsets.only(right: 20),
            icon: const Icon(
              Icons.info_outline_rounded,
              color: Colors.white,
            ),
            onPressed: () {
              addressInfo();
            },
          ),
        ],
      ),
      body: FutureBuilder(
        builder: (context, snapshot) {
          if (snapshot.connectionState == ConnectionState.done) {
            return U.MapBox(
              live: true,
              accessToken:
                  'pk.eyJ1IjoienllbCIsImEiOiJja3ZoY3RraWI1eGE4MzFxd21paGlyZGQ5In0.-Zcr5egbG9bgHhq856uN5Q',
              key: _mapKey,
              center: [
                position.latitude ?? 1.0073318019947193,
                position.longitude ?? 102.71110037148851
              ],
              showLocationIndicator: true,
              zoom: 15,
              markers: U.MarkerLayer(
                markers,
                widget: const MarkerImage('assets/images/store_marker.png'),
                onLongPress: (position, latlng) {
                  if (latlng is LatLng && mounted) {
                    setState(() {
                      markers.removeWhere((marker) => marker.latlng == latlng);
                    });
                  }
                },
              ),
              onTap: (latlng) {
                if (markers.isEmpty) {
                  if (mounted) {
                    setState(() {
                      Marker marker = U.Marker(latlng, data: latlng);
                      markers.add(marker);
                    });
                  }
                } else if (markers.isNotEmpty) {
                  markers.remove(markers.first);
                  setState(() {
                    Marker marker = U.Marker(latlng, data: latlng);
                    markers.add(marker);
                  });
                }
                Future.delayed(const Duration(milliseconds: 300), () {
                  showModalBottomSheet(
                    context: context,
                    backgroundColor: Colors.transparent,
                    isDismissible: true,
                    isScrollControlled: true,
                    builder: (context) => AddressForm(
                        isLoading: false, lat: latlng.lat, lng: latlng.lng),
                  );
                });
              },
            );
          }
          return const Center(
            child: CircularProgressIndicator(),
          );
        },
        future: getLocation(),
      ),
    );
  }
}

final _formKey = GlobalKey<FormState>();

class AddressForm extends StatelessWidget {
  final _nameController = TextEditingController();
  final _addressController = TextEditingController();
  final _numberPhoneController = TextEditingController();
  final _descrptionController = TextEditingController();
  final uid = FirebaseAuth.instance.currentUser.uid;
  final db = FirebaseFirestore.instance;
  final double lat;
  final double lng;

  final bool isLoading;

  getDay(day) {
    String hari;
    switch (day) {
      case 1:
        {
          hari = "Senin";
        }
        break;

      case 2:
        {
          hari = "Selasa";
        }
        break;

      case 3:
        {
          hari = "Rabu";
        }
        break;

      case 4:
        {
          hari = "Kamis";
        }
        break;
      case 5:
        {
          hari = "Jumat";
        }
        break;
      case 6:
        {
          hari = "Sabtu";
        }
        break;
      case 7:
        {
          hari = "Minggu";
        }
        break;

      default:
        {
          hari = ("Invalid");
        }
        break;
    }
    return hari;
  }

  Future createStore({
    String name,
    String address,
    String about,
    String numberPhone,
    double lat,
    double lng,
  }) {
    try {
      // uid
      return db.collection('stores').doc(uid).set({
        'about': about,
        'address': address,
        'isVerified': false,
        'lat': lat.toString(),
        'lng': lng.toString(),
        'location': GeoPoint(lat, lng),
        'name': name,
        'numberPhone': numberPhone,
        'rate': '0',
        'thumbnail':
            'https://ik.imagekit.io/n0t5masg5jg/stores/default_store_iRhg15nJP.jpg?updatedAt=1638677916449',
        'created_at': FieldValue.serverTimestamp()
      }).then((value) {
        for (int i = 1; i <= 7; i++) {
          db
              .collection('stores')
              .doc(uid)
              .collection('operationTime')
              .doc(i.toString())
              .set({
            'closeHours': '20.00',
            'day': i.toString(), // 1 - 7 for weekday
            'dayID': getDay(i),
            'openHours': '08.00'
          });
        }
      });
    } catch (e) {
      return e.code;
    }
  }

  AddressForm({key, this.isLoading, this.lat, this.lng}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return SingleChildScrollView(
      child: Padding(
        padding:
            EdgeInsets.only(bottom: MediaQuery.of(context).viewInsets.bottom),
        child: Container(
          padding: const EdgeInsets.symmetric(vertical: 30, horizontal: 20),
          decoration: const BoxDecoration(
            color: Colors.white,
            borderRadius: BorderRadius.only(
              topLeft: Radius.circular(15.0),
              topRight: Radius.circular(15.0),
            ),
          ),
          child: (isLoading)
              ? const CircularProgressIndicator()
              : Form(
                  key: _formKey,
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    mainAxisSize: MainAxisSize.min,
                    children: [
                      const Text(
                        'Buka Toko Baru',
                        style: TextStyle(
                          fontSize: 24,
                          fontWeight: FontWeight.w500,
                        ),
                      ),
                      const Text(
                        'Pastikan pin anda sudah sesuai dengan lokasi',
                        style: TextStyle(fontSize: 14),
                      ),
                      const SizedBox(height: 25),
                      Container(
                        color: Colors.white,
                        child: TextFormField(
                          validator: (val) =>
                              val.isEmpty ? 'Name store is required' : null,
                          controller: _nameController,
                          keyboardType: TextInputType.text,
                          decoration: const InputDecoration(
                            border: OutlineInputBorder(),
                            contentPadding: EdgeInsets.symmetric(
                                vertical: 10.0, horizontal: 10.0),
                            labelText: 'Nama Toko',
                          ),
                        ),
                      ),
                      const SizedBox(height: 15),
                      Container(
                        color: Colors.white,
                        child: TextFormField(
                          validator: (val) =>
                              val.isEmpty ? 'Your address is required' : null,
                          controller: _addressController,
                          keyboardType: TextInputType.text,
                          decoration: const InputDecoration(
                            contentPadding: EdgeInsets.symmetric(
                                vertical: 10.0, horizontal: 10.0),
                            hintText: 'Alamat',
                            border: OutlineInputBorder(),
                            labelText: 'Alamat',
                          ),
                        ),
                      ),
                      const SizedBox(height: 15),
                      Container(
                        color: Colors.white,
                        child: TextFormField(
                          validator: (val) =>
                              val.isEmpty ? 'Phone is required' : null,
                          controller: _numberPhoneController,
                          keyboardType: TextInputType.text,
                          decoration: const InputDecoration(
                            contentPadding: EdgeInsets.symmetric(
                                vertical: 10.0, horizontal: 10.0),
                            hintText: 'Nomor HP',
                            border: OutlineInputBorder(),
                            labelText: 'Nomor telepon aktif',
                          ),
                        ),
                      ),
                      const SizedBox(height: 15),
                      Container(
                        color: Colors.white,
                        child: TextFormField(
                          validator: (val) =>
                              val.isEmpty ? 'Deskripsi is required' : null,
                          controller: _descrptionController,
                          minLines: 3,
                          maxLines: 5,
                          keyboardType: TextInputType.text,
                          decoration: const InputDecoration(
                            contentPadding: EdgeInsets.symmetric(
                                vertical: 10.0, horizontal: 10.0),
                            hintText: 'Deskripsi',
                            alignLabelWithHint: true,
                            border: OutlineInputBorder(),
                            labelText: 'Informasi toko anda',
                          ),
                        ),
                      ),
                      const SizedBox(height: 15),
                      ButtonSubmit(
                        onPress: () async {
                          if (_formKey.currentState.validate()) {
                            createStore(
                              name: _nameController.text,
                              about: _descrptionController.text,
                              address: _addressController.text,
                              numberPhone: _numberPhoneController.text,
                              lat: lat,
                              lng: lng,
                            );
                            int count = 0;
                            Navigator.popUntil(context, (route) {
                              return count++ == 2;
                            });
                          }
                        },
                        text: 'SUBMIT',
                      )
                    ],
                  ),
                ),
        ),
      ),
    );
  }
}
