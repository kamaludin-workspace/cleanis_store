import 'package:cleanis_store/config/colors.dart';
import 'package:flutter/material.dart';

class VoucherCard extends StatelessWidget {
  final String bigTitle, title, voucher, date;

  const VoucherCard({
    Key key,
    this.bigTitle,
    this.title,
    this.voucher,
    this.date,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      margin: const EdgeInsets.fromLTRB(10, 10, 10, 0),
      width: double.infinity,
      height: 160,
      decoration: BoxDecoration(
        color: Colors.white,
        borderRadius: const BorderRadius.all(
          Radius.circular(8.0),
        ),
        border: Border.all(
          width: 1,
          color: Colors.grey[300],
        ),
      ),
      child: Column(
        children: <Widget>[
          Container(
            height: 95,
            decoration: BoxDecoration(
              borderRadius: const BorderRadius.only(
                topRight: Radius.circular(8),
                topLeft: Radius.circular(8),
              ),
              image: DecorationImage(
                image: AssetImage('assets/images/$voucher'),
                fit: BoxFit.cover,
              ),
            ),
            child: Container(
              width: double.infinity,
              padding: const EdgeInsets.fromLTRB(9, 25, 100, 20),
              child: Text(
                bigTitle,
                style: const TextStyle(
                  fontFamily: 'NunitoBold',
                  color: Colors.black,
                  fontSize: 22,
                  fontWeight: FontWeight.w900,
                ),
              ),
            ),
          ),
          Container(
            height: 55,
            decoration: BoxDecoration(
              border: Border(
                top: BorderSide(
                  width: 0.8,
                  color: Colors.grey[300],
                ),
              ),
            ),
            padding: const EdgeInsets.fromLTRB(10, 5, 10, 0),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Expanded(
                  flex: 3,
                  child: Container(
                    alignment: Alignment.centerLeft,
                    child: Text(
                      title,
                      style: const TextStyle(
                        fontSize: 15,
                        height: 1.1,
                        fontWeight: FontWeight.w700,
                      ),
                    ),
                  ),
                ),
                Row(
                  children: [
                    Container(
                      margin: const EdgeInsets.only(bottom: 2),
                      child: const Icon(
                        Icons.calendar_today,
                        size: 11,
                        color: primaryColor,
                      ),
                    ),
                    const SizedBox(width: 5),
                    Text(
                      'Berlaku hingga $date',
                      style: const TextStyle(
                        fontSize: 12,
                        fontWeight: FontWeight.w300,
                      ),
                    ),
                  ],
                ),
              ],
            ),
          ),
        ],
      ),
    );
  }
}
