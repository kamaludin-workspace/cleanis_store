import 'package:cleanis_store/config/colors.dart';
import 'package:flutter/material.dart';
import 'package:flutter_rating_bar/flutter_rating_bar.dart';
import 'package:percent_indicator/linear_percent_indicator.dart';

class RatingScores extends StatelessWidget {
  final String starCount, reviewCount;
  final double star;
  const RatingScores({
    Key key,
    this.starCount,
    this.reviewCount,
    this.star,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return SizedBox(
      height: 120,
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Text(
            starCount,
            style: const TextStyle(
              fontSize: 60,
              fontWeight: FontWeight.w900,
            ),
          ),
          RatingBarIndicator(
            rating: star,
            itemBuilder: (context, index) => const Icon(
              Icons.star,
              color: primaryColor,
            ),
            itemCount: 5,
            itemSize: 25,
            direction: Axis.horizontal,
          ),
          const SizedBox(height: 3),
          Text(reviewCount)
        ],
      ),
    );
  }
}

class PercentageScore extends StatelessWidget {
  final double percentFive, percentFour, percentThree, percentTwo, percentOne;
  const PercentageScore({
    Key key,
    this.percentFive,
    this.percentFour,
    this.percentThree,
    this.percentTwo,
    this.percentOne,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      width: double.infinity,
      padding: const EdgeInsets.only(left: 10),
      height: 120,
      child: Column(
        mainAxisAlignment: MainAxisAlignment.spaceEvenly,
        children: [
          LinearPercentIndicator(
            leading: const Text('5'),
            lineHeight: 10.0,
            percent: percentFive,
            progressColor: primaryColor,
          ),
          LinearPercentIndicator(
            leading: const Text('4'),
            lineHeight: 10.0,
            percent: percentFour,
            progressColor: primaryColor,
          ),
          LinearPercentIndicator(
            leading: const Text('3'),
            lineHeight: 10.0,
            percent: percentThree,
            progressColor: primaryColor,
          ),
          LinearPercentIndicator(
            leading: const Text('2'),
            lineHeight: 10.0,
            percent: percentTwo,
            progressColor: primaryColor,
          ),
          LinearPercentIndicator(
            leading: const Text('1'),
            lineHeight: 10.0,
            percent: percentOne,
            progressColor: primaryColor,
          ),
        ],
      ),
    );
  }
}
