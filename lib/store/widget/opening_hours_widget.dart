import 'package:flutter/material.dart';

class OpeningHours extends StatelessWidget {
  final String day;
  final Color status;
  final String hour;

  const OpeningHours({
    Key key,
    this.day,
    this.hour,
    this.status,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: const EdgeInsets.only(bottom: 5),
      child: Row(
        children: [
          const SizedBox(width: 2),
          MyBullet(
            height: 7,
            width: 7,
            status: status,
          ),
          const SizedBox(width: 2),
          SizedBox(
            width: 100,
            child: Text(
              day,
              style: const TextStyle(fontSize: 16),
            ),
          ),
          const Text(
            ':',
            style: TextStyle(fontSize: 16),
          ),
          Container(width: 20),
          Text(
            hour,
            style: const TextStyle(fontSize: 16),
          )
        ],
      ),
    );
  }
}

class MyBullet extends StatelessWidget {
  final double height;
  final double width;
  final Color status;

  const MyBullet({Key key, this.height, this.width, this.status})
      : super(key: key);
  @override
  Widget build(BuildContext context) {
    return Container(
      height: height,
      width: width,
      margin: const EdgeInsets.only(right: 5),
      decoration: BoxDecoration(
        color: status,
        shape: BoxShape.circle,
      ),
    );
  }
}
