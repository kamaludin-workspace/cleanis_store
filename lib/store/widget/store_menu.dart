import 'package:cleanis_store/store/information/information_edit.dart';
import 'package:cleanis_store/store/offers/offers_screen.dart';
import 'package:cleanis_store/store/rating_reviews.dart';
import 'package:cleanis_store/store/services/services_screen.dart';
import 'package:flutter/material.dart';

class StoreMenu extends StatelessWidget {
  final String firestoreDocID;
  const StoreMenu({
    Key key,
    this.firestoreDocID,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return ExpansionTile(
      title: const Text(
        'Menu',
        style: TextStyle(
          fontSize: 22,
          fontWeight: FontWeight.bold,
        ),
      ),
      children: [
        Container(
          padding: const EdgeInsets.fromLTRB(10, 0, 10, 20),
          child: Row(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Expanded(
                child: Column(
                  children: [
                    Container(
                      width: double.infinity,
                      height: 120,
                      padding: const EdgeInsets.symmetric(
                          horizontal: 15, vertical: 10),
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: [
                          Row(
                            children: [
                              Icon(Icons.info_outline_rounded,
                                  size: 36, color: Colors.grey[100]),
                              const Spacer(),
                              TextButton(
                                style: ElevatedButton.styleFrom(
                                  tapTargetSize:
                                      MaterialTapTargetSize.shrinkWrap,
                                  minimumSize: Size.zero,
                                  shape: const CircleBorder(),
                                ),
                                child: const Icon(
                                  Icons.east_rounded,
                                  size: 28,
                                  color: Colors.white,
                                ),
                                onPressed: () {
                                  Navigator.push(
                                    context,
                                    MaterialPageRoute(
                                      builder: (context) => InformationEdit(
                                        docID: firestoreDocID,
                                      ),
                                    ),
                                  );
                                },
                              ),
                            ],
                          ),
                          const Text(
                            'Store Informations',
                            style: TextStyle(
                              height: 1.0,
                              fontSize: 20,
                              color: Colors.white,
                              fontWeight: FontWeight.bold,
                            ),
                          )
                        ],
                      ),
                      decoration: BoxDecoration(
                        borderRadius:
                            const BorderRadius.all(Radius.circular(20)),
                        boxShadow: [
                          BoxShadow(
                            color: Colors.teal[500],
                            offset: const Offset(0, 10),
                            blurRadius: 10,
                            spreadRadius: -5,
                          ),
                        ],
                        gradient: LinearGradient(
                          begin: Alignment.topLeft,
                          end: Alignment.bottomCenter,
                          colors: [
                            Colors.teal[200],
                            Colors.teal[300],
                            Colors.teal[500],
                            Colors.teal[500]
                          ],
                          stops: const [0.1, 0.3, 0.9, 1.0],
                        ),
                      ),
                    ),
                    const SizedBox(height: 10),
                    Container(
                      width: double.infinity,
                      height: 90,
                      padding: const EdgeInsets.symmetric(
                          horizontal: 15, vertical: 10),
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: [
                          Row(
                            children: [
                              Icon(Icons.shopping_bag_outlined,
                                  size: 32, color: Colors.grey[100]),
                              const Spacer(),
                              TextButton(
                                style: ElevatedButton.styleFrom(
                                  tapTargetSize:
                                      MaterialTapTargetSize.shrinkWrap,
                                  minimumSize: Size.zero,
                                  shape: const CircleBorder(),
                                ),
                                child: const Icon(
                                  Icons.east_rounded,
                                  size: 28,
                                  color: Colors.white,
                                ),
                                onPressed: () {
                                  Navigator.push(
                                    context,
                                    MaterialPageRoute(
                                      builder: (context) =>
                                          const ServicesScreen(),
                                    ),
                                  );
                                },
                              ),
                            ],
                          ),
                          const Text(
                            'Services',
                            style: TextStyle(
                              fontSize: 20,
                              color: Colors.white,
                              fontWeight: FontWeight.bold,
                            ),
                          )
                        ],
                      ),
                      decoration: BoxDecoration(
                          borderRadius:
                              const BorderRadius.all(Radius.circular(20)),
                          boxShadow: [
                            BoxShadow(
                              color: Colors.blue[400],
                              offset: const Offset(0, 10),
                              blurRadius: 10,
                              spreadRadius: -5,
                            ),
                          ],
                          gradient: LinearGradient(
                              begin: Alignment.topLeft,
                              end: Alignment.bottomCenter,
                              colors: [
                                Colors.blue[200],
                                Colors.blue[300],
                                Colors.blue[500],
                                Colors.blue[500],
                              ],
                              stops: const [
                                0.1,
                                0.3,
                                0.9,
                                1.0
                              ])),
                    ),
                  ],
                ),
              ),
              const SizedBox(width: 10),
              Expanded(
                child: Column(
                  children: [
                    Container(
                      width: double.infinity,
                      height: 90,
                      padding: const EdgeInsets.symmetric(
                          horizontal: 15, vertical: 10),
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: [
                          Row(
                            children: [
                              Icon(Icons.local_offer_outlined,
                                  size: 32, color: Colors.grey[100]),
                              const Spacer(),
                              TextButton(
                                style: ElevatedButton.styleFrom(
                                  tapTargetSize:
                                      MaterialTapTargetSize.shrinkWrap,
                                  minimumSize: Size.zero,
                                  shape: const CircleBorder(),
                                ),
                                child: const Icon(
                                  Icons.east_rounded,
                                  size: 28,
                                  color: Colors.white,
                                ),
                                onPressed: () {
                                  Navigator.push(
                                    context,
                                    MaterialPageRoute(
                                      builder: (context) =>
                                          const OffersScreen(),
                                    ),
                                  );
                                },
                              ),
                            ],
                          ),
                          const Text(
                            'Offers',
                            style: TextStyle(
                              fontSize: 20,
                              color: Colors.white,
                              fontWeight: FontWeight.bold,
                            ),
                          )
                        ],
                      ),
                      decoration: BoxDecoration(
                        borderRadius:
                            const BorderRadius.all(Radius.circular(20)),
                        boxShadow: [
                          BoxShadow(
                            color: Colors.indigo[400],
                            offset: const Offset(0, 10),
                            blurRadius: 10,
                            spreadRadius: -5,
                          ),
                        ],
                        gradient: LinearGradient(
                          begin: Alignment.topLeft,
                          end: Alignment.bottomCenter,
                          colors: [
                            Colors.indigo[200],
                            Colors.indigo[300],
                            Colors.indigo[400],
                            Colors.indigo[400],
                          ],
                          stops: const [0.1, 0.3, 0.9, 1.0],
                        ),
                      ),
                    ),
                    const SizedBox(height: 10),
                    Container(
                      width: double.infinity,
                      height: 120,
                      padding: const EdgeInsets.symmetric(
                          horizontal: 15, vertical: 10),
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: [
                          Row(
                            children: [
                              Icon(Icons.question_answer_outlined,
                                  size: 32, color: Colors.grey[100]),
                              const Spacer(),
                              TextButton(
                                style: ElevatedButton.styleFrom(
                                  tapTargetSize:
                                      MaterialTapTargetSize.shrinkWrap,
                                  minimumSize: Size.zero,
                                  shape: const CircleBorder(),
                                ),
                                child: const Icon(
                                  Icons.east_rounded,
                                  size: 28,
                                  color: Colors.white,
                                ),
                                onPressed: () {
                                  Navigator.push(
                                    context,
                                    MaterialPageRoute(
                                      builder: (context) => RatingAndReviews(
                                        firestoreDocID: firestoreDocID,
                                      ),
                                    ),
                                  );
                                },
                              ),
                            ],
                          ),
                          const Text(
                            'Ratings & Reviews',
                            style: TextStyle(
                              height: 1.0,
                              fontSize: 20,
                              color: Colors.white,
                              fontWeight: FontWeight.bold,
                            ),
                          )
                        ],
                      ),
                      decoration: BoxDecoration(
                        borderRadius:
                            const BorderRadius.all(Radius.circular(20)),
                        boxShadow: [
                          BoxShadow(
                            color: Colors.blueGrey[400],
                            offset: const Offset(0, 10),
                            blurRadius: 15,
                            spreadRadius: -5,
                          ),
                        ],
                        gradient: LinearGradient(
                          begin: Alignment.topLeft,
                          end: Alignment.bottomCenter,
                          colors: [
                            Colors.blueGrey[200],
                            Colors.blueGrey[300],
                            Colors.blueGrey[400],
                            Colors.blueGrey[400],
                          ],
                          stops: const [0.1, 0.3, 0.9, 1.0],
                        ),
                      ),
                    ),
                  ],
                ),
              ),
            ],
          ),
        ),
      ],
    );
  }
}
