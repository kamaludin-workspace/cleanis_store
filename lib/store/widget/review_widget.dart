import 'package:cleanis_store/config/colors.dart';
import 'package:flutter/material.dart';
import 'package:flutter_rating_bar/flutter_rating_bar.dart';

class Riview extends StatelessWidget {
  final String name, date, comment, image;
  final double rate;
  const Riview({
    Key key,
    this.name,
    this.date,
    this.comment,
    this.image,
    this.rate,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      decoration: BoxDecoration(
        color: Colors.white,
        borderRadius: const BorderRadius.all(
          Radius.circular(8.0),
        ),
        border: Border.all(
          width: 1,
          color: Colors.grey[300],
        ),
      ),
      padding: const EdgeInsets.all(10),
      margin: const EdgeInsets.only(top: 10),
      width: double.infinity,
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Row(
            children: [
              CircleAvatar(
                radius: 26,
                backgroundColor: Colors.grey[300],
                child: CircleAvatar(
                  radius: 25,
                  backgroundColor: Colors.white,
                  backgroundImage: NetworkImage(image),
                ),
              ),
              Container(
                margin: const EdgeInsets.symmetric(horizontal: 10),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Text(
                      name,
                      style: const TextStyle(
                          fontSize: 16, fontWeight: FontWeight.w700),
                    ),
                    Row(
                      children: [
                        RatingBarIndicator(
                          rating: rate,
                          itemBuilder: (context, index) => const Icon(
                            Icons.star,
                            color: primaryColor,
                          ),
                          itemCount: 5,
                          itemSize: 15,
                          direction: Axis.horizontal,
                        ),
                        const SizedBox(width: 5),
                        Text(
                          date,
                          style: const TextStyle(fontSize: 14),
                        )
                      ],
                    )
                  ],
                ),
              )
            ],
          ),
          const Divider(),
          const SizedBox(height: 5),
          Text(comment),
        ],
      ),
    );
  }
}
