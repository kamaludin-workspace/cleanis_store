import 'package:cleanis_store/config/colors.dart';
import 'package:flutter/material.dart';

class StoreButtonAction extends StatelessWidget {
  final String title;
  final Icon icon;
  final VoidCallback onPressed;
  const StoreButtonAction({
    Key key,
    this.title,
    this.icon,
    this.onPressed,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Column(
      children: <Widget>[
        Material(
          color: Colors.white,
          child: Center(
            child: Ink(
              decoration: const ShapeDecoration(
                color: Colors.white,
                shape: CircleBorder(),
              ),
              child: IconButton(
                icon: icon,
                color: primaryColor,
                onPressed: onPressed,
              ),
            ),
          ),
        ),
        Text(
          title,
          style: const TextStyle(
            fontSize: 14,
            fontWeight: FontWeight.w500,
          ),
        )
      ],
    );
  }
}
