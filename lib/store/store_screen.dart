import 'dart:io';

import 'package:cleanis_store/config/colors.dart';
import 'package:cleanis_store/config/styles.dart';
import 'package:cleanis_store/store/store_form.dart';
import 'package:cleanis_store/store/widget/store_menu.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_storage/firebase_storage.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:image_picker/image_picker.dart';

import '../loading.dart';
import 'operation_time/operation_time.dart';
import 'preview_store.dart';
import 'widget/opening_hours_widget.dart';
import 'widget/store_information_widget.dart';

class StoreScreen extends StatefulWidget {
  const StoreScreen({Key key}) : super(key: key);

  @override
  _StoreScreenState createState() => _StoreScreenState();
}

class _StoreScreenState extends State<StoreScreen> {
  final uid = FirebaseAuth.instance.currentUser.uid;
  final db = FirebaseFirestore.instance;

  bool loadingUpload = false;

  Future getStore() async {
    final store =
        await FirebaseFirestore.instance.collection('stores').doc(uid).get();
    return store;
  }

  Future getListOperationTime() async {
    final aboutStore = await FirebaseFirestore.instance
        .collection('stores')
        .doc(uid)
        .collection('operationTime')
        .get();
    return aboutStore;
  }

  final picker = ImagePicker();
  File _image;

  Future getImage() async {
    final pickedFile = await picker.pickImage(source: ImageSource.gallery);
    setState(() {
      if (pickedFile != null) _image = File(pickedFile.path);
    });
  }

  void snackBarAction(title) {
    final snackBar = SnackBar(
      content: Text(title),
      action: SnackBarAction(
        label: 'Close',
        onPressed: () {
          ScaffoldMessenger.of(context).hideCurrentSnackBar();
        },
      ),
    );
    ScaffoldMessenger.of(context).showSnackBar(snackBar);
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: (loadingUpload)
          ? const Loading()
          : SingleChildScrollView(
              child: FutureBuilder(
                builder: (context, snapshot) {
                  if (snapshot.connectionState == ConnectionState.done) {
                    final store = snapshot.data[0];
                    if (snapshot.hasData) {
                      try {
                        return Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            Container(
                              width: double.infinity,
                              height: MediaQuery.of(context).size.width * 0.50,
                              decoration: BoxDecoration(
                                color: Colors.blue[200],
                                image: DecorationImage(
                                  image: _image != null
                                      ? FileImage(_image)
                                      : NetworkImage(store['thumbnail'],
                                          scale: 1.0),
                                  fit: BoxFit.cover,
                                ),
                              ),
                              alignment: Alignment.bottomCenter,
                              child: Container(
                                padding: const EdgeInsets.symmetric(
                                    horizontal: 15, vertical: 5),
                                color: Colors.black87.withOpacity(0.5),
                                child: Row(
                                  children: [
                                    Expanded(
                                      flex: 4,
                                      child: Text(
                                        store['name'],
                                        overflow: TextOverflow.ellipsis,
                                        maxLines: 1,
                                        style: TextStyle(
                                            color: Colors.grey[100],
                                            fontSize: 24,
                                            fontWeight: FontWeight.bold),
                                      ),
                                    ),
                                    const Spacer(),
                                    // UPLOAD IMAGE
                                    ElevatedButton(
                                      style: ElevatedButton.styleFrom(
                                        tapTargetSize:
                                            MaterialTapTargetSize.shrinkWrap,
                                        minimumSize: Size.zero,
                                        padding: EdgeInsets.zero,
                                        shape: const CircleBorder(),
                                      ),
                                      child: Ink(
                                        decoration: BoxDecoration(
                                            gradient: LinearGradient(
                                              begin: Alignment.topLeft,
                                              end: Alignment.bottomCenter,
                                              colors: [
                                                Colors.blue[200],
                                                Colors.blue[300],
                                                Colors.blue[500],
                                                Colors.blue[500],
                                              ],
                                              stops: const [0.1, 0.3, 0.9, 1.0],
                                            ),
                                            borderRadius:
                                                BorderRadius.circular(20)),
                                        child: const SizedBox(
                                          height: 40,
                                          width: 40,
                                          child: Icon(
                                            Icons.camera_alt_outlined,
                                            size: 18,
                                          ),
                                        ),
                                      ),
                                      onPressed: () {
                                        // Update photo
                                        getImage();
                                      },
                                    ),
                                    const SizedBox(width: 5),
                                    (_image != null)
                                        ? ElevatedButton(
                                            style: ElevatedButton.styleFrom(
                                              tapTargetSize:
                                                  MaterialTapTargetSize
                                                      .shrinkWrap,
                                              minimumSize: Size.zero,
                                              padding: EdgeInsets.zero,
                                              shape: const CircleBorder(),
                                            ),
                                            child: Ink(
                                              decoration: BoxDecoration(
                                                  gradient: LinearGradient(
                                                    begin: Alignment.topLeft,
                                                    end: Alignment.bottomCenter,
                                                    colors: [
                                                      Colors.green[200],
                                                      Colors.green[300],
                                                      Colors.green[500],
                                                      Colors.green[500],
                                                    ],
                                                    stops: const [
                                                      0.1,
                                                      0.3,
                                                      0.9,
                                                      1.0
                                                    ],
                                                  ),
                                                  borderRadius:
                                                      BorderRadius.circular(
                                                          20)),
                                              child: Container(
                                                padding:
                                                    const EdgeInsets.symmetric(
                                                        vertical: 10,
                                                        horizontal: 6),
                                                child: const Text(
                                                  'SIMPAN',
                                                  style: TextStyle(
                                                      fontSize: 16,
                                                      fontWeight:
                                                          FontWeight.bold),
                                                ),
                                              ),
                                            ),
                                            onPressed: () async {
                                              // Update photo
                                              setState(() {
                                                loadingUpload = true;
                                              });
                                              var storeRef = FirebaseStorage
                                                  .instance
                                                  .ref('storeThumbnail')
                                                  .child(uid);

                                              var uploadTask =
                                                  storeRef.putFile(_image);
                                              var completePut =
                                                  await uploadTask;
                                              String downloadUrl =
                                                  await completePut.ref
                                                      .getDownloadURL();
                                              db
                                                  .collection('stores')
                                                  .doc(uid)
                                                  .update({
                                                'thumbnail': downloadUrl
                                              });
                                              setState(() {
                                                _image = null;
                                                loadingUpload = false;
                                                Future.delayed(
                                                    const Duration(
                                                        milliseconds: 500), () {
                                                  snackBarAction(
                                                      'Photo Profil Diperbarui');
                                                });
                                              });
                                            },
                                          )
                                        : ElevatedButton(
                                            style: ElevatedButton.styleFrom(
                                              shape: const CircleBorder(),
                                              minimumSize: Size.zero,
                                              padding: EdgeInsets.zero,
                                              tapTargetSize:
                                                  MaterialTapTargetSize
                                                      .shrinkWrap,
                                            ),
                                            child: Ink(
                                              decoration: BoxDecoration(
                                                  gradient: LinearGradient(
                                                    begin: Alignment.topLeft,
                                                    end: Alignment.bottomCenter,
                                                    colors: [
                                                      Colors.blue[200],
                                                      Colors.blue[300],
                                                      Colors.blue[500],
                                                      Colors.blue[500],
                                                    ],
                                                    stops: const [
                                                      0.1,
                                                      0.3,
                                                      0.9,
                                                      1.0
                                                    ],
                                                  ),
                                                  borderRadius:
                                                      BorderRadius.circular(
                                                          20)),
                                              child: const SizedBox(
                                                height: 40,
                                                width: 40,
                                                child: Icon(
                                                  Icons.visibility_outlined,
                                                  size: 18,
                                                ),
                                              ),
                                            ),
                                            onPressed: () {
                                              Navigator.push(
                                                context,
                                                MaterialPageRoute(
                                                  builder: (context) =>
                                                      PreviewStore(
                                                    firestoreDocID: store.id,
                                                  ),
                                                ),
                                              );
                                            },
                                          ),
                                  ],
                                ),
                              ),
                            ),
                            StoreMenu(
                              firestoreDocID: store.id,
                            ),
                            StoreInformation(
                              name: store['name'],
                              about: store['about'],
                              address: store['address'],
                            ),
                            Container(
                              padding: const EdgeInsets.symmetric(
                                  horizontal: 15, vertical: 5),
                              child: Column(
                                crossAxisAlignment: CrossAxisAlignment.start,
                                children: [
                                  Row(
                                    mainAxisAlignment: MainAxisAlignment.center,
                                    children: [
                                      const Text(
                                        'Operation Times',
                                        style: TextStyle(
                                          fontSize: 22,
                                          fontWeight: FontWeight.bold,
                                        ),
                                      ),
                                      const Spacer(),
                                      TextButton.icon(
                                        icon: const Icon(
                                          Icons.edit_rounded,
                                          size: 18,
                                        ),
                                        label: const Text('Edit',
                                            style: TextStyle(
                                                fontSize: 16,
                                                fontWeight: FontWeight.bold)),
                                        onPressed: () {
                                          Navigator.push(
                                            context,
                                            MaterialPageRoute(
                                              builder: (context) =>
                                                  const OperationTime(),
                                            ),
                                          ).then((_) => {setState(() {})});
                                        },
                                      )
                                    ],
                                  ),
                                  MediaQuery.removePadding(
                                    context: context,
                                    removeTop: true,
                                    child: ListView.builder(
                                      physics:
                                          const NeverScrollableScrollPhysics(),
                                      shrinkWrap: true,
                                      itemCount: snapshot.data[1].docs.length,
                                      itemBuilder:
                                          (BuildContext context, int index) {
                                        return OpeningHours(
                                          status: Colors.green,
                                          day: snapshot.data[1].docs[index]
                                              ['dayID'],
                                          hour: snapshot.data[1].docs[index]
                                                  ['openHours'] +
                                              ' - ' +
                                              snapshot.data[1].docs[index]
                                                  ['closeHours'],
                                        );
                                      },
                                    ),
                                  ),
                                ],
                              ),
                            ),
                          ],
                        );
                      } catch (e) {
                        return Container(
                          padding: const EdgeInsets.symmetric(horizontal: 15),
                          height: MediaQuery.of(context).size.height * 0.50,
                          child: Column(
                            mainAxisAlignment: MainAxisAlignment.end,
                            children: [
                              Image.asset('assets/images/store.png', scale: 5),
                              const SizedBox(height: 10),
                              const Text(
                                'Anda belum mempunyai toko',
                                style: headingTitle,
                              ),
                              const SizedBox(height: 15),
                              SizedBox(
                                width: double.infinity,
                                height: 45,
                                child: ElevatedButton(
                                  child: const Text(
                                    'BUKA TOKO SEKARANG',
                                    style: TextStyle(
                                        fontSize: 18,
                                        fontWeight: FontWeight.bold,
                                        color: Colors.white),
                                  ),
                                  onPressed: () {
                                    Navigator.push(
                                      context,
                                      MaterialPageRoute(
                                        builder: (context) => const StoreForm(),
                                      ),
                                    ).then((_) => setState(() {}));
                                  },
                                  style: ButtonStyle(
                                    backgroundColor:
                                        MaterialStateProperty.all<Color>(
                                            primaryColor),
                                  ),
                                ),
                              )
                            ],
                          ),
                        );
                      }
                    }
                  }
                  return Container(
                    alignment: Alignment.bottomCenter,
                    height: MediaQuery.of(context).size.height * 0.50,
                    child: const CircularProgressIndicator(),
                  );
                },
                future: Future.wait([getStore(), getListOperationTime()]),
              ),
            ),
    );
  }
}
