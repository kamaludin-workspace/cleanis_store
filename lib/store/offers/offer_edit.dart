import 'package:cleanis_store/common_widget/title_form.dart';
import 'package:cleanis_store/config/colors.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:intl/intl.dart';

class OfferEdit extends StatefulWidget {
  final String docID;
  const OfferEdit({Key key, this.docID}) : super(key: key);

  @override
  _OfferEditState createState() => _OfferEditState();
}

class _OfferEditState extends State<OfferEdit> {
  final uid = FirebaseAuth.instance.currentUser.uid;
  final db = FirebaseFirestore.instance;

  DateTime _date = DateTime.now();
  final _formKey = GlobalKey<FormState>();

  Future updateOffer({
    String limitDate,
    String subtitle,
    String theme,
    String title,
  }) {
    try {
      return db
          .collection('stores')
          .doc(uid)
          .collection('offers')
          .doc(widget.docID)
          .update({
        'limitDate': limitDate,
        'subtitle': subtitle,
        'theme': theme,
        'title': title,
      });
    } catch (e) {
      return e.code;
    }
  }

  Future loadOffer() async {
    final service = await db
        .collection('stores')
        .doc(uid)
        .collection('offers')
        .doc(widget.docID)
        .get();
    return service;
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: primaryColor,
        title: const Text(
          'Service Form',
        ),
      ),
      body: SingleChildScrollView(
        child: FutureBuilder(
          builder: (context, snapshot) {
            if (snapshot.connectionState == ConnectionState.done) {
              final offer = snapshot.data;
              final _pickedDateController = TextEditingController();
              Future<void> _selectPickedDate(BuildContext context) async {
                bool _limitDay(DateTime day) {
                  if ((day.isAfter(
                          DateTime.now().subtract(const Duration(days: 1))) &&
                      day.isBefore(
                          DateTime.now().add(const Duration(days: 10))))) {
                    return true;
                  }
                  return false;
                }

                DateTime _datePicked = await showDatePicker(
                  context: context,
                  initialDate: _date,
                  firstDate: DateTime(DateTime.now().year),
                  lastDate: DateTime(DateTime.now().year + 1),
                  selectableDayPredicate: _limitDay,
                );

                if (_datePicked != null) {
                  _date = _datePicked;
                  _pickedDateController.text =
                      DateFormat("EEEE, d MMMM yyyy", "id_ID").format(_date);
                }
              }

              String _value = offer['theme'];
              final _nameController = TextEditingController()
                ..text = offer['title'];
              final _descriptionController = TextEditingController()
                ..text = offer['subtitle'];

              return Container(
                padding: const EdgeInsets.fromLTRB(15, 0, 15, 15),
                child: Form(
                  key: _formKey,
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      const TitleForm(title: "Thumbnail"),
                      SingleChildScrollView(
                        scrollDirection: Axis.horizontal,
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: <Widget>[
                            GestureDetector(
                              onTap: () =>
                                  setState(() => _value = "blue_1.jpg"),
                              child: ThumbnailInput(
                                imageValue: _value,
                                image: "blue_1.jpg",
                              ),
                            ),
                            const SizedBox(width: 5),
                            GestureDetector(
                              onTap: () =>
                                  setState(() => _value = "yellow_2.jpg"),
                              child: ThumbnailInput(
                                imageValue: _value,
                                image: "yellow_2.jpg",
                              ),
                            ),
                          ],
                        ),
                      ),
                      const TitleForm(title: "Judul"),
                      Container(
                        color: Colors.white,
                        child: TextFormField(
                          controller: _nameController,
                          validator: (val) =>
                              val.isEmpty ? 'Nama is required' : null,
                          keyboardType: TextInputType.text,
                          decoration: const InputDecoration(
                            border: OutlineInputBorder(),
                            isDense: true,
                            hintText: 'Judul penawaran',
                          ),
                        ),
                      ),
                      const TitleForm(title: "Deskripsi"),
                      Container(
                        color: Colors.white,
                        child: TextFormField(
                          controller: _descriptionController,
                          validator: (val) =>
                              val.isEmpty ? 'Field is required' : null,
                          keyboardType: TextInputType.multiline,
                          minLines: 2,
                          maxLines: 5,
                          decoration: const InputDecoration(
                            border: OutlineInputBorder(),
                            isDense: true,
                            hintText: 'Deskripsi tentang layanan',
                            alignLabelWithHint: true,
                          ),
                        ),
                      ),
                      TitleForm(
                          title: "Tanggal berlaku - ${offer['limitDate']}"),
                      TextFormField(
                        controller: _pickedDateController,
                        readOnly: true,
                        onTap: () async {
                          _selectPickedDate(context);
                        },
                        validator: (val) =>
                            val.isEmpty ? 'Tidak boleh kosong' : null,
                        decoration: const InputDecoration(
                          border: OutlineInputBorder(),
                          isDense: true,
                          contentPadding: EdgeInsets.only(left: 10),
                          suffixIcon: Icon(
                            Icons.calendar_today,
                            size: 18,
                          ),
                          hintText: 'Berlaku hingga',
                        ),
                      ),
                      const SizedBox(height: 10),
                      SizedBox(
                        width: double.infinity,
                        height: 40,
                        child: ElevatedButton(
                          child: const Text(
                            'SIMPAN',
                            style: TextStyle(
                                fontSize: 18,
                                fontWeight: FontWeight.bold,
                                color: Colors.white),
                          ),
                          onPressed: () {
                            if (_formKey.currentState.validate()) {
                              updateOffer(
                                limitDate: _pickedDateController.text,
                                subtitle: _descriptionController.text,
                                theme: _value,
                                title: _nameController.text,
                              );
                              Navigator.of(context).pop();
                            }
                          },
                          style: ButtonStyle(
                            backgroundColor:
                                MaterialStateProperty.all<Color>(primaryColor),
                          ),
                        ),
                      )
                    ],
                  ),
                ),
              );
            }
            return const Center(
              child: CircularProgressIndicator(),
            );
          },
          future: loadOffer(),
        ),
      ),
    );
  }
}

class ThumbnailInput extends StatelessWidget {
  final String image, imageValue;
  const ThumbnailInput({
    Key key,
    this.image,
    this.imageValue,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      decoration: BoxDecoration(
        color: imageValue == image ? Colors.lightBlue[100] : Colors.white,
        boxShadow: [
          BoxShadow(
            blurRadius: 10,
            color: Colors.grey[300],
            spreadRadius: 1,
            offset: const Offset(0, 3),
          )
        ],
        borderRadius: const BorderRadius.all(
          Radius.circular(6),
        ),
        border: imageValue == image
            ? Border.all(
                color: primaryColor,
                width: 2,
              )
            : const Border(),
      ),
      padding: const EdgeInsets.all(4),
      child: Image.asset("assets/images/$image", scale: 8),
    );
  }
}
