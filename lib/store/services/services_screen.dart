import 'package:cleanis_store/config/colors.dart';
import 'package:cleanis_store/store/services/service_card_widget.dart';
import 'package:cleanis_store/store/services/service_detail.dart';
import 'package:cleanis_store/store/services/service_form.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:intl/intl.dart';

class ServicesScreen extends StatefulWidget {
  const ServicesScreen({Key key}) : super(key: key);

  @override
  _ServicesScreenState createState() => _ServicesScreenState();
}

class _ServicesScreenState extends State<ServicesScreen> {
  final nominal = NumberFormat("#,##0", "en_US");
  final uid = FirebaseAuth.instance.currentUser.uid;

  Future<QuerySnapshot> loadServiceofStore() {
    return FirebaseFirestore.instance
        .collection('stores')
        .doc(uid)
        .collection('services')
        .get();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: primaryColor,
        title: const Text('Services Stores'),
        actions: [
          IconButton(
            icon: const Icon(
              Icons.add_rounded,
              size: 28,
              color: Colors.white,
            ),
            onPressed: () {
              Navigator.push(
                context,
                MaterialPageRoute(
                  builder: (context) => const ServiceForm(),
                ),
              ).then((_) => setState(() {}));
            },
          )
        ],
      ),
      body: FutureBuilder(
        builder: (context, snapshot) {
          if (snapshot.connectionState == ConnectionState.done) {
            if (snapshot.hasData) {
              if (snapshot.data.docs.isEmpty) {
                return Center(
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      Image.asset(
                        'assets/images/empty.png',
                        scale: 5,
                      ),
                      const SizedBox(height: 15),
                      const Text(
                        'Service tidak ditemukan',
                        style: TextStyle(
                          fontSize: 20,
                          fontWeight: FontWeight.bold,
                        ),
                      ),
                      const SizedBox(height: 20),
                      SizedBox(
                        width: MediaQuery.of(context).size.width * 0.60,
                        height: 40,
                        child: ElevatedButton(
                          child: const Text(
                            'Tambah Service Baru',
                            style: TextStyle(
                                fontSize: 18,
                                fontWeight: FontWeight.bold,
                                color: Colors.white),
                          ),
                          onPressed: () {
                            Navigator.push(
                              context,
                              MaterialPageRoute(
                                builder: (context) => const ServiceForm(),
                              ),
                            ).then((_) => setState(() {}));
                          },
                          style: ButtonStyle(
                            backgroundColor:
                                MaterialStateProperty.all<Color>(primaryColor),
                          ),
                        ),
                      )
                    ],
                  ),
                );
              } else {
                return SingleChildScrollView(
                  child: Column(
                    children: [
                      MediaQuery.removePadding(
                        context: context,
                        removeTop: true,
                        child: ListView.builder(
                          physics: const NeverScrollableScrollPhysics(),
                          shrinkWrap: true,
                          itemCount: snapshot.data.docs.length,
                          itemBuilder: (BuildContext context, int index) {
                            return ServiceCard(
                              onPressed: () {
                                Navigator.push(
                                  context,
                                  MaterialPageRoute(
                                    builder: (context) => ServiceDetail(
                                      docID: snapshot.data.docs[index].id,
                                      title: snapshot.data.docs[index]['name'],
                                      description: snapshot.data.docs[index]
                                          ['description'],
                                      price: nominal.format(int.parse(
                                          snapshot.data.docs[index]['prices'])),
                                      unit: snapshot.data.docs[index]['unit'],
                                      processTime: snapshot.data.docs[index]
                                          ['processTime'],
                                      deliveryCost: snapshot.data.docs[index]
                                          ['costDelivery'],
                                      thumbnail: snapshot.data.docs[index]
                                          ['thumbnail'],
                                    ),
                                  ),
                                ).then((_) => setState(() {}));
                              },
                              title: snapshot.data.docs[index]['name'],
                              processTime: snapshot.data.docs[index]
                                  ['processTime'],
                              deliveryCost: snapshot.data.docs[index]
                                  ['costDelivery'],
                              description: snapshot.data.docs[index]
                                  ['description'],
                              thumbnail: snapshot.data.docs[index]['thumbnail'],
                              price: nominal.format(int.parse(
                                  snapshot.data.docs[index]['prices'])),
                              unit: snapshot.data.docs[index]['unit'],
                            );
                          },
                        ),
                      ),
                      Container(
                        margin: const EdgeInsets.symmetric(vertical: 10),
                        padding: const EdgeInsets.symmetric(horizontal: 15),
                        width: double.infinity,
                        height: 40,
                        child: ElevatedButton(
                          child: const Text(
                            'Tambah Service Baru',
                            style: TextStyle(
                                fontSize: 18,
                                fontWeight: FontWeight.bold,
                                color: Colors.white),
                          ),
                          onPressed: () {
                            Navigator.push(
                              context,
                              MaterialPageRoute(
                                builder: (context) => const ServiceForm(),
                              ),
                            ).then((_) => setState(() {}));
                          },
                          style: ButtonStyle(
                            backgroundColor:
                                MaterialStateProperty.all<Color>(primaryColor),
                          ),
                        ),
                      ),
                    ],
                  ),
                );
              }
            }
          }
          return const Center(child: CircularProgressIndicator());
        },
        future: loadServiceofStore(),
      ),
    );
  }
}
