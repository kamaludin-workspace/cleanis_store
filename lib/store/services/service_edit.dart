import 'package:cleanis_store/common_widget/title_form.dart';
import 'package:cleanis_store/config/colors.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';

String name,
    thumbnail,
    costDelivery,
    description,
    prices,
    processTime,
    unit,
    _value;

class ServiceEdit extends StatefulWidget {
  final String docID;
  const ServiceEdit({Key key, this.docID}) : super(key: key);

  @override
  _ServiceEditState createState() => _ServiceEditState();
}

class _ServiceEditState extends State<ServiceEdit> {
  final uid = FirebaseAuth.instance.currentUser.uid;
  final db = FirebaseFirestore.instance;

  final _formKey = GlobalKey<FormState>();

  CollectionReference collectionReference = FirebaseFirestore.instance
      .collection('stores')
      .doc(FirebaseAuth.instance.currentUser.uid)
      .collection('services');

  void getServiceDetail() {
    collectionReference.doc(widget.docID).get().then((value) {
      var fields = value;
      setState(() {
        name = fields.get('name');
        costDelivery = fields.get('costDelivery');
        description = fields.get('description');
        prices = fields.get('prices');
        processTime = fields.get('processTime');
        thumbnail = fields.get('thumbnail');
        unit = fields.get('unit');
        _value = fields.get('thumbnail');
      });
    });
  }

  Future updateService({
    String costDelivery,
    String description,
    String name,
    String prices,
    String processTime,
    String thumbnail,
    String unit,
  }) {
    try {
      return db
          .collection('stores')
          .doc(uid)
          .collection('services')
          .doc(widget.docID)
          .update({
        'costDelivery': costDelivery,
        'description': description,
        'name': name,
        'prices': prices,
        'processTime': processTime,
        'thumbnail': thumbnail,
        'unit': unit,
      });
    } catch (e) {
      return e.code;
    }
  }

  @override
  void initState() {
    getServiceDetail();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    final _nameController = TextEditingController()..text = name;
    final _priceController = TextEditingController()..text = prices;
    final _unitController = TextEditingController()..text = unit;
    final _deliveryCostController = TextEditingController()
      ..text = costDelivery;
    final _processTimeController = TextEditingController()..text = processTime;
    final _descriptionController = TextEditingController()..text = description;

    return Scaffold(
      appBar: AppBar(
        backgroundColor: primaryColor,
        title: const Text(
          'Service Form',
        ),
      ),
      body: (name != '')
          ? SingleChildScrollView(
              child: Container(
                padding: const EdgeInsets.fromLTRB(15, 0, 15, 15),
                child: Form(
                  key: _formKey,
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      const TitleForm(title: 'Thumbnail'),
                      SingleChildScrollView(
                        scrollDirection: Axis.horizontal,
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: <Widget>[
                            GestureDetector(
                              onTap: () {
                                setState(() {
                                  _value = "drying.png";
                                });
                              },
                              child: ThumbnailInput(
                                imageValue: _value,
                                image: "drying.png",
                              ),
                            ),
                            GestureDetector(
                              onTap: () {
                                setState(() {
                                  _value = "backpack.png";
                                });
                              },
                              child: ThumbnailInput(
                                imageValue: _value,
                                image: "backpack.png",
                              ),
                            ),
                            GestureDetector(
                              onTap: () {
                                setState(() {
                                  _value = "carpet.png";
                                });
                              },
                              child: ThumbnailInput(
                                imageValue: _value,
                                image: "carpet.png",
                              ),
                            ),
                            GestureDetector(
                              onTap: () {
                                setState(() {
                                  _value = "shoes.png";
                                });
                              },
                              child: ThumbnailInput(
                                imageValue: _value,
                                image: "shoes.png",
                              ),
                            ),
                            GestureDetector(
                              onTap: () {
                                setState(() {
                                  _value = "doll.png";
                                });
                              },
                              child: ThumbnailInput(
                                imageValue: _value,
                                image: "doll.png",
                              ),
                            ),
                          ],
                        ),
                      ),
                      const TitleForm(title: "Nama"),
                      Container(
                        color: Colors.white,
                        child: TextFormField(
                          controller: _nameController,
                          validator: (val) =>
                              val.isEmpty ? 'Nama is required' : null,
                          keyboardType: TextInputType.text,
                          decoration: const InputDecoration(
                            border: OutlineInputBorder(),
                            isDense: true,
                            hintText: 'Nama layanan',
                          ),
                        ),
                      ),
                      const TitleForm(title: "Harga / unit"),
                      Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: [
                          Container(
                            width: MediaQuery.of(context).size.width * 0.60,
                            color: Colors.white,
                            child: TextFormField(
                              controller: _priceController,
                              validator: (val) =>
                                  val.isEmpty ? 'Field is required' : null,
                              keyboardType: TextInputType.text,
                              decoration: const InputDecoration(
                                border: OutlineInputBorder(),
                                isDense: true,
                                hintText: 'Harga layanan',
                              ),
                            ),
                          ),
                          Container(
                            width: MediaQuery.of(context).size.width * 0.30,
                            color: Colors.white,
                            child: TextFormField(
                              controller: _unitController,
                              validator: (val) =>
                                  val.isEmpty ? 'Field is required' : null,
                              keyboardType: TextInputType.text,
                              decoration: const InputDecoration(
                                border: OutlineInputBorder(),
                                isDense: true,
                                labelText: 'Unit',
                              ),
                            ),
                          ),
                        ],
                      ),
                      const TitleForm(title: "Ongkos Kirim /KM"),
                      Container(
                        color: Colors.white,
                        child: TextFormField(
                          controller: _deliveryCostController,
                          validator: (val) =>
                              val.isEmpty ? 'Field is required' : null,
                          keyboardType: TextInputType.text,
                          decoration: const InputDecoration(
                            border: OutlineInputBorder(),
                            isDense: true,
                            hintText: 'Ongkos Kirim',
                          ),
                        ),
                      ),
                      const TitleForm(title: "Waktu Prosess"),
                      Container(
                        color: Colors.white,
                        child: TextFormField(
                          controller: _processTimeController,
                          validator: (val) =>
                              val.isEmpty ? 'Field is required' : null,
                          keyboardType: TextInputType.text,
                          decoration: const InputDecoration(
                            border: OutlineInputBorder(),
                            isDense: true,
                            hintText: 'Waktu pengerjaan',
                          ),
                        ),
                      ),
                      const TitleForm(title: "Deskripsi"),
                      Container(
                        color: Colors.white,
                        child: TextFormField(
                          controller: _descriptionController,
                          validator: (val) =>
                              val.isEmpty ? 'Field is required' : null,
                          keyboardType: TextInputType.multiline,
                          minLines: 2,
                          maxLines: 5,
                          decoration: const InputDecoration(
                            border: OutlineInputBorder(),
                            isDense: true,
                            hintText: 'Deskripsi tentang layanan',
                            alignLabelWithHint: true,
                          ),
                        ),
                      ),
                      const SizedBox(height: 10),
                      SizedBox(
                        width: double.infinity,
                        height: 40,
                        child: ElevatedButton(
                          child: const Text(
                            'SIMPAN',
                            style: TextStyle(
                                fontSize: 18,
                                fontWeight: FontWeight.bold,
                                color: Colors.white),
                          ),
                          onPressed: () {
                            if (_formKey.currentState.validate()) {
                              updateService(
                                costDelivery: _deliveryCostController.text,
                                description: _descriptionController.text,
                                name: _nameController.text,
                                prices: _priceController.text,
                                processTime: _processTimeController.text,
                                thumbnail: _value,
                                unit: _unitController.text,
                              );
                              int count = 0;
                              Navigator.popUntil(context, (route) {
                                return count++ == 2;
                              });
                            }
                          },
                          style: ButtonStyle(
                            backgroundColor:
                                MaterialStateProperty.all<Color>(primaryColor),
                          ),
                        ),
                      )
                    ],
                  ),
                ),
              ),
            )
          : const CircularProgressIndicator(),
    );
  }
}

class ThumbnailInput extends StatelessWidget {
  final String image, imageValue;
  const ThumbnailInput({
    Key key,
    this.image,
    this.imageValue,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      decoration: BoxDecoration(
        color: imageValue == image ? Colors.lightBlue[100] : Colors.white,
        boxShadow: [
          BoxShadow(
            blurRadius: 10,
            color: Colors.grey[300],
            spreadRadius: 1,
            offset: const Offset(0, 3),
          )
        ],
        borderRadius: const BorderRadius.all(
          Radius.circular(6),
        ),
        border: imageValue == image
            ? Border.all(
                color: primaryColor,
                width: 2,
              )
            : const Border(),
      ),
      margin: const EdgeInsets.only(right: 10),
      padding: const EdgeInsets.all(5),
      child: Image.asset("assets/images/services/$image", scale: 8),
    );
  }
}
