import 'package:cleanis_store/config/colors.dart';
import 'package:cleanis_store/config/styles.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:intl/intl.dart';

import 'service_edit.dart';

class ServiceDetail extends StatefulWidget {
  final String docID,
      title,
      description,
      price,
      unit,
      thumbnail,
      processTime,
      deliveryCost;

  const ServiceDetail({
    Key key,
    this.title,
    this.description,
    this.price,
    this.unit,
    this.thumbnail,
    this.processTime,
    this.deliveryCost,
    this.docID,
  }) : super(key: key);

  @override
  _ServiceDetailState createState() => _ServiceDetailState();
}

class _ServiceDetailState extends State<ServiceDetail> {
  final uid = FirebaseAuth.instance.currentUser.uid;
  final db = FirebaseFirestore.instance;
  final nominal = NumberFormat("#,##0", "en_US");
  formatOngkir(price) {
    String text;
    if (price == '0') {
      return text = ' Gratis';
    } else {
      var proc = nominal.format(int.parse(price));
      text = ' Rp. ' + proc + '/Km';
      return text;
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: primaryColor,
        title: Text(widget.title),
      ),
      body: Container(
        width: double.infinity,
        padding: const EdgeInsets.all(15),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Row(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Expanded(
                  flex: 3,
                  child: Container(
                    width: double.infinity,
                    height: 90,
                    decoration: BoxDecoration(
                      image: DecorationImage(
                        image: AssetImage(
                            "assets/images/services/${widget.thumbnail}"),
                        scale: 8,
                      ),
                      border: Border.all(
                        color: Colors.grey,
                      ),
                      borderRadius: const BorderRadius.all(
                        Radius.circular(5),
                      ),
                    ),
                  ),
                ),
                Expanded(
                  flex: 7,
                  child: Container(
                    padding: const EdgeInsets.only(left: 10),
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Text(
                          widget.title,
                          style: const TextStyle(
                            fontSize: 24,
                            color: Colors.black87,
                            fontWeight: FontWeight.bold,
                          ),
                        ),
                        Row(
                          children: [
                            const Icon(
                              Icons.schedule,
                              size: 20,
                              color: primaryColor,
                            ),
                            Text(
                              widget.processTime,
                              style: const TextStyle(
                                color: primaryDarkenColor,
                                fontSize: 16,
                                fontWeight: FontWeight.w100,
                              ),
                            ),
                            const SizedBox(width: 5),
                            const Icon(
                              Icons.delivery_dining,
                              size: 24,
                              color: primaryColor,
                            ),
                            Text(
                              formatOngkir(widget.deliveryCost),
                              style: const TextStyle(
                                color: primaryDarkenColor,
                                fontSize: 16,
                                fontWeight: FontWeight.w100,
                              ),
                            ),
                          ],
                        ),
                        const SizedBox(height: 15),
                        Row(
                          mainAxisAlignment: MainAxisAlignment.start,
                          children: <Widget>[
                            Row(
                              mainAxisSize: MainAxisSize.min,
                              crossAxisAlignment: CrossAxisAlignment.baseline,
                              textBaseline: TextBaseline.alphabetic,
                              children: [
                                const Text('Rp. ',
                                    style: TextStyle(fontSize: 16)),
                                Text(widget.price,
                                    style: const TextStyle(fontSize: 28)),
                                Text('/${widget.unit}',
                                    style: const TextStyle(fontSize: 16)),
                              ],
                            ),
                          ],
                        ),
                      ],
                    ),
                  ),
                ),
              ],
            ),
            const SizedBox(height: 15),
            const Text('Deskripsi', style: titleStyle),
            const SizedBox(height: 10),
            Text(
              widget.description,
              style: const TextStyle(
                height: 1.2,
                fontSize: 15,
                letterSpacing: 0.4,
                color: Colors.black54,
                fontWeight: FontWeight.bold,
              ),
            ),
            const SizedBox(height: 20),
            Row(
              children: [
                Expanded(
                  child: SizedBox(
                    width: double.infinity,
                    height: 40,
                    child: ElevatedButton(
                      child: const Text(
                        'HAPUS',
                        style: TextStyle(
                            fontSize: 18,
                            fontWeight: FontWeight.bold,
                            color: Colors.white),
                      ),
                      onPressed: () {
                        db
                            .collection('stores')
                            .doc(uid)
                            .collection('services')
                            .doc(widget.docID)
                            .delete();
                        Navigator.pop(context);
                      },
                      style: ButtonStyle(
                        backgroundColor:
                            MaterialStateProperty.all<Color>(Colors.red),
                      ),
                    ),
                  ),
                ),
                const SizedBox(width: 5),
                Expanded(
                  child: SizedBox(
                    width: double.infinity,
                    height: 40,
                    child: ElevatedButton(
                      child: const Text(
                        'EDIT',
                        style: TextStyle(
                            fontSize: 18,
                            fontWeight: FontWeight.bold,
                            color: Colors.white),
                      ),
                      onPressed: () {
                        Navigator.push(
                          context,
                          MaterialPageRoute(
                            builder: (context) =>
                                ServiceEdit(docID: widget.docID),
                          ),
                        ).then((_) => setState(() {}));
                      },
                      style: ButtonStyle(
                        backgroundColor:
                            MaterialStateProperty.all<Color>(Colors.green),
                      ),
                    ),
                  ),
                )
              ],
            )
          ],
        ),
      ),
    );
  }
}
