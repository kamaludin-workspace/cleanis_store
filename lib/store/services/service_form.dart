import 'package:cleanis_store/common_widget/title_form.dart';
import 'package:cleanis_store/config/colors.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';

class ServiceForm extends StatefulWidget {
  const ServiceForm({Key key}) : super(key: key);

  @override
  _ServiceFormState createState() => _ServiceFormState();
}

class _ServiceFormState extends State<ServiceForm> {
  final uid = FirebaseAuth.instance.currentUser.uid;
  final db = FirebaseFirestore.instance;
  String thumbnailValue = "drying.png";
  final _formKey = GlobalKey<FormState>();

  final _nameController = TextEditingController();
  final _priceController = TextEditingController();
  final _deliveryCostController = TextEditingController();
  final _processTimeController = TextEditingController();
  final _descriptionController = TextEditingController();

  Future createService({
    String costDelivery,
    String description,
    String name,
    String prices,
    String processTime,
    String thumbnail,
    String unit,
  }) {
    try {
      return db.collection('stores').doc(uid).collection('services').doc().set({
        'costDelivery': costDelivery,
        'description': description,
        'name': name,
        'prices': prices,
        'processTime': processTime,
        'thumbnail': thumbnail,
        'unit': unit,
      });
    } catch (e) {
      return e.code;
    }
  }

  List<DropdownMenuItem<String>> get dropdownItems {
    List<DropdownMenuItem<String>> menuItems = [
      const DropdownMenuItem(child: Text("Pcs"), value: "Pcs"),
      const DropdownMenuItem(child: Text("Kg"), value: "Kg"),
    ];
    return menuItems;
  }

  String unitValue = "Pcs";

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: primaryColor,
        title: const Text(
          'Service Form',
        ),
      ),
      body: SingleChildScrollView(
        child: Container(
          padding: const EdgeInsets.fromLTRB(15, 0, 15, 15),
          child: Form(
            key: _formKey,
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                const TitleForm(title: "Thumbnail"),
                SingleChildScrollView(
                  scrollDirection: Axis.horizontal,
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: <Widget>[
                      GestureDetector(
                        onTap: () =>
                            setState(() => thumbnailValue = "drying.png"),
                        child: ThumbnailInput(
                          imageValue: thumbnailValue,
                          image: "drying.png",
                        ),
                      ),
                      GestureDetector(
                        onTap: () =>
                            setState(() => thumbnailValue = "backpack.png"),
                        child: ThumbnailInput(
                          imageValue: thumbnailValue,
                          image: "backpack.png",
                        ),
                      ),
                      GestureDetector(
                        onTap: () =>
                            setState(() => thumbnailValue = "carpet.png"),
                        child: ThumbnailInput(
                          imageValue: thumbnailValue,
                          image: "carpet.png",
                        ),
                      ),
                      GestureDetector(
                        onTap: () =>
                            setState(() => thumbnailValue = "shoes.png"),
                        child: ThumbnailInput(
                          imageValue: thumbnailValue,
                          image: "shoes.png",
                        ),
                      ),
                      GestureDetector(
                        onTap: () =>
                            setState(() => thumbnailValue = "doll.png"),
                        child: ThumbnailInput(
                          imageValue: thumbnailValue,
                          image: "doll.png",
                        ),
                      ),
                    ],
                  ),
                ),
                const TitleForm(title: "Nama"),
                Container(
                  color: Colors.white,
                  child: TextFormField(
                    controller: _nameController,
                    validator: (val) => val.isEmpty ? 'Nama is required' : null,
                    keyboardType: TextInputType.text,
                    decoration: const InputDecoration(
                      border: OutlineInputBorder(),
                      isDense: true,
                      hintText: 'Nama layanan',
                    ),
                  ),
                ),
                const TitleForm(title: "Harga / unit"),
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Container(
                      width: MediaQuery.of(context).size.width * 0.60,
                      color: Colors.white,
                      child: TextFormField(
                        controller: _priceController,
                        validator: (val) =>
                            val.isEmpty ? 'Field is required' : null,
                        keyboardType: TextInputType.text,
                        decoration: const InputDecoration(
                          border: OutlineInputBorder(),
                          isDense: true,
                          hintText: 'Harga layanan',
                        ),
                      ),
                    ),
                    Container(
                      width: MediaQuery.of(context).size.width * 0.30,
                      color: Colors.white,
                      child: DropdownButtonFormField(
                        decoration: InputDecoration(
                          labelText: 'Unit',
                          contentPadding: const EdgeInsets.symmetric(
                              vertical: 13, horizontal: 5),
                          enabledBorder: OutlineInputBorder(
                            borderSide: const BorderSide(
                                color: Colors.black54, width: 1),
                            borderRadius: BorderRadius.circular(4),
                          ),
                          filled: true,
                          fillColor: Colors.white,
                        ),
                        dropdownColor: Colors.white,
                        value: unitValue,
                        onChanged: (String newValue) {
                          setState(() {
                            unitValue = newValue;
                          });
                        },
                        items: dropdownItems,
                      ),
                    ),
                  ],
                ),
                const TitleForm(title: "Ongkos Kirim /KM"),
                Container(
                  color: Colors.white,
                  child: TextFormField(
                    controller: _deliveryCostController,
                    validator: (val) =>
                        val.isEmpty ? 'Field is required' : null,
                    keyboardType: TextInputType.text,
                    decoration: const InputDecoration(
                      border: OutlineInputBorder(),
                      isDense: true,
                      hintText: 'Ongkos Kirim',
                    ),
                  ),
                ),
                const TitleForm(title: "Waktu Prosess"),
                Container(
                  color: Colors.white,
                  child: TextFormField(
                    controller: _processTimeController,
                    validator: (val) =>
                        val.isEmpty ? 'Field is required' : null,
                    keyboardType: TextInputType.text,
                    decoration: const InputDecoration(
                      border: OutlineInputBorder(),
                      isDense: true,
                      hintText: 'Waktu pengerjaan',
                    ),
                  ),
                ),
                const TitleForm(title: "Deskripsi"),
                Container(
                  color: Colors.white,
                  child: TextFormField(
                    controller: _descriptionController,
                    validator: (val) =>
                        val.isEmpty ? 'Field is required' : null,
                    keyboardType: TextInputType.multiline,
                    minLines: 2,
                    maxLines: 5,
                    decoration: const InputDecoration(
                      border: OutlineInputBorder(),
                      isDense: true,
                      hintText: 'Deskripsi tentang layanan',
                      alignLabelWithHint: true,
                    ),
                  ),
                ),
                const SizedBox(height: 10),
                SizedBox(
                  width: double.infinity,
                  height: 40,
                  child: ElevatedButton(
                    child: const Text(
                      'SIMPAN',
                      style: TextStyle(
                          fontSize: 18,
                          fontWeight: FontWeight.bold,
                          color: Colors.white),
                    ),
                    onPressed: () {
                      if (_formKey.currentState.validate()) {
                        createService(
                          costDelivery: _deliveryCostController.text,
                          description: _descriptionController.text,
                          name: _nameController.text,
                          prices: _priceController.text,
                          processTime: _processTimeController.text,
                          thumbnail: thumbnailValue,
                          unit: unitValue,
                        );
                        Navigator.of(context).pop();
                      }
                    },
                    style: ButtonStyle(
                      backgroundColor:
                          MaterialStateProperty.all<Color>(primaryColor),
                    ),
                  ),
                )
              ],
            ),
          ),
        ),
      ),
    );
  }
}

class ThumbnailInput extends StatelessWidget {
  final String image, imageValue;
  const ThumbnailInput({
    Key key,
    this.image,
    this.imageValue,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      decoration: BoxDecoration(
        color: imageValue == image ? Colors.lightBlue[100] : Colors.white,
        boxShadow: [
          BoxShadow(
            blurRadius: 10,
            color: Colors.grey[300],
            spreadRadius: 1,
            offset: const Offset(0, 3),
          )
        ],
        borderRadius: const BorderRadius.all(
          Radius.circular(6),
        ),
        border: imageValue == image
            ? Border.all(
                color: primaryColor,
                width: 2,
              )
            : const Border(),
      ),
      margin: const EdgeInsets.only(right: 10),
      padding: const EdgeInsets.all(4),
      child: Image.asset("assets/images/services/$image", scale: 8),
    );
  }
}
