import 'dart:ui';

import 'package:cleanis_store/config/colors.dart';
import 'package:flutter/material.dart';
import 'package:intl/intl.dart';

class ServiceCard extends StatelessWidget {
  final nominal = NumberFormat("#,##0", "en_US");
  final String title,
      description,
      price,
      unit,
      thumbnail,
      processTime,
      deliveryCost,
      isStoreOpen;
  final VoidCallback onPressed;

  ServiceCard({
    Key key,
    this.title,
    this.description,
    this.price,
    this.unit,
    this.onPressed,
    this.thumbnail,
    this.processTime,
    this.isStoreOpen,
    this.deliveryCost,
  }) : super(key: key);

  formatOngkir(price) {
    String text;
    if (price == '0') {
      return text = ' Gratis';
    } else {
      var proc = nominal.format(int.parse(price));
      text = ' Rp. ' + proc + '/Km';
      return text;
    }
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      margin: const EdgeInsets.fromLTRB(15, 10, 15, 0),
      padding: const EdgeInsets.all(15),
      decoration: BoxDecoration(
        color: Colors.white,
        borderRadius: const BorderRadius.all(
          Radius.circular(8.0),
        ),
        border: Border.all(
          width: 1,
          color: Colors.grey[300],
        ),
      ),
      child: Column(
        children: [
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Expanded(
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Text(
                      title,
                      style: const TextStyle(
                        fontSize: 20,
                        color: Colors.black87,
                        fontWeight: FontWeight.bold,
                      ),
                    ),
                    const SizedBox(height: 2),
                    Row(
                      children: [
                        const Icon(
                          Icons.schedule,
                          size: 16,
                          color: primaryColor,
                        ),
                        Text(
                          ' ' + processTime,
                          style: const TextStyle(
                            color: primaryColor,
                            fontSize: 14,
                            fontWeight: FontWeight.w600,
                          ),
                        ),
                        const SizedBox(width: 5),
                        const Icon(
                          Icons.delivery_dining,
                          size: 18,
                          color: primaryColor,
                        ),
                        Text(
                          formatOngkir(deliveryCost),
                          style: const TextStyle(
                            color: primaryColor,
                            fontSize: 14,
                            fontWeight: FontWeight.w600,
                          ),
                        ),
                      ],
                    ),
                    const SizedBox(height: 8),
                    Text(
                      description,
                      maxLines: 3,
                      overflow: TextOverflow.ellipsis,
                      softWrap: false,
                      style: const TextStyle(
                        height: 1.2,
                        fontSize: 15,
                        letterSpacing: 0.2,
                        color: Colors.black54,
                        fontWeight: FontWeight.w400,
                      ),
                    ),
                  ],
                ),
              ),
              const SizedBox(width: 2),
              Container(
                height: 100,
                decoration: BoxDecoration(
                  border: Border.all(
                    color: Colors.grey[400],
                  ),
                  borderRadius: const BorderRadius.all(
                    Radius.circular(8),
                  ),
                ),
                child: Image.asset(
                  "assets/images/services/" + thumbnail,
                  scale: 8,
                ),
              ),
            ],
          ),
          const SizedBox(height: 4),
          Row(
              crossAxisAlignment: CrossAxisAlignment.center,
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Row(
                  mainAxisSize: MainAxisSize.min,
                  crossAxisAlignment: CrossAxisAlignment.baseline,
                  textBaseline: TextBaseline.alphabetic,
                  children: [
                    const Text('Rp. ', style: TextStyle(fontSize: 16)),
                    Text(price, style: const TextStyle(fontSize: 28)),
                    Text('/$unit', style: const TextStyle(fontSize: 16)),
                  ],
                ),
                ElevatedButton(
                  style: ElevatedButton.styleFrom(
                    elevation: 0,
                    primary: primaryColor,
                    shape: const StadiumBorder(),
                    tapTargetSize: MaterialTapTargetSize.shrinkWrap,
                    minimumSize: Size.zero,
                    padding: EdgeInsets.zero,
                  ),
                  onPressed: onPressed,
                  child: Container(
                    padding: const EdgeInsets.fromLTRB(15, 2, 5, 2),
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.spaceAround,
                      children: const [
                        Text(
                          'DETAIL',
                          style: TextStyle(
                            fontSize: 17,
                            fontWeight: FontWeight.bold,
                          ),
                        ),
                        Icon(
                          Icons.chevron_right_rounded,
                          size: 28,
                        ),
                      ],
                    ),
                  ),
                )
              ])
        ],
      ),
    );
  }
}
