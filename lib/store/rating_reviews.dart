import 'package:cleanis_store/config/colors.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';
import 'package:intl/intl.dart';

import 'widget/rating_widget.dart';
import 'widget/review_widget.dart';
import 'widget/subtitlestore_widget.dart';

class RatingAndReviews extends StatefulWidget {
  final String firestoreDocID;
  const RatingAndReviews({Key key, this.firestoreDocID}) : super(key: key);

  @override
  _RatingAndReviewsState createState() => _RatingAndReviewsState();
}

class _RatingAndReviewsState extends State<RatingAndReviews> {
  final db = FirebaseFirestore.instance;
  Future<QuerySnapshot> reviewersList({String id}) {
    return FirebaseFirestore.instance
        .collection('stores')
        .doc(id)
        .collection('reviewers')
        .orderBy('dateReviews', descending: true)
        .get();
  }

  Future setRatingStore({String id}) async {
    var storeRate = [];
    var rateResult = [];
    final reviews =
        await db.collection('stores').doc(id).collection('reviewers').get();

    if (reviews.docs.isNotEmpty) {
      for (var element in reviews.docs) {
        storeRate.add({"rate": element.get('rate')});
      }
      var sum = (storeRate
              .map<double>((m) => double.parse(m["rate"]))
              .reduce((a, b) => a + b)) /
          reviews.docs.length;

      final getFiveLength =
          storeRate.where((e) => double.parse(e['rate']) >= 5.0).length;
      final getFourLength = storeRate
          .where((e) =>
              double.parse(e['rate']) >= 4.0 && double.parse(e['rate']) <= 4.9)
          .length;
      final getThreeLength = storeRate
          .where((e) =>
              double.parse(e['rate']) >= 3.0 && double.parse(e['rate']) <= 3.9)
          .length;
      final getTwoLength = storeRate
          .where((e) =>
              double.parse(e['rate']) >= 2.0 && double.parse(e['rate']) <= 2.9)
          .length;
      final getOneLength = storeRate
          .where((e) =>
              double.parse(e['rate']) >= 1.0 && double.parse(e['rate']) <= 1.9)
          .length;

      var fiveStar = (((getFiveLength / reviews.docs.length) * 100) / 100);
      var fourStar = (((getFourLength / reviews.docs.length) * 100) / 100);
      var threeStar = (((getThreeLength / reviews.docs.length) * 100) / 100);
      var twoStar = (((getTwoLength / reviews.docs.length) * 100) / 100);
      var oneStar = (((getOneLength / reviews.docs.length) * 100) / 100);

      rateResult.add({
        "average": sum,
        "fiveStar": fiveStar,
        "fourStar": fourStar,
        "threeStar": threeStar,
        "twoStar": twoStar,
        "oneStar": oneStar
      });
    }
    return rateResult;
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: primaryColor,
        title: const Text("Rating and Reviews"),
        elevation: 0,
      ),
      body: FutureBuilder(
        future: Future.wait([
          reviewersList(id: widget.firestoreDocID),
          setRatingStore(id: widget.firestoreDocID),
        ]),
        builder: (context, snapshot) {
          if (snapshot.connectionState == ConnectionState.done) {
            final reviewerList = snapshot.data[0];
            final getRateResult = snapshot.data[1];
            if (reviewerList.docs.isEmpty) {
              return Center(
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    Image.asset(
                      'assets/images/empty.png',
                      scale: 5,
                    ),
                    const SizedBox(height: 16),
                    const Text(
                      'Belum ada Rating dan Komentar',
                      style: TextStyle(
                        fontSize: 20,
                        fontWeight: FontWeight.bold,
                      ),
                    )
                  ],
                ),
              );
            } else {
              return SingleChildScrollView(
                padding: const EdgeInsets.all(10),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    const SubtitleStore(title: 'Rating'),
                    Row(
                      children: [
                        Expanded(
                          flex: 2,
                          child: RatingScores(
                            starCount: (getRateResult[0]['average'])
                                .toStringAsFixed(1),
                            star: double.parse((getRateResult[0]['average'])
                                .toStringAsFixed(1)),
                            reviewCount:
                                (snapshot.data[0].docs.length).toString() +
                                    ' Reviews',
                          ),
                        ),
                        Expanded(
                          flex: 3,
                          child: PercentageScore(
                            percentFive: getRateResult[0]['fiveStar'],
                            percentFour: getRateResult[0]['fourStar'],
                            percentThree: getRateResult[0]['threeStar'],
                            percentTwo: getRateResult[0]['twoStar'],
                            percentOne: getRateResult[0]['oneStar'],
                          ),
                        )
                      ],
                    ),
                    const SizedBox(height: 10),
                    const SubtitleStore(title: 'Reviews'),
                    MediaQuery.removePadding(
                      context: context,
                      removeTop: true,
                      child: ListView.builder(
                          physics: const NeverScrollableScrollPhysics(),
                          shrinkWrap: true,
                          itemCount: snapshot.data[0].docs.length,
                          itemBuilder: (BuildContext context, int index) {
                            Timestamp t =
                                snapshot.data[0].docs[index]['dateReviews'];
                            DateTime d = t.toDate();
                            var time =
                                DateFormat("d MMMM yyyy", "id_ID").format(d);
                            return Riview(
                              name: snapshot.data[0].docs[index]['displayName'],
                              date: time.toString(),
                              image: snapshot.data[0].docs[index]['photoUrl'] ??
                                  'https://ik.imagekit.io/n0t5masg5jg/stores/photoUsers/user_Bp0tCB1Pd.png?ik-sdk-version=javascript-1.4.3&updatedAt=1642516788106',
                              rate: double.parse(
                                  snapshot.data[0].docs[index]['rate']),
                              comment: snapshot.data[0].docs[index]['comment'],
                            );
                          }),
                    ),
                  ],
                ),
              );
            }
          }
          return const Center(child: CircularProgressIndicator());
        },
      ),
    );
  }
}
