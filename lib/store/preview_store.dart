import 'package:cleanis_store/config/colors.dart';
import 'package:intl/intl.dart';
import 'package:badges/badges.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';
import 'package:flutter_rating_bar/flutter_rating_bar.dart';
import 'package:url_launcher/url_launcher.dart';
import 'widget/opening_hours_widget.dart';
import 'widget/rating_widget.dart';
import 'widget/review_widget.dart';
import 'widget/service_store_widget.dart';
import 'widget/store_button_action_widget.dart';
import 'widget/subtitlestore_widget.dart';
import 'widget/tabtitle_widget.dart';
import 'widget/voucher_widget.dart';

class PreviewStore extends StatefulWidget {
  final String firestoreDocID;
  const PreviewStore({Key key, this.firestoreDocID}) : super(key: key);

  @override
  _PreviewStoreState createState() => _PreviewStoreState();
}

class _PreviewStoreState extends State<PreviewStore> {
  final nominal = NumberFormat("#,##0", "en_US");

  String name,
      address,
      rate,
      thumbnail,
      numberPhone,
      day,
      openHours,
      lat,
      lng,
      closeHours;

  bool isVerified;
  double star;
  final db = FirebaseFirestore.instance;

  loadStore() async {
    var getRating = [];
    double countStar;
    final reviews = await db
        .collection('stores')
        .doc(widget.firestoreDocID)
        .collection('reviewers')
        .get();
    if (reviews.docs.isNotEmpty) {
      for (var element in reviews.docs) {
        getRating.add({"rate": element.get('rate')});
      }
      countStar = (getRating
              .map<double>((m) => double.parse(m["rate"]))
              .reduce((a, b) => a + b)) /
          reviews.docs.length;
    } else {
      countStar = 0;
    }

    db
        .collection('stores')
        .doc(widget.firestoreDocID)
        .get()
        .then((DocumentSnapshot documentSnapshot) {
      if (documentSnapshot.exists) {
        setState(() {
          name = documentSnapshot.get('name');
          address = documentSnapshot.get('address');
          thumbnail = documentSnapshot.get('thumbnail');
          numberPhone = documentSnapshot.get('numberPhone');
          lat = documentSnapshot.get('lat');
          lng = documentSnapshot.get('lng');
          isVerified = documentSnapshot.get('isVerified');
          star = countStar;
        });
      } else {
        setState(() {
          name = 'Toko tidak ditemukan';
        });
      }
    });
  }

  getOperationTime() async {
    var date = DateTime.now();
    FirebaseFirestore.instance
        .collection('stores')
        .doc(widget.firestoreDocID)
        .collection('operationTime')
        .doc((date.weekday).toString())
        .get()
        .then((DocumentSnapshot documentSnapshot) {
      if (documentSnapshot.exists) {
        setState(() {
          day = documentSnapshot.get('day');
          openHours = documentSnapshot.get('openHours');
          closeHours = documentSnapshot.get('closeHours');
        });
      }
    });
  }

  operationTimeStatus(day, open, close) {
    var date = DateTime.now();
    if (day == null) {
      return 'INSTATUS';
    } else if (double.parse(open) <= date.hour &&
        double.parse(close) >= date.hour) {
      return 'OPEN';
    } else {
      return 'CLOSE';
    }
  }

  Future getAboutStore() async {
    final aboutStore = await FirebaseFirestore.instance
        .collection('stores')
        .doc(widget.firestoreDocID)
        .get();
    return aboutStore;
  }

  Future getListOperationTime() async {
    final aboutStore = await FirebaseFirestore.instance
        .collection('stores')
        .doc(widget.firestoreDocID)
        .collection('operationTime')
        .get();
    return aboutStore;
  }

  Future<QuerySnapshot> loadServiceofStore({String id}) {
    return FirebaseFirestore.instance
        .collection('stores')
        .doc(id)
        .collection('services')
        .get();
  }

  Future<QuerySnapshot> loadOffersOfStore({String id}) {
    return FirebaseFirestore.instance
        .collection('stores')
        .doc(id)
        .collection('offers')
        .get();
  }

  Future<QuerySnapshot> reviewersList({String id}) {
    return FirebaseFirestore.instance
        .collection('stores')
        .doc(id)
        .collection('reviewers')
        .get();
  }

  Future setRatingStore({String id}) async {
    var storeRate = [];
    var rateResult = [];
    final reviews =
        await db.collection('stores').doc(id).collection('reviewers').get();

    if (reviews.docs.isNotEmpty) {
      for (var element in reviews.docs) {
        storeRate.add({"rate": element.get('rate')});
      }
      var sum = (storeRate
              .map<double>((m) => double.parse(m["rate"]))
              .reduce((a, b) => a + b)) /
          reviews.docs.length;

      final getFiveLength =
          storeRate.where((e) => double.parse(e['rate']) >= 5.0).length;
      final getFourLength = storeRate
          .where((e) =>
              double.parse(e['rate']) >= 4.0 && double.parse(e['rate']) <= 4.9)
          .length;
      final getThreeLength = storeRate
          .where((e) =>
              double.parse(e['rate']) >= 3.0 && double.parse(e['rate']) <= 3.9)
          .length;
      final getTwoLength = storeRate
          .where((e) =>
              double.parse(e['rate']) >= 2.0 && double.parse(e['rate']) <= 2.9)
          .length;
      final getOneLength = storeRate
          .where((e) =>
              double.parse(e['rate']) >= 1.0 && double.parse(e['rate']) <= 1.9)
          .length;

      var fiveStar = (((getFiveLength / reviews.docs.length) * 100) / 100);
      var fourStar = (((getFourLength / reviews.docs.length) * 100) / 100);
      var threeStar = (((getThreeLength / reviews.docs.length) * 100) / 100);
      var twoStar = (((getTwoLength / reviews.docs.length) * 100) / 100);
      var oneStar = (((getOneLength / reviews.docs.length) * 100) / 100);

      rateResult.add({
        "average": sum,
        "fiveStar": fiveStar,
        "fourStar": fourStar,
        "threeStar": threeStar,
        "twoStar": twoStar,
        "oneStar": oneStar
      });
    }
    return rateResult;
  }

  @override
  void initState() {
    super.initState();
    loadStore();
    getOperationTime();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: background,
      body: DefaultTabController(
        length: 4,
        child: NestedScrollView(
          headerSliverBuilder: (BuildContext context, bool innerBoxIsScrolled) {
            Colors.white;
            return <Widget>[
              SliverAppBar(
                pinned: true,
                floating: false,
                backgroundColor: primaryColor,
                expandedHeight: MediaQuery.of(context).size.width * 0.50,
                title: SABT(child: Text((name == null) ? 'Load...' : name)),
                flexibleSpace: FlexibleSpaceBar(
                  background: Container(
                    decoration: BoxDecoration(
                      image: DecorationImage(
                        image: NetworkImage(
                            (thumbnail == null)
                                ? 'https://ik.imagekit.io/n0t5masg5jg/stores/default_store_iRhg15nJP.jpg?updatedAt=1638677916449'
                                : thumbnail,
                            scale: 1.0),
                        fit: BoxFit.cover,
                      ),
                    ),
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.end,
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Container(
                          padding: const EdgeInsets.symmetric(
                              horizontal: 15.0, vertical: 5),
                          color: Colors.black12.withOpacity(0.4),
                          width: double.infinity,
                          child: Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                              Text(
                                (name == null) ? '' : name,
                                overflow: TextOverflow.ellipsis,
                                style: const TextStyle(
                                  fontSize: 22,
                                  color: Colors.white,
                                  letterSpacing: 0.7,
                                  fontWeight: FontWeight.bold,
                                ),
                              ),
                              Row(
                                children: [
                                  Flexible(
                                    flex: 5,
                                    child: Text(
                                      (address == null) ? '' : address,
                                      softWrap: true,
                                      overflow: TextOverflow.ellipsis,
                                      style: const TextStyle(
                                        fontSize: 15,
                                        color: Colors.white,
                                        fontWeight: FontWeight.w500,
                                      ),
                                    ),
                                  ),
                                ],
                              ),
                              Row(
                                children: [
                                  Row(
                                    children: [
                                      RatingBarIndicator(
                                        unratedColor: Colors.grey[200],
                                        rating: (star == null) ? 0 : star,
                                        itemBuilder: (context, index) =>
                                            const Icon(
                                          Icons.star,
                                          color: Colors.amberAccent,
                                        ),
                                        itemCount: 5,
                                        itemSize: 18,
                                        direction: Axis.horizontal,
                                      ),
                                      const SizedBox(width: 3),
                                      Text(
                                        (star == null)
                                            ? '0'
                                            : (star).toStringAsFixed(1),
                                        style: const TextStyle(
                                            color: Colors.white,
                                            fontSize: 17,
                                            fontWeight: FontWeight.bold),
                                      )
                                    ],
                                  ),
                                  const Spacer(),
                                  Badge(
                                    toAnimate: false,
                                    shape: BadgeShape.square,
                                    badgeColor: (operationTimeStatus(
                                                day, openHours, closeHours) ==
                                            'CLOSE')
                                        ? Colors.red
                                        : Colors.green,
                                    borderRadius: BorderRadius.circular(12),
                                    padding: const EdgeInsets.symmetric(
                                        horizontal: 10, vertical: 4),
                                    badgeContent: Text(
                                      (day != null)
                                          ? operationTimeStatus(
                                              day, openHours, closeHours)
                                          : 'Unknow',
                                      style: const TextStyle(
                                        color: Colors.white,
                                        fontSize: 12,
                                      ),
                                    ),
                                  ),
                                ],
                              ),
                            ],
                          ),
                        )
                      ],
                    ),
                  ),
                ),
              ),
              SliverAppBar(
                pinned: false,
                floating: false,
                primary: false,
                toolbarHeight: 75.0,
                automaticallyImplyLeading: false,
                flexibleSpace: FlexibleSpaceBar(
                  background: Container(
                    color: Colors.white,
                    padding:
                        const EdgeInsets.symmetric(horizontal: 10, vertical: 0),
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        StoreButtonAction(
                          onPressed: () {
                            if (numberPhone != null) {
                              launch('tel:' +
                                  ((numberPhone == null) ? '0' : numberPhone));
                            }
                          },
                          icon: Icon(Icons.phone,
                              color: (numberPhone != null)
                                  ? primaryColor
                                  : Colors.grey[300]),
                          title: 'Phone',
                        ),
                        StoreButtonAction(
                          onPressed: () {
                            if (numberPhone != null) {
                              launch('sms:' +
                                  ((numberPhone == null) ? '0' : numberPhone));
                            }
                          },
                          icon: Icon(Icons.mail_outline,
                              color: (numberPhone != null)
                                  ? primaryColor
                                  : Colors.grey[300]),
                          title: 'Message',
                        ),
                        StoreButtonAction(
                          onPressed: () {
                            if (lat != null) {
                              String url =
                                  'https://www.google.com/maps/search/?api=1&query=$lat,$lng';
                              launch(url);
                            }
                          },
                          icon: const Icon(
                            Icons.place_rounded,
                            color: primaryColor,
                          ),
                          title: 'Location',
                        ),
                        StoreButtonAction(
                          onPressed: () =>
                              const Tooltip(message: 'Akun terverifikasi'),
                          icon: Icon(
                            Icons.verified_user,
                            color: (isVerified != null)
                                ? primaryColor
                                : Colors.grey[300],
                          ),
                          title:
                              (isVerified != null) ? 'Verified' : 'Un Verified',
                        ),
                      ],
                    ),
                  ),
                ),
              ),
              SliverPersistentHeader(
                delegate: MySliverPersistentHeaderDelegate(
                  const TabBar(
                    indicatorColor: primaryColor,
                    labelColor: primaryColor,
                    unselectedLabelColor: Colors.black54,
                    isScrollable: true,
                    tabs: [
                      Tab(child: TabTitle(title: "SERVICE")),
                      Tab(child: TabTitle(title: "OFFERS")),
                      Tab(child: TabTitle(title: "ABOUT")),
                      Tab(child: TabTitle(title: "REVIEWS")),
                    ],
                  ),
                ),
                pinned: true,
              ),
            ];
          },
          body: TabBarView(
            children: <Widget>[
              FutureBuilder(
                future: loadServiceofStore(id: widget.firestoreDocID),
                builder: (context, snapshot) {
                  if (snapshot.connectionState == ConnectionState.done) {
                    if (snapshot.hasData) {
                      if (!snapshot.hasData || snapshot.data.docs.isEmpty) {
                        return Column(
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: [
                            Image.asset(
                              'assets/images/empty.png',
                              scale: 5,
                            ),
                            const SizedBox(height: 16),
                            const Text('Service tidak ditemukan',
                                style: TextStyle(
                                  fontSize: 20,
                                  fontWeight: FontWeight.bold,
                                ))
                          ],
                        );
                      } else {
                        return MediaQuery.removePadding(
                          context: context,
                          removeTop: true,
                          child: ListView.builder(
                            shrinkWrap: true,
                            itemCount: snapshot.data.docs.length,
                            itemBuilder: (BuildContext context, int index) {
                              return ServiceStore(
                                isStoreOpen: operationTimeStatus(
                                    day, openHours, closeHours),
                                title: snapshot.data.docs[index]['name'],
                                processTime: snapshot.data.docs[index]
                                    ['processTime'],
                                deliveryCost: snapshot.data.docs[index]
                                    ['costDelivery'],
                                description: snapshot.data.docs[index]
                                    ['description'],
                                thumbnail: snapshot.data.docs[index]
                                    ['thumbnail'],
                                price: nominal.format(int.parse(
                                    snapshot.data.docs[index]['prices'])),
                                unit: snapshot.data.docs[index]['unit'],
                              );
                            },
                          ),
                        );
                      }
                    }
                  }
                  return const Center(child: CircularProgressIndicator());
                },
              ),
              // Offers container
              FutureBuilder(
                future: loadOffersOfStore(id: widget.firestoreDocID),
                builder: (context, snapshot) {
                  if (snapshot.connectionState == ConnectionState.done) {
                    if (!snapshot.hasData || snapshot.data.docs.isEmpty) {
                      return Column(
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: [
                          Image.asset(
                            'assets/images/empty.png',
                            scale: 5,
                          ),
                          const SizedBox(height: 16),
                          const Text('Belum ada penawaran baru',
                              style: TextStyle(
                                fontSize: 20,
                                fontWeight: FontWeight.bold,
                              ))
                        ],
                      );
                    } else {
                      return MediaQuery.removePadding(
                        context: context,
                        removeTop: true,
                        child: ListView.builder(
                            physics: const NeverScrollableScrollPhysics(),
                            shrinkWrap: true,
                            itemCount: snapshot.data.docs.length,
                            itemBuilder: (BuildContext context, int index) {
                              return VoucherCard(
                                bigTitle: snapshot.data.docs[index]['title'],
                                title: snapshot.data.docs[index]['subtitle'],
                                voucher: snapshot.data.docs[index]['theme'],
                                date: snapshot.data.docs[index]['limitDate'],
                              );
                            }),
                      );
                    }
                  }
                  return const Center(child: CircularProgressIndicator());
                },
              ),
              // About container
              FutureBuilder(
                  future:
                      Future.wait([getAboutStore(), getListOperationTime()]),
                  builder: (context, snapshot) {
                    if (snapshot.connectionState == ConnectionState.done) {
                      try {
                        final about = snapshot.data[0]['about'];
                        final address = snapshot.data[0]['address'];
                        return SingleChildScrollView(
                          child: Container(
                            padding: const EdgeInsets.all(15),
                            color: Colors.white,
                            child: Column(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: [
                                const Text(
                                  'About Us',
                                  style: TextStyle(
                                    fontSize: 20,
                                    fontWeight: FontWeight.w700,
                                  ),
                                ),
                                const Divider(),
                                Text(
                                  about,
                                  style: const TextStyle(
                                    fontSize: 16,
                                    fontWeight: FontWeight.w400,
                                  ),
                                ),
                                const SizedBox(height: 25),
                                const Text(
                                  'Address',
                                  style: TextStyle(
                                    fontSize: 20,
                                    fontWeight: FontWeight.w700,
                                  ),
                                ),
                                const Divider(),
                                Text(
                                  address,
                                  style: const TextStyle(
                                    fontSize: 16,
                                    fontWeight: FontWeight.w400,
                                  ),
                                ),
                                const SizedBox(height: 25),
                                const Text(
                                  'Jam Operasi',
                                  style: TextStyle(
                                    fontSize: 20,
                                    fontWeight: FontWeight.w700,
                                  ),
                                ),
                                const Divider(),
                                MediaQuery.removePadding(
                                  context: context,
                                  removeTop: true,
                                  child: ListView.builder(
                                      physics:
                                          const NeverScrollableScrollPhysics(),
                                      shrinkWrap: true,
                                      itemCount: snapshot.data[1].docs.length,
                                      itemBuilder:
                                          (BuildContext context, int index) {
                                        return OpeningHours(
                                            status: Colors.green,
                                            day: snapshot.data[1].docs[index]
                                                ['dayID'],
                                            hour: snapshot.data[1].docs[index]
                                                    ['openHours'] +
                                                ' - ' +
                                                snapshot.data[1].docs[index]
                                                    ['closeHours']);
                                      }),
                                ),
                              ],
                            ),
                          ),
                        );
                      } catch (e) {
                        return Column(
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: [
                            Image.asset(
                              'assets/images/empty.png',
                              scale: 5,
                            ),
                            const SizedBox(height: 16),
                            const Text('Informasi toko belum tersedia',
                                style: TextStyle(
                                  fontSize: 20,
                                  fontWeight: FontWeight.bold,
                                ))
                          ],
                        );
                      }
                    }
                    return const Center(child: CircularProgressIndicator());
                  }),
              // Reviews container
              FutureBuilder(
                future: Future.wait([
                  reviewersList(id: widget.firestoreDocID),
                  setRatingStore(id: widget.firestoreDocID),
                ]),
                builder: (context, snapshot) {
                  if (snapshot.connectionState == ConnectionState.done) {
                    final reviewerList = snapshot.data[0];
                    final getRateResult = snapshot.data[1];
                    if (reviewerList.docs.isEmpty) {
                      return Column(
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: [
                          Image.asset(
                            'assets/images/empty.png',
                            scale: 5,
                          ),
                          const SizedBox(height: 16),
                          const Text('Belum ada komentar',
                              style: TextStyle(
                                fontSize: 20,
                                fontWeight: FontWeight.bold,
                              ))
                        ],
                      );
                    } else {
                      return SingleChildScrollView(
                        padding: const EdgeInsets.all(10),
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            const SubtitleStore(title: 'Rating'),
                            Row(
                              children: [
                                Expanded(
                                  flex: 2,
                                  child: RatingScores(
                                    starCount: (getRateResult[0]['average'])
                                        .toStringAsFixed(1),
                                    star: double.parse((getRateResult[0]
                                            ['average'])
                                        .toStringAsFixed(1)),
                                    reviewCount: (snapshot.data[0].docs.length)
                                            .toString() +
                                        ' Reviews',
                                  ),
                                ),
                                Expanded(
                                  flex: 3,
                                  child: PercentageScore(
                                    percentFive: getRateResult[0]['fiveStar'],
                                    percentFour: getRateResult[0]['fourStar'],
                                    percentThree: getRateResult[0]['threeStar'],
                                    percentTwo: getRateResult[0]['twoStar'],
                                    percentOne: getRateResult[0]['oneStar'],
                                  ),
                                )
                              ],
                            ),
                            const SizedBox(height: 10),
                            const SubtitleStore(title: 'Reviews'),
                            MediaQuery.removePadding(
                              context: context,
                              removeTop: true,
                              child: ListView.builder(
                                  physics: const NeverScrollableScrollPhysics(),
                                  shrinkWrap: true,
                                  itemCount: snapshot.data[0].docs.length,
                                  itemBuilder:
                                      (BuildContext context, int index) {
                                    Timestamp t = snapshot.data[0].docs[index]
                                        ['dateReviews'];
                                    DateTime d = t.toDate();
                                    var time =
                                        DateFormat("d MMMM yyyy", "id_ID")
                                            .format(d);
                                    return Riview(
                                      name: snapshot.data[0].docs[index]
                                          ['displayName'],
                                      date: time,
                                      image: snapshot.data[0].docs[index]
                                              ['photoUrl'] ??
                                          'https://ik.imagekit.io/n0t5masg5jg/stores/photoUsers/user_Bp0tCB1Pd.png?ik-sdk-version=javascript-1.4.3&updatedAt=1642516788106',
                                      rate: double.parse(
                                          snapshot.data[0].docs[index]['rate']),
                                      comment: snapshot.data[0].docs[index]
                                          ['comment'],
                                    );
                                  }),
                            ),
                          ],
                        ),
                      );
                    }
                  }
                  return const Center(child: CircularProgressIndicator());
                },
              ),
              // Reviews container
            ],
          ),
        ),
      ),
    );
  }
}

class MySliverPersistentHeaderDelegate extends SliverPersistentHeaderDelegate {
  MySliverPersistentHeaderDelegate(this._tabBar);

  final TabBar _tabBar;

  @override
  double get minExtent => _tabBar.preferredSize.height;
  @override
  double get maxExtent => _tabBar.preferredSize.height;

  @override
  Widget build(
      BuildContext context, double shrinkOffset, bool overlapsContent) {
    return Container(
      alignment: Alignment.center,
      decoration: BoxDecoration(
        color: Colors.white,
        boxShadow: [
          BoxShadow(
            color: Colors.grey.withOpacity(0.2),
            spreadRadius: 1,
            blurRadius: 1,
          ),
        ],
      ),
      child: _tabBar,
    );
  }

  @override
  bool shouldRebuild(MySliverPersistentHeaderDelegate oldDelegate) {
    return false;
  }
}

class SABT extends StatefulWidget {
  final Widget child;
  const SABT({
    Key key,
    @required this.child,
  }) : super(key: key);
  @override
  _SABTState createState() {
    return _SABTState();
  }
}

class _SABTState extends State<SABT> {
  ScrollPosition _position;
  bool _visible;
  @override
  void dispose() {
    _removeListener();
    super.dispose();
  }

  @override
  void didChangeDependencies() {
    super.didChangeDependencies();
    _removeListener();
    _addListener();
  }

  void _addListener() {
    _position = Scrollable.of(context)?.position;
    _position?.addListener(_positionListener);
    _positionListener();
  }

  void _removeListener() {
    _position?.removeListener(_positionListener);
  }

  void _positionListener() {
    final FlexibleSpaceBarSettings settings =
        context.dependOnInheritedWidgetOfExactType<FlexibleSpaceBarSettings>();
    bool visible =
        settings == null || settings.currentExtent <= settings.minExtent;
    if (_visible != visible) {
      setState(() {
        _visible = visible;
      });
    }
  }

  @override
  Widget build(BuildContext context) {
    return Visibility(
      visible: _visible,
      child: widget.child,
    );
  }
}
