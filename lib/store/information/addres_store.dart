import 'package:cleanis_store/common_widget/title_form.dart';
import 'package:cleanis_store/config/colors.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:geolocator/geolocator.dart';
import 'package:universe/universe.dart';

class AddressStore extends StatefulWidget {
  final String docID, lat, lng, address;
  const AddressStore({Key key, this.docID, this.lat, this.lng, this.address})
      : super(key: key);

  @override
  _AddressStoreState createState() => _AddressStoreState();
}

class _AddressStoreState extends State<AddressStore> {
  final _mapKey = UniqueKey();
  List<Marker> markers = [];
  Position position;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      resizeToAvoidBottomInset: false,
      appBar: AppBar(
        backgroundColor: primaryColor,
        title: const Text('Edit Address Store'),
      ),
      body: U.MapBox(
        accessToken:
            'pk.eyJ1IjoienllbCIsImEiOiJja3ZoY3RraWI1eGE4MzFxd21paGlyZGQ5In0.-Zcr5egbG9bgHhq856uN5Q',
        key: _mapKey,
        center: [double.parse(widget.lat), double.parse(widget.lng)],
        showLocationIndicator: true,
        showCenterMarker: true,
        showLocationMarker: true,
        zoom: 16,
        centerMarker: U.MarkerLayer(
          markers,
          widget: const MarkerImage('assets/images/store_marker.png'),
          size: 48,
        ),
        markers: U.MarkerLayer(
          [double.parse(widget.lat), double.parse(widget.lng)],
          widget: const MarkerImage('assets/images/shop_marker.png'),
          size: 58,
          onTap: (position, latlng) {
            showModalBottomSheet(
              context: context,
              backgroundColor: Colors.transparent,
              isDismissible: true,
              isScrollControlled: true,
              builder: (context) => EditAddres(
                newLat: double.parse(widget.lat),
                newLng: double.parse(widget.lng),
                address: widget.address,
              ),
            );
          },
        ),
        layers: [
          U.MarkerLayer(
            markers,
            widget: const MarkerImage('assets/images/store_marker.png'),
            size: 48,
          ),
        ],
        onTap: (latlng) {
          if (markers.isEmpty) {
            if (mounted) {
              setState(() {
                Marker marker = U.Marker(latlng, data: latlng);
                markers.add(marker);
              });
            }
          } else if (markers.isNotEmpty) {
            markers.remove(markers.first);
            setState(() {
              Marker marker = U.Marker(latlng, data: latlng);
              markers.add(marker);
            });
          }
          showModalBottomSheet(
            context: context,
            backgroundColor: Colors.transparent,
            isDismissible: true,
            isScrollControlled: true,
            builder: (context) => EditAddres(
              newLat: latlng.lat,
              newLng: latlng.lng,
              address: '',
            ),
          );
        },
      ),
    );
  }
}

class EditAddres extends StatefulWidget {
  final String address;
  final double newLat;
  final double newLng;
  const EditAddres({Key key, this.newLat, this.newLng, this.address})
      : super(key: key);

  @override
  State<EditAddres> createState() => _EditAddresState();
}

class _EditAddresState extends State<EditAddres> {
  final uid = FirebaseAuth.instance.currentUser.uid;

  final db = FirebaseFirestore.instance;
  bool error = false;
  Future updateAddress({
    String docID,
    String address,
    String lat,
    String lng,
  }) {
    try {
      return db.collection('stores').doc(uid).update({
        'address': address,
        'lat': lat,
        'lng': lng,
      });
    } catch (e) {
      return e.code;
    }
  }

  @override
  Widget build(BuildContext context) {
    final _addressController = TextEditingController()..text = widget.address;
    return SingleChildScrollView(
      child: Padding(
        padding:
            EdgeInsets.only(bottom: MediaQuery.of(context).viewInsets.bottom),
        child: Container(
          padding: const EdgeInsets.symmetric(vertical: 30, horizontal: 20),
          decoration: const BoxDecoration(
            color: Colors.white,
            borderRadius: BorderRadius.only(
              topLeft: Radius.circular(15.0),
              topRight: Radius.circular(15.0),
            ),
          ),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              const TitleForm(title: "Alamat lengkap"),
              (error == true)
                  ? const Text('Alamat tidak boleh kosong',
                      style: TextStyle(
                        color: Colors.red,
                        fontSize: 16,
                      ))
                  : const SizedBox(),
              Container(
                color: Colors.white,
                child: TextFormField(
                  controller: _addressController,
                  validator: (val) => val.isEmpty ? 'Field is required' : null,
                  keyboardType: TextInputType.multiline,
                  minLines: 2,
                  maxLines: 5,
                  decoration: const InputDecoration(
                    border: OutlineInputBorder(),
                    isDense: true,
                    hintText: 'Alamat toko lengkap',
                    alignLabelWithHint: true,
                  ),
                ),
              ),
              ElevatedButton(
                child: const Text(
                  'Simpan',
                  style: TextStyle(
                      fontSize: 18,
                      fontWeight: FontWeight.bold,
                      color: Colors.white),
                ),
                onPressed: () {
                  if (_addressController.text != '') {
                    updateAddress(
                      address: _addressController.text,
                      lat: (widget.newLat).toString(),
                      lng: (widget.newLng).toString(),
                    );
                    int count = 0;
                    Navigator.popUntil(context, (route) {
                      return count++ == 2;
                    });
                  } else {
                    setState(() {
                      error = true;
                    });
                  }
                },
                style: ButtonStyle(
                  backgroundColor:
                      MaterialStateProperty.all<Color>(primaryColor),
                ),
              )
            ],
          ),
        ),
      ),
    );
  }
}
