import 'package:cleanis_store/common_widget/title_form.dart';
import 'package:cleanis_store/config/colors.dart';
import 'package:cleanis_store/store/operation_time/operation_time.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';

import 'addres_store.dart';

class InformationEdit extends StatefulWidget {
  final String docID;
  const InformationEdit({Key key, this.docID}) : super(key: key);

  @override
  _InformationEditState createState() => _InformationEditState();
}

class _InformationEditState extends State<InformationEdit> {
  final uid = FirebaseAuth.instance.currentUser.uid;
  final db = FirebaseFirestore.instance;

  final _formKey = GlobalKey<FormState>();

  Future loadStore() async {
    final service = await db.collection('stores').doc(uid).get();
    return service;
  }

  Future updateStoreInformation({
    String name,
    String about,
    String numberPhone,
  }) {
    try {
      return db.collection('stores').doc(uid).update({
        'name': name,
        'about': about,
        'numberPhone': numberPhone,
      });
    } catch (e) {
      return e.code;
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: primaryColor,
        title: const Text('Store Information'),
      ),
      body: FutureBuilder(
        builder: (context, snapshot) {
          if (snapshot.connectionState == ConnectionState.done) {
            final service = snapshot.data;
            final _nameController = TextEditingController()
              ..text = service['name'];
            final _aboutController = TextEditingController()
              ..text = service['about'];
            final _phoneController = TextEditingController()
              ..text = service['numberPhone'];
            return SingleChildScrollView(
              child: Container(
                padding: const EdgeInsets.fromLTRB(15, 0, 15, 15),
                child: Form(
                  key: _formKey,
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      const SizedBox(height: 10),
                      Row(
                        children: [
                          Expanded(
                            child: SizedBox(
                              width: double.infinity,
                              height: 40,
                              child: ElevatedButton(
                                child: const Text(
                                  'ALAMAT TOKO',
                                  style: TextStyle(
                                      fontSize: 18,
                                      fontWeight: FontWeight.bold,
                                      color: Colors.white),
                                ),
                                onPressed: () {
                                  Navigator.push(
                                    context,
                                    MaterialPageRoute(
                                      builder: (context) => AddressStore(
                                        docID: service.id,
                                        lat: service['lat'],
                                        lng: service['lng'],
                                        address: service['address'],
                                      ),
                                    ),
                                  ).then((_) => {setState(() {})});
                                },
                                style: ButtonStyle(
                                  backgroundColor:
                                      MaterialStateProperty.all<Color>(
                                          primaryColor),
                                ),
                              ),
                            ),
                          ),
                          const SizedBox(width: 5),
                          Expanded(
                            child: SizedBox(
                              width: double.infinity,
                              height: 40,
                              child: ElevatedButton(
                                child: const Text(
                                  'JAM OPERASI',
                                  style: TextStyle(
                                      fontSize: 18,
                                      fontWeight: FontWeight.bold,
                                      color: Colors.white),
                                ),
                                onPressed: () {
                                  Navigator.push(
                                    context,
                                    MaterialPageRoute(
                                      builder: (context) =>
                                          const OperationTime(),
                                    ),
                                  ).then((_) => {setState(() {})});
                                },
                                style: ButtonStyle(
                                  backgroundColor:
                                      MaterialStateProperty.all<Color>(
                                          primaryColor),
                                ),
                              ),
                            ),
                          )
                        ],
                      ),
                      const SizedBox(height: 10),
                      const TitleForm(title: "Nama Toko"),
                      Container(
                        color: Colors.white,
                        child: TextFormField(
                          controller: _nameController,
                          validator: (val) =>
                              val.isEmpty ? 'Nama is required' : null,
                          keyboardType: TextInputType.text,
                          decoration: const InputDecoration(
                            border: OutlineInputBorder(),
                            isDense: true,
                            hintText: 'Nama layanan',
                          ),
                        ),
                      ),
                      const TitleForm(title: "Deskripsi"),
                      Container(
                        color: Colors.white,
                        child: TextFormField(
                          controller: _aboutController,
                          validator: (val) =>
                              val.isEmpty ? 'Field is required' : null,
                          keyboardType: TextInputType.multiline,
                          minLines: 2,
                          maxLines: 5,
                          decoration: const InputDecoration(
                            border: OutlineInputBorder(),
                            isDense: true,
                            hintText: 'Deskripsi tentang layanan',
                            alignLabelWithHint: true,
                          ),
                        ),
                      ),
                      const TitleForm(title: "No. HP"),
                      Container(
                        color: Colors.white,
                        child: TextFormField(
                          controller: _phoneController,
                          validator: (val) =>
                              val.isEmpty ? 'Field is required' : null,
                          decoration: const InputDecoration(
                            border: OutlineInputBorder(),
                            isDense: true,
                            hintText: 'Nomor Handphone',
                            alignLabelWithHint: true,
                          ),
                        ),
                      ),
                      const SizedBox(height: 10),
                      SizedBox(
                        width: double.infinity,
                        height: 40,
                        child: ElevatedButton(
                          child: const Text(
                            'SIMPAN',
                            style: TextStyle(
                                fontSize: 18,
                                fontWeight: FontWeight.bold,
                                color: Colors.white),
                          ),
                          onPressed: () {
                            if (_formKey.currentState.validate()) {
                              updateStoreInformation(
                                  name: _nameController.text,
                                  about: _aboutController.text,
                                  numberPhone: _phoneController.text);
                              setState(() {});
                            }
                          },
                          style: ButtonStyle(
                            backgroundColor:
                                MaterialStateProperty.all<Color>(primaryColor),
                          ),
                        ),
                      )
                    ],
                  ),
                ),
              ),
            );
          }
          return const Center(
            child: CircularProgressIndicator(),
          );
        },
        future: loadStore(),
      ),
    );
  }
}
