import 'package:cleanis_store/auth/auth_service.dart';
import 'package:cleanis_store/auth/widgets/header_widget.dart';
import 'package:cleanis_store/config/colors.dart';
import 'package:cleanis_store/config/styles.dart';
import 'package:flutter/gestures.dart';
import 'package:flutter/material.dart';
import '../loading.dart';
import 'widgets/subtitle_widget.dart';
import 'package:email_validator/email_validator.dart';

class LoginScreen extends StatefulWidget {
  final Function toggleView;
  const LoginScreen({key, this.toggleView}) : super(key: key);

  @override
  _LoginScreenState createState() => _LoginScreenState();
}

class _LoginScreenState extends State<LoginScreen> {
  bool _isLoading = false;
  final _formKey = GlobalKey<FormState>();
  final AuthService authService = AuthService();
  String error, email, password = '';

  bool _isShowPassword = false;

  void _showPassword() {
    setState(() {
      _isShowPassword = !_isShowPassword;
    });
  }

  void loading() {
    setState(() {
      _isLoading = !_isLoading;
    });
  }

  @override
  Widget build(BuildContext context) {
    return _isLoading
        ? const Loading()
        : Scaffold(
            backgroundColor: background,
            body: SingleChildScrollView(
              child: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                children: <Widget>[
                  const Header(
                    title: 'Cleanis',
                    subtitle: 'On demand app for laundry service',
                  ),
                  Form(
                    key: _formKey,
                    child: Container(
                      decoration: BoxDecoration(
                        color: Colors.white,
                        borderRadius:
                            const BorderRadius.all(Radius.circular(8.0)),
                        border: Border.all(width: 1, color: Colors.grey[300]),
                      ),
                      padding: const EdgeInsets.fromLTRB(15, 0, 15, 15),
                      margin: const EdgeInsets.symmetric(
                          vertical: 30, horizontal: 10),
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: <Widget>[
                          const Subtitle(title: 'Sign in to your account'),
                          const Divider(color: Colors.black26),
                          (error != null)
                              ? Container(
                                  decoration: BoxDecoration(
                                      color: Colors.red[100],
                                      borderRadius: const BorderRadius.all(
                                          Radius.circular(6))),
                                  width: double.infinity,
                                  padding: const EdgeInsets.symmetric(
                                      vertical: 8, horizontal: 8),
                                  child: Text(
                                    error,
                                    style: const TextStyle(
                                        color: Colors.black87,
                                        fontSize: 16,
                                        fontWeight: FontWeight.w600),
                                  ),
                                )
                              : const SizedBox(height: 0),
                          const SizedBox(height: 10),
                          Container(
                            color: Colors.white,
                            child: TextFormField(
                              validator: (email) =>
                                  EmailValidator.validate(email)
                                      ? null
                                      : "Masukkan alamat email",
                              onChanged: (val) {
                                setState(() => email = val);
                              },
                              keyboardType: TextInputType.emailAddress,
                              decoration: const InputDecoration(
                                prefixIcon: Icon(
                                  Icons.email,
                                  size: 24,
                                ),
                                contentPadding: EdgeInsets.zero,
                                border: OutlineInputBorder(),
                                focusColor: primaryColor,
                                fillColor: primaryColor,
                                labelText: 'Email',
                              ),
                            ),
                          ),
                          const SizedBox(height: 15),
                          Container(
                            color: Colors.white,
                            child: TextFormField(
                              obscureText: !_isShowPassword,
                              onChanged: (val) {
                                setState(() => password = val);
                              },
                              validator: (val) => val.isEmpty
                                  ? 'Require password for authenticate'
                                  : null,
                              decoration: InputDecoration(
                                contentPadding: const EdgeInsets.symmetric(
                                    horizontal: 10.0),
                                prefixIcon: const Icon(
                                  Icons.lock,
                                  size: 24,
                                ),
                                border: const OutlineInputBorder(),
                                suffixIcon: GestureDetector(
                                  onTap: () {
                                    _showPassword();
                                  },
                                  child: Icon(
                                    _isShowPassword
                                        ? Icons.visibility
                                        : Icons.visibility_off,
                                    color: _isShowPassword
                                        ? primaryColor
                                        : Colors.grey,
                                  ),
                                ),
                                isDense: true,
                                labelText: 'Password',
                              ),
                            ),
                          ),
                          Padding(
                            padding: const EdgeInsets.symmetric(vertical: 4.0),
                            child: Row(
                              children: const <Widget>[
                                Spacer(),
                                Text(
                                  'Forgot Password',
                                  style: TextStyle(
                                    fontSize: 14,
                                    color: primaryColor,
                                    fontWeight: FontWeight.w800,
                                  ),
                                )
                              ],
                            ),
                          ),
                          SizedBox(
                            width: double.infinity,
                            height: 40,
                            child: ElevatedButton(
                              child: const Text('LOGIN'),
                              onPressed: () async {
                                if (_formKey.currentState.validate()) {
                                  setState(() => _isLoading = !_isLoading);
                                  dynamic authResult = await authService.signIn(
                                    email: email.trim(),
                                    password: password.trim(),
                                  );
                                  if (authResult == 'user-not-found') {
                                    setState(() {
                                      error = 'Account not registered yet';
                                      _isLoading = !_isLoading;
                                    });
                                  } else if (authResult == 'wrong-password') {
                                    setState(() {
                                      error = 'Wrong password, try again';
                                      _isLoading = !_isLoading;
                                    });
                                  } else if (authResult == 'invalid-email') {
                                    setState(() {
                                      error = 'Use valid email';
                                      _isLoading = !_isLoading;
                                    });
                                  } else if (authResult == null) {
                                    setState(() {
                                      error = 'Cannot login with this account';
                                      _isLoading = !_isLoading;
                                    });
                                  } else {
                                    if (!mounted) return;
                                    error = 'Kesalahan tidak terduga';
                                    setState(() {
                                      _isLoading = !_isLoading;
                                    });
                                  }
                                }
                              },
                              style: ElevatedButton.styleFrom(
                                  primary: primaryColor,
                                  textStyle: const TextStyle(
                                    fontSize: 18,
                                    fontWeight: FontWeight.bold,
                                  )),
                            ),
                          )
                        ],
                      ),
                    ),
                  ),
                  Padding(
                    padding: const EdgeInsets.all(30),
                    child: RichText(
                      textAlign: TextAlign.center,
                      text: TextSpan(
                        children: [
                          const TextSpan(
                            text: "Don't have account? ",
                            style: styleTextAccount,
                          ),
                          TextSpan(
                            recognizer: TapGestureRecognizer()
                              ..onTap = () => widget.toggleView(),
                            text: "REGISTER HERE",
                            style: styleTextPage,
                          ),
                        ],
                      ),
                    ),
                  ),
                ],
              ),
            ),
          );
  }
}
