import 'package:flutter/material.dart';

class Subtitle extends StatelessWidget {
  final String title;
  const Subtitle({
    Key key,
    this.title,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.only(top: 20),
      child: Text(
        title,
        style: const TextStyle(fontSize: 22, fontWeight: FontWeight.w400),
      ),
    );
  }
}
