import 'package:cleanis_store/config/colors.dart';
import 'package:flutter/material.dart';

class Header extends StatelessWidget {
  final String title;
  final String subtitle;
  const Header({
    Key key,
    this.title,
    this.subtitle,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      width: double.infinity,
      padding: const EdgeInsets.only(top: 40),
      child: Column(
        children: [
          const SizedBox(height: 20),
          Image.asset('assets/images/logo.png', scale: 5),
          const SizedBox(height: 10),
          Text(
            title,
            style: const TextStyle(
              fontSize: 32,
              color: primaryColor,
              fontWeight: FontWeight.bold,
            ),
          ),
          Text(
            subtitle,
            style: const TextStyle(
              fontSize: 18,
              color: primaryDarkenColor,
              fontWeight: FontWeight.w400,
            ),
          ),
        ],
      ),
    );
  }
}
