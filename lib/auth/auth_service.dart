import 'package:cleanis_store/auth/user_models.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_auth/firebase_auth.dart';

class AuthService {
  final db = FirebaseFirestore.instance;
  final FirebaseAuth firebaseAuth = FirebaseAuth.instance;

  // * create user object based on firebaseauth 'User'
  Users _userFromFirebase(User user) {
    return user != null ? Users(uid: user.uid) : null;
  }

  // * Auth change user stream
  Stream<User> get authStateChanges => firebaseAuth.idTokenChanges();

  // GET UID
  Future<String> getCurrentUID() async {
    return firebaseAuth.currentUser.uid;
  }

  // GET CURRENT USER
  Future getCurrentUser() async {
    // * this line show circular loading
    firebaseAuth.currentUser.reload();
    return firebaseAuth.currentUser;
  }

  Future getCurrentProfile() async {
    String uid = firebaseAuth.currentUser.uid;
    final userProfile = await db.collection('usersProfile').doc(uid).get();
    if (userProfile == null || !userProfile.exists) {
      return null;
    } else {
      return userProfile;
    }
  }

  Future signUp({String email, String password, String name}) async {
    try {
      UserCredential authResult = await firebaseAuth
          .createUserWithEmailAndPassword(email: email, password: password);
      firebaseAuth.currentUser.updateDisplayName(name);
      User user = authResult.user;
      return _userFromFirebase(user);
    } on FirebaseAuthException catch (e) {
      return e.code;
    } catch (e) {
      return null;
    }
  }

  Future changeProfile({String displayName, String phoneNumber}) async {
    String uid = firebaseAuth.currentUser.uid;
    try {
      await db
          .collection('usersProfile')
          .doc(uid)
          .set({'displayName': displayName, 'phoneNumber': phoneNumber});
      dynamic result = firebaseAuth.currentUser.updateDisplayName(displayName);
      firebaseAuth.currentUser.reload();
      return result;
    } on FirebaseAuthException catch (e) {
      return e.code;
    } catch (e) {
      return e;
    }
  }

  Future<bool> updatePhotoUrl({String downloadUrl}) async {
    await FirebaseAuth.instance.currentUser.updatePhotoURL(downloadUrl);
    return true;
  }

  // * Sign in use email password
  Future signIn({String email, password}) async {
    try {
      UserCredential authResult = await firebaseAuth.signInWithEmailAndPassword(
        email: email,
        password: password,
      );
      User user = authResult.user;
      return _userFromFirebase(user);
    } on FirebaseAuthException catch (e) {
      return e.code;
    } catch (e) {
      return null;
    }
  }

  // * Logout
  Future<void> signOut() async {
    await firebaseAuth.signOut();
  }

  Future<bool> validatePassword(String password) async {
    var firebaseUser = firebaseAuth.currentUser;

    var authCredentials = EmailAuthProvider.credential(
        email: firebaseUser.email, password: password);
    try {
      var authResult =
          await firebaseUser.reauthenticateWithCredential(authCredentials);
      return authResult.user != null;
    } catch (e) {
      return false;
    }
  }
}
