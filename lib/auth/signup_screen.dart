import 'package:cleanis_store/auth/widgets/header_widget.dart';
import 'package:cleanis_store/config/colors.dart';
import 'package:cleanis_store/config/styles.dart';
import 'package:email_validator/email_validator.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/gestures.dart';
import 'package:flutter/material.dart';
import '../loading.dart';
import 'auth_service.dart';
import 'widgets/subtitle_widget.dart';

class SignUpScreen extends StatefulWidget {
  final Function toggleView;
  const SignUpScreen({Key key, this.toggleView}) : super(key: key);

  @override
  _SignUpScreenState createState() => _SignUpScreenState();
}

class _SignUpScreenState extends State<SignUpScreen> {
  bool _isLoading = false;
  final _formKey = GlobalKey<FormState>();
  final AuthService authService = AuthService();
  final FirebaseAuth firebaseAuth = FirebaseAuth.instance;
  String error, name, email, password = '';

  bool _isShowPassword = false;

  void _showPassword() {
    setState(() {
      _isShowPassword = !_isShowPassword;
    });
  }

  void loading() {
    setState(() {
      _isLoading = !_isLoading;
    });
  }

  @override
  Widget build(BuildContext context) {
    return _isLoading
        ? const Loading()
        : Scaffold(
            backgroundColor: background,
            body: SingleChildScrollView(
              child: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                children: <Widget>[
                  const Header(
                    title: 'Cleanis',
                    subtitle: 'Clean up what your mess up',
                  ),
                  Container(
                    decoration: BoxDecoration(
                      color: Colors.white,
                      borderRadius:
                          const BorderRadius.all(Radius.circular(8.0)),
                      border: Border.all(width: 1, color: Colors.grey[300]),
                    ),
                    padding: const EdgeInsets.fromLTRB(15, 0, 15, 15),
                    margin: const EdgeInsets.symmetric(
                        vertical: 30, horizontal: 10),
                    child: Form(
                      key: _formKey,
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: <Widget>[
                          const Subtitle(title: 'Enter your information below'),
                          const Divider(color: Colors.black26),
                          const SizedBox(height: 10),
                          (error != null)
                              ? Text(
                                  error,
                                  style: const TextStyle(
                                      color: Colors.red,
                                      fontSize: 16,
                                      fontWeight: FontWeight.w600),
                                )
                              : const SizedBox(height: 0),
                          Container(
                            color: Colors.white,
                            child: TextFormField(
                              onChanged: (val) {
                                setState(() => name = val);
                              },
                              validator: (val) =>
                                  val.isEmpty ? 'Name is importants' : null,
                              decoration: const InputDecoration(
                                contentPadding: EdgeInsets.zero,
                                prefixIcon: Icon(Icons.account_circle_rounded),
                                border: OutlineInputBorder(),
                                labelText: 'Name',
                              ),
                            ),
                          ),
                          const SizedBox(height: 10),
                          Container(
                            color: Colors.white,
                            child: TextFormField(
                              validator: (email) =>
                                  EmailValidator.validate(email)
                                      ? null
                                      : "Masukkan alamat email",
                              onChanged: (val) {
                                setState(() => email = val);
                              },
                              keyboardType: TextInputType.emailAddress,
                              decoration: const InputDecoration(
                                prefixIcon: Icon(Icons.email),
                                contentPadding: EdgeInsets.zero,
                                border: OutlineInputBorder(),
                                focusColor: primaryColor,
                                fillColor: primaryColor,
                                labelText: 'Email',
                              ),
                            ),
                          ),
                          const SizedBox(height: 10),
                          Container(
                            color: Colors.white,
                            child: TextFormField(
                              obscureText: !_isShowPassword,
                              onChanged: (val) {
                                setState(() => password = val);
                              },
                              validator: (val) => val.isEmpty
                                  ? 'Require password for authenticate'
                                  : null,
                              decoration: InputDecoration(
                                contentPadding: EdgeInsets.zero,
                                prefixIcon: const Icon(
                                  Icons.lock,
                                  size: 24,
                                ),
                                border: const OutlineInputBorder(),
                                suffixIcon: GestureDetector(
                                  onTap: () {
                                    _showPassword();
                                  },
                                  child: Icon(
                                    _isShowPassword
                                        ? Icons.visibility
                                        : Icons.visibility_off,
                                    color: _isShowPassword
                                        ? primaryColor
                                        : Colors.grey,
                                  ),
                                ),
                                isDense: true,
                                labelText: 'Password',
                              ),
                            ),
                          ),
                          const SizedBox(height: 10),
                          SizedBox(
                            width: double.infinity,
                            height: 40,
                            child: ElevatedButton(
                              child: const Text('Register Now'),
                              onPressed: () async {
                                if (_formKey.currentState.validate()) {
                                  setState(() => loading());
                                  dynamic authResult = await authService.signUp(
                                    email: email.trim(),
                                    password: password.trim(),
                                    name: name,
                                  );
                                  await firebaseAuth.currentUser.reload();
                                  if (authResult == 'email-already-in-use') {
                                    setState(() {
                                      error = 'Email already uses';
                                      loading();
                                    });
                                  } else if (authResult == 'invalid-email') {
                                    setState(() {
                                      error = 'Use valid email';
                                      loading();
                                    });
                                  } else if (authResult == 'weak-password') {
                                    setState(() {
                                      error = 'Your password weak, min 6 chars';
                                      loading();
                                    });
                                  } else if (authResult == null) {
                                    setState(() {
                                      error =
                                          'Cannot registered with this account, try again';
                                      loading();
                                    });
                                  }
                                }
                              },
                              style: ElevatedButton.styleFrom(
                                  primary: primaryColor,
                                  textStyle: const TextStyle(
                                    fontSize: 18,
                                    fontWeight: FontWeight.bold,
                                  )),
                            ),
                          )
                        ],
                      ),
                    ),
                  ),
                  Padding(
                    padding: const EdgeInsets.all(30),
                    child: RichText(
                      textAlign: TextAlign.center,
                      text: TextSpan(
                        children: [
                          const TextSpan(
                            text: "Don't have account? ",
                            style: styleTextAccount,
                          ),
                          TextSpan(
                            recognizer: TapGestureRecognizer()
                              ..onTap = () => widget.toggleView(),
                            text: "LOGIN NOW",
                            style: styleTextPage,
                          ),
                        ],
                      ),
                    ),
                  ),
                ],
              ),
            ),
          );
  }
}
