import 'package:cleanis_store/auth/auth_service.dart';
import 'package:cleanis_store/config/colors.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';

import '../loading.dart';
import 'widget/submitbutton_widget.dart';
import 'widget/title_widget.dart';

class PasswordScreen extends StatefulWidget {
  const PasswordScreen({Key key}) : super(key: key);

  @override
  _PasswordScreenState createState() => _PasswordScreenState();
}

class _PasswordScreenState extends State<PasswordScreen> {
  final FirebaseAuth firebaseAuth = FirebaseAuth.instance;
  final AuthService authService = AuthService();
  final _formKey = GlobalKey<FormState>();
  bool _isShowPassword = false;
  bool loading = false;
  String oldFormPassword, newFormPassword, checkPassword;

  void _showPassword() {
    setState(() {
      _isShowPassword = !_isShowPassword;
    });
  }

  @override
  Widget build(BuildContext context) {
    return loading
        ? const Loading()
        : Scaffold(
            backgroundColor: Colors.white,
            appBar: AppBar(
              backgroundColor: primaryColor,
              title: const Text('Change password'),
              elevation: 0,
            ),
            body: SingleChildScrollView(
              child: Container(
                padding: const EdgeInsets.all(15),
                child: Column(
                  children: <Widget>[
                    Form(
                      key: _formKey,
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: <Widget>[
                          const ProfileTitle(
                              title: "Change your password account"),
                          const Divider(),
                          const SizedBox(height: 10),
                          Container(
                            color: Colors.white,
                            child: TextFormField(
                              obscureText: !_isShowPassword,
                              keyboardType: TextInputType.text,
                              validator: (val) => val.isEmpty
                                  ? 'Current password is required'
                                  : null,
                              onChanged: (val) {
                                setState(() => oldFormPassword = val);
                              },
                              decoration: InputDecoration(
                                contentPadding: EdgeInsets.zero,
                                prefixIcon: const Icon(Icons.lock),
                                suffixIcon: GestureDetector(
                                  onTap: () {
                                    _showPassword();
                                  },
                                  child: Icon(
                                    _isShowPassword
                                        ? Icons.visibility
                                        : Icons.visibility_off,
                                    color: _isShowPassword
                                        ? primaryColor
                                        : Colors.grey,
                                  ),
                                ),
                                isDense: true,
                                border: const OutlineInputBorder(),
                                labelText: 'Old Password',
                                errorText: (checkPassword != null)
                                    ? checkPassword
                                    : null,
                              ),
                            ),
                          ),
                          const SizedBox(height: 10),
                          Container(
                            color: Colors.white,
                            child: TextFormField(
                              obscureText: !_isShowPassword,
                              keyboardType: TextInputType.text,
                              validator: (val) {
                                if (val.isEmpty) return 'Cannot empty field';
                                if (val.length < 6) {
                                  return 'Minimum 6 character password';
                                }
                                return null;
                              },
                              onChanged: (val) {
                                setState(() => newFormPassword = val);
                              },
                              decoration: InputDecoration(
                                contentPadding: EdgeInsets.zero,
                                prefixIcon: const Icon(Icons.lock),
                                suffixIcon: GestureDetector(
                                  onTap: () {
                                    _showPassword();
                                  },
                                  child: Icon(
                                    _isShowPassword
                                        ? Icons.visibility
                                        : Icons.visibility_off,
                                    color: _isShowPassword
                                        ? primaryColor
                                        : Colors.grey,
                                  ),
                                ),
                                isDense: true,
                                border: const OutlineInputBorder(),
                                labelText: 'Password',
                              ),
                            ),
                          ),
                          const SizedBox(height: 10),
                          Container(
                            color: Colors.white,
                            child: TextFormField(
                              obscureText: !_isShowPassword,
                              keyboardType: TextInputType.text,
                              validator: (val) {
                                if (val.isEmpty) {
                                  return 'Required confirm password';
                                } else if (val != newFormPassword) {
                                  return 'Confirm not match';
                                }
                                return null;
                              },
                              decoration: InputDecoration(
                                contentPadding: EdgeInsets.zero,
                                prefixIcon: const Icon(Icons.lock),
                                suffixIcon: GestureDetector(
                                  onTap: () {
                                    _showPassword();
                                  },
                                  child: Icon(
                                    _isShowPassword
                                        ? Icons.visibility
                                        : Icons.visibility_off,
                                    color: _isShowPassword
                                        ? primaryColor
                                        : Colors.grey,
                                  ),
                                ),
                                isDense: true,
                                border: const OutlineInputBorder(),
                                labelText: 'Confirm Password',
                              ),
                            ),
                          ),
                          const SizedBox(height: 15),
                          ButtonSubmit(
                            text: 'SUBMIT',
                            onPress: () async {
                              if (_formKey.currentState.validate()) {
                                var user = firebaseAuth.currentUser;
                                setState(() => loading = true);
                                bool checkCurrentPasswordValid =
                                    await authService
                                        .validatePassword(oldFormPassword);
                                final snackBar = SnackBar(
                                  content:
                                      const Text('Kata sandi telah diubah'),
                                  action: SnackBarAction(
                                    label: 'Close',
                                    onPressed: () {
                                      ScaffoldMessenger.of(context)
                                          .hideCurrentSnackBar();
                                    },
                                  ),
                                );
                                ScaffoldMessenger.of(context)
                                    .showSnackBar(snackBar);
                                if (checkCurrentPasswordValid) {
                                  try {
                                    user.updatePassword(newFormPassword);
                                    setState(() => loading = false);
                                  } on FirebaseAuthException catch (e) {
                                    setState(() {
                                      checkPassword = e.code;
                                      loading = false;
                                    });
                                  } catch (e) {
                                    setState(() {
                                      checkPassword = e;
                                      loading = false;
                                    });
                                  }
                                } else {
                                  setState(() {
                                    checkPassword = 'Your old password wrong';
                                    loading = false;
                                  });
                                }
                              }
                            },
                          )
                        ],
                      ),
                    ),
                  ],
                ),
              ),
            ),
          );
  }
}
