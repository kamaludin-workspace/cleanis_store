import 'dart:io';
import 'package:cleanis_store/auth/auth_service.dart';
import 'package:cleanis_store/config/colors.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:firebase_storage/firebase_storage.dart';
import 'package:flutter/material.dart';
import 'package:image_picker/image_picker.dart';
import '../loading.dart';
import 'widget/submitbutton_widget.dart';
import 'widget/title_widget.dart';

class AccountScreen extends StatefulWidget {
  const AccountScreen({Key key}) : super(key: key);

  @override
  _AccountScreenState createState() => _AccountScreenState();
}

class _AccountScreenState extends State<AccountScreen> {
  final _formKey = GlobalKey<FormState>();
  final GlobalKey<ScaffoldState> _scaffoldKey = GlobalKey<ScaffoldState>();
  final AuthService authService = AuthService();
  final FirebaseAuth firebaseAuth = FirebaseAuth.instance;

  final db = FirebaseFirestore.instance;
  final picker = ImagePicker();

  final _currentUser = AuthService().getCurrentUser();
  final _getCurrentProfile = AuthService().getCurrentProfile();

  bool loading = false;
  String error;

  FirebaseAuth auth = FirebaseAuth.instance;
  File _image;

  Future getImage() async {
    final pickedFile = await picker.pickImage(source: ImageSource.gallery);
    setState(() {
      if (pickedFile != null) _image = File(pickedFile.path);
    });
  }

  void snackBarAction(title) {
    final snackBar = SnackBar(
      content: Text(title),
      action: SnackBarAction(
        label: 'Close',
        onPressed: () {
          ScaffoldMessenger.of(context).hideCurrentSnackBar();
        },
      ),
    );
    ScaffoldMessenger.of(context).showSnackBar(snackBar);
  }

  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return loading
        ? const Loading()
        : Scaffold(
            key: _scaffoldKey,
            backgroundColor: background,
            appBar: AppBar(
              backgroundColor: primaryColor,
              title: const Text("Account"),
              elevation: 0,
            ),
            body: SingleChildScrollView(
              child: Form(
                child: Container(
                  padding: const EdgeInsets.all(20),
                  child: FutureBuilder(
                      future: Future.wait([_currentUser, _getCurrentProfile]),
                      builder: (context, snapshot) {
                        if (snapshot.connectionState == ConnectionState.done) {
                          final user = snapshot.data[0];
                          final _nameController = TextEditingController()
                            ..text = user.displayName;
                          final userProfile = snapshot.data[1];
                          final _phoneController = TextEditingController()
                            ..text = (snapshot.data[1] == null)
                                ? user.phoneNumber
                                : userProfile['phoneNumber'];
                          return Form(
                            key: _formKey,
                            child: Column(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: [
                                const ProfileTitle(title: "Profile picture"),
                                const SizedBox(height: 10),
                                Row(
                                  children: [
                                    Container(
                                      decoration: BoxDecoration(
                                        color: Colors.white,
                                        shape: BoxShape.circle,
                                        boxShadow: [
                                          BoxShadow(
                                            blurRadius: 10,
                                            color: Colors.grey[400],
                                            spreadRadius: 1,
                                            offset: const Offset(0, 5),
                                          )
                                        ],
                                      ),
                                      child: CircleAvatar(
                                        radius: 57,
                                        backgroundColor: primaryColor,
                                        child: (user.photoURL == null)
                                            ? _image == null
                                                ? Text(
                                                    getInitials(user
                                                        .displayName),
                                                    style: const TextStyle(
                                                        fontSize: 26,
                                                        fontWeight: FontWeight
                                                            .w700))
                                                : CircleAvatar(
                                                    radius: 55.0,
                                                    backgroundImage:
                                                        FileImage(_image),
                                                    backgroundColor: Colors
                                                        .transparent)
                                            : _image == null
                                                ? CircleAvatar(
                                                    radius: 55,
                                                    backgroundColor: Colors
                                                        .white,
                                                    backgroundImage:
                                                        NetworkImage(
                                                            '${user.photoURL}'))
                                                : CircleAvatar(
                                                    radius: 55.0,
                                                    backgroundImage:
                                                        FileImage(_image),
                                                    backgroundColor:
                                                        Colors.transparent),
                                      ),
                                    ),
                                    const SizedBox(width: 20),
                                    OutlinedButton(
                                      child: Row(
                                        children: const [
                                          Icon(
                                            Icons.camera,
                                            color: primaryColor,
                                          ),
                                          Text(
                                            'Upload',
                                            style: TextStyle(
                                                fontSize: 14,
                                                color: primaryColor),
                                          )
                                        ],
                                      ),
                                      onPressed: () {
                                        getImage();
                                      },
                                      style: OutlinedButton.styleFrom(
                                        backgroundColor: Colors.white,
                                        elevation: 1,
                                        padding: const EdgeInsets.symmetric(
                                            horizontal: 15, vertical: 2),
                                        side: const BorderSide(
                                          width: 2,
                                          color: primaryColor,
                                        ),
                                        shape: const StadiumBorder(),
                                      ),
                                    ),
                                    ElevatedButton(
                                      style: ElevatedButton.styleFrom(
                                          elevation: 1,
                                          shape: const CircleBorder(),
                                          primary: Colors.white),
                                      onPressed: () {},
                                      child: Container(
                                          width: 35,
                                          height: 35,
                                          decoration: const BoxDecoration(
                                              shape: BoxShape.circle),
                                          child: const Icon(
                                            Icons.delete,
                                            color: Colors.redAccent,
                                          )),
                                    )
                                  ],
                                ),
                                const SizedBox(height: 20),
                                const ProfileTitle(title: "General"),
                                Container(
                                  height: 45,
                                  margin: const EdgeInsets.only(top: 10),
                                  color: Colors.white,
                                  child: TextFormField(
                                    keyboardType: TextInputType.text,
                                    validator: (val) =>
                                        val.isEmpty ? 'Name required' : null,
                                    decoration: const InputDecoration(
                                      contentPadding:
                                          EdgeInsets.symmetric(horizontal: 10),
                                      border: OutlineInputBorder(),
                                      labelText: 'Nama',
                                    ),
                                    controller: _nameController,
                                  ),
                                ),
                                Container(
                                  height: 45,
                                  color: Colors.white,
                                  margin: const EdgeInsets.only(top: 10),
                                  child: TextFormField(
                                    controller: _phoneController,
                                    validator: (val) => val.isEmpty
                                        ? 'Your number phone is required'
                                        : null,
                                    decoration: const InputDecoration(
                                      contentPadding:
                                          EdgeInsets.symmetric(horizontal: 10),
                                      border: OutlineInputBorder(),
                                      focusColor: primaryColor,
                                      fillColor: primaryColor,
                                      labelText: 'Phone',
                                    ),
                                  ),
                                ),
                                const SizedBox(height: 15),
                                ButtonSubmit(
                                  text: 'Update',
                                  onPress: () async {
                                    if (_formKey.currentState.validate()) {
                                      setState(() => loading = true);
                                      String uid = firebaseAuth.currentUser.uid;
                                      if (_image == null) {
                                        authService.changeProfile(
                                            displayName: _nameController.text,
                                            phoneNumber: _phoneController.text);
                                        setState(() {
                                          loading = !loading;
                                        });
                                        Future.delayed(
                                            const Duration(milliseconds: 600),
                                            () {
                                          snackBarAction('Profil Diperbarui');
                                        });
                                      } else if (_image != null) {
                                        setState(() {
                                          loading = true;
                                        });
                                        authService.changeProfile(
                                            displayName: _nameController.text,
                                            phoneNumber: _phoneController.text);
                                        var storeRef = FirebaseStorage.instance
                                            .ref('profile')
                                            .child(uid);
                                        var uploadTask =
                                            storeRef.putFile(_image);
                                        var completePut = await uploadTask;
                                        String downloadUrl = await completePut
                                            .ref
                                            .getDownloadURL();
                                        bool updateUrl =
                                            await authService.updatePhotoUrl(
                                                downloadUrl: downloadUrl);

                                        if (updateUrl) {
                                          setState(() {
                                            loading = false;
                                            Future.delayed(
                                                const Duration(
                                                    milliseconds: 500), () {
                                              snackBarAction(
                                                  'Photo Profil Diperbarui');
                                            });
                                          });
                                        }
                                      }
                                    }
                                  },
                                )
                              ],
                            ),
                          );
                        }
                        return const Center(
                          child: CircularProgressIndicator(),
                        );
                      }),
                ),
              ),
            ),
          );
  }

  String getInitials(String name) => name.isNotEmpty
      ? name.trim().split(' ').map((l) => l[0]).take(2).join()
      : '';
}
