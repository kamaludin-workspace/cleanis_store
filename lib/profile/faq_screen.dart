import 'package:cleanis_store/config/colors.dart';
import 'package:flutter/material.dart';

import 'widget/title_widget.dart';

class FaqScreen extends StatefulWidget {
  const FaqScreen({Key key}) : super(key: key);

  @override
  _FaqScreenState createState() => _FaqScreenState();
}

class _FaqScreenState extends State<FaqScreen> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: background,
      appBar: AppBar(
        backgroundColor: primaryColor,
        title: const Text('FAQ'),
        elevation: 0,
      ),
      body: SingleChildScrollView(
        padding: const EdgeInsets.all(15),
        child: Column(
          children: <Widget>[
            Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
                const ProfileTitle(title: "How we can help you?"),
                const Divider(),
                const SizedBox(height: 10),
                Container(
                  height: 45,
                  color: Colors.white,
                  child: const TextField(
                    keyboardType: TextInputType.text,
                    decoration: InputDecoration(
                        border: OutlineInputBorder(),
                        isDense: true,
                        labelText: 'Ketik permasalahan anda',
                        suffixIcon: Icon(Icons.search)),
                  ),
                ),
                const SizedBox(height: 20),
                const ProfileTitle(
                  title: "Frequently Asked Questions",
                ),
                const Divider(height: 10),
                ListTile(
                  title: Transform(
                    transform: Matrix4.translationValues(-16, 0.0, 0.0),
                    child: const Text("How to order?",
                        style: TextStyle(fontSize: 16, color: Colors.black)),
                  ),
                  trailing: Transform(
                    transform: Matrix4.translationValues(15, 0.0, 0.0),
                    child: const Icon(Icons.keyboard_arrow_right),
                  ),
                ),
                ListTile(
                  title: Transform(
                    transform: Matrix4.translationValues(-16, 0.0, 0.0),
                    child: const Text("How to cancel order?",
                        style: TextStyle(fontSize: 16, color: Colors.black)),
                  ),
                  trailing: Transform(
                    transform: Matrix4.translationValues(15, 0.0, 0.0),
                    child: const Icon(Icons.keyboard_arrow_right),
                  ),
                ),
                ListTile(
                  title: Transform(
                    transform: Matrix4.translationValues(-16, 0.0, 0.0),
                    child: const Text("How to pay order?",
                        style: TextStyle(fontSize: 16, color: Colors.black)),
                  ),
                  trailing: Transform(
                    transform: Matrix4.translationValues(15, 0.0, 0.0),
                    child: const Icon(Icons.keyboard_arrow_right),
                  ),
                ),
              ],
            ),
          ],
        ),
      ),
    );
  }
}
