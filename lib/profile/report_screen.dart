import 'package:cleanis_store/config/colors.dart';
import 'package:flutter/material.dart';

import 'widget/submitbutton_widget.dart';
import 'widget/title_widget.dart';

class ReportScreen extends StatefulWidget {
  const ReportScreen({Key key}) : super(key: key);

  @override
  _ReportScreenState createState() => _ReportScreenState();
}

enum Service { bug, feedback }

class _ReportScreenState extends State<ReportScreen> {
  Service _service = Service.bug;
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: background,
      appBar: AppBar(
        backgroundColor: primaryColor,
        title: const Text('Report & Feedback'),
        elevation: 0,
      ),
      body: Container(
        padding: const EdgeInsets.all(20),
        child: Column(
          children: <Widget>[
            Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
                const ProfileTitle(title: "Report or give your feedback"),
                const Divider(),
                const Text(
                  'Jenis',
                  style: TextStyle(fontSize: 18, fontWeight: FontWeight.w600),
                ),
                Container(
                  alignment: Alignment.bottomLeft,
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.start,
                    children: [
                      Radio(
                        activeColor: primaryColor,
                        value: Service.bug,
                        groupValue: _service,
                        onChanged: (Service value) {
                          setState(() {
                            _service = value;
                          });
                        },
                      ),
                      const Text('Some problem'),
                      const SizedBox(width: 15),
                      Radio(
                        activeColor: primaryColor,
                        value: Service.feedback,
                        groupValue: _service,
                        onChanged: (Service value) {
                          setState(() {
                            _service = value;
                          });
                        },
                      ),
                      const Text('Feedback'),
                    ],
                  ),
                ),
                const SizedBox(height: 10),
                Container(
                  color: Colors.white,
                  child: const TextField(
                    keyboardType: TextInputType.multiline,
                    minLines: 1,
                    maxLines: 5,
                    decoration: InputDecoration(
                      fillColor: Colors.red,
                      border: OutlineInputBorder(),
                      labelText: 'Your message',
                    ),
                  ),
                ),
                const SizedBox(height: 10),
                const Text(
                  'Image',
                  style: TextStyle(fontSize: 18, fontWeight: FontWeight.w600),
                ),
                const SizedBox(height: 15),
                const ButtonSubmit(
                  text: 'SUBMIT',
                )
              ],
            ),
          ],
        ),
      ),
    );
  }
}
