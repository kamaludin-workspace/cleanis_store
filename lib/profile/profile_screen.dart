import 'package:cleanis_store/auth/auth_service.dart';
import 'package:cleanis_store/config/colors.dart';
import 'package:cleanis_store/config/styles.dart';
import 'package:cleanis_store/profile/faq_screen.dart';
import 'package:cleanis_store/profile/password_screen.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'report_screen.dart';
import 'widget/profileuser_widget.dart';
import 'package:cleanis_store/profile/account_screen.dart';

class ProfileScreen extends StatefulWidget {
  const ProfileScreen({Key key}) : super(key: key);

  @override
  _ProfileScreenState createState() => _ProfileScreenState();
}

class _ProfileScreenState extends State<ProfileScreen> {
  var currentUser = FirebaseAuth.instance.currentUser;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: background,
      appBar: AppBar(
        backgroundColor: primaryColor,
        title: const Text('Profile', style: appBarStyleLight),
        elevation: 0,
      ),
      body: SingleChildScrollView(
        child: Column(
          children: <Widget>[
            ProfileUser(
              name: (currentUser.displayName == '')
                  ? 'Empty Display Name'
                  : currentUser.displayName,
              image: (currentUser.photoURL == null)
                  ? (currentUser.displayName != null)
                      ? 'https://ui-avatars.com/api/?font-size=0.33&size=265&name=' +
                          getInitials(currentUser.displayName) +
                          '&color=fff&background=1976D2'
                      : 'https://ui-avatars.com/api/?font-size=0.33&size=265&name=NN&color=fff&background=1976D2'
                  : currentUser.photoURL,
            ),
            Container(
              margin: const EdgeInsets.all(10),
              decoration: BoxDecoration(
                color: Colors.white,
                borderRadius: const BorderRadius.all(
                  Radius.circular(8.0),
                ),
                border: Border.all(
                  width: 1,
                  color: Colors.grey[300],
                ),
              ),
              child: Column(
                children: <Widget>[
                  ListTile(
                    leading: const Icon(
                      Icons.face,
                      color: primaryColor,
                    ),
                    title: const Text('Account'),
                    trailing: const Icon(Icons.keyboard_arrow_right),
                    onTap: () {
                      Navigator.push(
                        context,
                        MaterialPageRoute(
                          builder: (context) => const AccountScreen(),
                        ),
                      ).then((_) => {setState(() {})});
                    },
                  ),
                  const Divider(height: 1),
                  ListTile(
                    leading: const Icon(
                      Icons.lock,
                      color: primaryColor,
                    ),
                    title: const Text('Change Password'),
                    trailing: const Icon(Icons.keyboard_arrow_right),
                    onTap: () {
                      Navigator.push(
                        context,
                        MaterialPageRoute(
                          builder: (context) => const PasswordScreen(),
                        ),
                      );
                    },
                  ),
                ],
              ),
            ),
            const SizedBox(height: 10),
            Container(
              padding: const EdgeInsets.symmetric(horizontal: 10),
              alignment: Alignment.topLeft,
              child: Text(
                'Others',
                style: TextStyle(
                  fontSize: 18,
                  color: Colors.grey[500],
                ),
              ),
            ),
            Container(
              margin: const EdgeInsets.all(10),
              decoration: BoxDecoration(
                color: Colors.white,
                borderRadius: const BorderRadius.all(
                  Radius.circular(8.0),
                ),
                border: Border.all(
                  width: 1,
                  color: Colors.grey[300],
                ),
              ),
              child: Column(
                children: <Widget>[
                  ListTile(
                    leading: const Icon(
                      Icons.feedback,
                      color: primaryColor,
                    ),
                    title: const Text('Report & Feedback'),
                    trailing: const Icon(Icons.keyboard_arrow_right),
                    onTap: () {
                      Navigator.push(
                        context,
                        MaterialPageRoute(
                          builder: (context) => const ReportScreen(),
                        ),
                      );
                    },
                  ),
                  const Divider(height: 1),
                  ListTile(
                    leading: const Icon(
                      Icons.help,
                      color: primaryColor,
                    ),
                    title: const Text('FAQ'),
                    trailing: const Icon(Icons.keyboard_arrow_right),
                    onTap: () {
                      Navigator.push(
                        context,
                        MaterialPageRoute(
                          builder: (context) => const FaqScreen(),
                        ),
                      );
                    },
                  ),
                  const Divider(height: 1),
                  ListTile(
                    leading: const Icon(
                      Icons.developer_mode,
                      color: primaryColor,
                    ),
                    title: const Text('About'),
                    trailing: const Icon(Icons.keyboard_arrow_right),
                    onTap: () {
                      showAboutDialog(
                        applicationName: 'Cleanis',
                        context: context,
                        applicationIcon: Image.asset(
                          'assets/images/logo.png',
                          scale: 3,
                        ),
                        applicationVersion: '0.1.0',
                        applicationLegalese: 'Code with ⚡ and 💻',
                      );
                    },
                  ),
                  const Divider(height: 1),
                  ListTile(
                    leading: const Icon(
                      Icons.power_settings_new,
                      color: primaryColor,
                    ),
                    title: const Text('Log out'),
                    subtitle: Text(currentUser.email),
                    trailing: const Icon(Icons.keyboard_arrow_right),
                    onTap: () {
                      confirmLogout(context);
                    },
                  ),
                ],
              ),
            ),
          ],
        ),
      ),
    );
  }

  String getInitials(String name) => name.isNotEmpty
      ? name.trim().split(' ').map((l) => l[0]).take(2).join()
      : '';

  confirmLogout(BuildContext context) {
    Widget cancelButton = TextButton(
      child: const Text("Log Out"),
      onPressed: () {
        context.read<AuthService>().signOut();
        Navigator.pop(context);
      },
    );
    Widget continueButton = OutlinedButton(
      child: const Text("Cancel"),
      style: OutlinedButton.styleFrom(
        primary: Colors.white,
        backgroundColor: primaryColor,
      ),
      onPressed: () => Navigator.pop(context),
    );
    AlertDialog alert = AlertDialog(
      title: const Text("Confirm Log out",
          style: TextStyle(
            fontSize: 18,
            fontWeight: FontWeight.bold,
          )),
      content: const Text("Are you sure to log out?"),
      actions: [
        cancelButton,
        continueButton,
      ],
    );
    showDialog(
      context: context,
      builder: (BuildContext context) {
        return alert;
      },
    );
  }
}
