import 'package:cleanis_store/config/colors.dart';
import 'package:flutter/material.dart';
import 'package:introduction_screen/introduction_screen.dart';
import 'package:shared_preferences/shared_preferences.dart';

import 'main.dart';

class OnBoardingPage extends StatefulWidget {
  const OnBoardingPage({key}) : super(key: key);

  @override
  _OnBoardingPageState createState() => _OnBoardingPageState();
}

class _OnBoardingPageState extends State<OnBoardingPage> {
  final introKey = GlobalKey<IntroductionScreenState>();

  void _onIntroEnd(context) {
    Navigator.of(context).pushReplacement(
      MaterialPageRoute(builder: (_) => const AppWrapper()),
    );
    _storeOnboardInfo();
  }

  Widget _buildImage(String assetName, [double width = 200]) {
    return Image.asset('assets/images/onboarding/$assetName', width: width);
  }

  _storeOnboardInfo() async {
    int isOnboard = 0;
    SharedPreferences prefs = await SharedPreferences.getInstance();
    await prefs.setInt('onBoard', isOnboard);
  }

  @override
  Widget build(BuildContext context) {
    const bodyStyle = TextStyle(fontSize: 19.0);
    const pageDecoration = PageDecoration(
      titlePadding: EdgeInsets.fromLTRB(0, 16, 0, 10),
      titleTextStyle: TextStyle(
          fontSize: 24.0, fontWeight: FontWeight.w700, color: primaryColor),
      bodyTextStyle: bodyStyle,
      descriptionPadding: EdgeInsets.fromLTRB(16.0, 0.0, 16.0, 0),
      pageColor: Colors.white,
    );

    return IntroductionScreen(
      key: introKey,
      globalBackgroundColor: Colors.white,
      pages: [
        PageViewModel(
          title: "Choose your clothes",
          body:
              "Lorem Ipsum is simply dummy text of the printing and typesetting industry the 1500s",
          image: _buildImage('laundry.png'),
          decoration: pageDecoration,
        ),
        PageViewModel(
          title: "Schedule pickup",
          body:
              "It is a long established fact that a reader will be distracted by the readable content",
          image: _buildImage('time.png'),
          decoration: pageDecoration,
        ),
        PageViewModel(
          title: "Get delivery",
          body: "Just stay and get delivered on your package",
          image: _buildImage('deliver.png'),
          decoration: pageDecoration,
        ),
        PageViewModel(
          title: "Track progress",
          body: "Track your dhobee progress on screen",
          image: _buildImage('trace.png'),
          decoration: pageDecoration,
        ),
        PageViewModel(
          title: "Pay later & satisfied",
          body: "Pay for order using credit or debit card",
          image: _buildImage('money.png'),
          decoration: pageDecoration,
        ),
      ],
      onDone: () => _onIntroEnd(context),
      showSkipButton: true,
      skipFlex: 0,
      nextFlex: 0,
      skip: const Text('Skip', style: TextStyle(color: primaryColor)),
      next: const Icon(
        Icons.arrow_forward,
        color: primaryColor,
      ),
      done: const Text('Done',
          style: TextStyle(fontWeight: FontWeight.w600, color: primaryColor)),
      curve: Curves.fastLinearToSlowEaseIn,
      controlsMargin: const EdgeInsets.all(16),
      controlsPadding: const EdgeInsets.fromLTRB(8.0, 4.0, 8.0, 4.0),
      dotsDecorator: const DotsDecorator(
        size: Size(8.0, 10.0),
        activeColor: primaryColor,
        color: primaryLightenColor,
        activeSize: Size(22.0, 10.0),
        activeShape: RoundedRectangleBorder(
          borderRadius: BorderRadius.all(Radius.circular(25.0)),
        ),
      ),
      dotsContainerDecorator: const ShapeDecoration(
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.all(Radius.circular(8.0)),
        ),
      ),
    );
  }
}
