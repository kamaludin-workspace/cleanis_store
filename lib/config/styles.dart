import 'package:flutter/material.dart';

import 'colors.dart';

// Example style
const headingTitle = TextStyle(
  fontSize: 22,
  fontWeight: FontWeight.w600,
);

// AUTH SCREEN STYLE
const styleTextAccount = TextStyle(
  fontSize: 14,
  color: Colors.black87,
  fontWeight: FontWeight.w300,
);

const styleTextPage = TextStyle(
  fontSize: 14,
  color: primaryColor,
  fontWeight: FontWeight.w800,
  decoration: TextDecoration.underline,
);

// MAIN
// 1. Appbar style
const appBarStyleDark = TextStyle(
  fontSize: 18,
  color: Colors.black,
);

const appBarStyleLight = TextStyle(
  fontSize: 20,
  color: Colors.white,
);

// 2. Title sections
const titleStyle = TextStyle(
  fontSize: 20,
  fontWeight: FontWeight.w700,
);
